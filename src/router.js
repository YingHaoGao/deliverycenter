import { lazy } from 'react'
const Backlog = lazy(() => import('@/routes/deliverycenter/Backlog'))
const Projects = lazy(() => import('@/routes/deliverycenter/Projects'))
const ProjectInfo = lazy(() => import('@/routes/deliverycenter/Projects/info'))
const Budget = lazy(() => import('@/routes/deliverycenter/Budget'))
const Logs = lazy(() => import('@/routes/deliverycenter/Logs'))
const Tasks = lazy(() => import('@/routes/deliverycenter/Tasks'))
const TaskInfo = lazy(() => import('@/routes/deliverycenter/Tasks/info'))
const Weeklys = lazy(() => import('@/routes/deliverycenter/Weeklys'))
const WeeklyInfo = lazy(() => import('@/routes/deliverycenter/Weeklys/info'))
const EditWeekly = lazy(() => import('@/routes/deliverycenter/Weeklys/edit'))
const Apply = lazy(() => import('@/routes/deliverycenter/Apply'))
const Examine = lazy(() => import('@/routes/deliverycenter/Examine'))
const Delivery = lazy(() => import('@/routes/deliverycenter/Delivery'))
const ApplyInfo = lazy(() => import('@/routes/deliverycenter/Apply/info'))
const ExamineInfo = lazy(() => import('@/routes/deliverycenter/Examine/info'))
const NotFound = lazy(() => import('@/routes/NotFound'))

export default [
  /**
   * 开发环境跳/login_admin重定向到/
   */
  process.env.NODE_ENV === 'development' && {
    path: '/login_admin',
    redirect: '/'
  },
  {
    path: '/',
    redirect: '/deliverycenter'
  },
  {
    path: '/deliverycenter',
    redirect: '/deliverycenter/backlog',
    routes: [
      {
        path: '/deliverycenter/backlog',
        component: Backlog
      },
      {
        path: '/deliverycenter/projects',
        component: Projects,
        exact: true,
        routes: [
          {
            path: '/deliverycenter/projects/project/:id',
            component: ProjectInfo
          }
        ]
      },
      {
        path: '/deliverycenter/budget/:id',
        component: Budget
      },
      {
        path: '/deliverycenter/logs',
        component: Logs
      },
      {
        path: '/deliverycenter/tasks',
        component: Tasks,
        exact: true,
        routes: [
          {
            path: '/deliverycenter/tasks/task/:id',
            component: TaskInfo
          }
        ]
      },
      {
        path: '/deliverycenter/weeklys',
        component: Weeklys,
        exact: true,
        routes: [
          {
            path: '/deliverycenter/weeklys/weekly/:id',
            component: WeeklyInfo
          },
          {
            path: '/deliverycenter/weeklys/editweekly',
            component: EditWeekly
          }
        ]
      },
      {
        path: '/deliverycenter/applys',
        component: Apply,
        exact: true,
        routes: [
          {
            path: '/deliverycenter/applys/apply/:id',
            component: ApplyInfo
          }
        ]
      },
      {
        path: '/deliverycenter/examines',
        component: Examine,
        exact: true,
        routes: [
          {
            path: '/deliverycenter/examines/examine/:id',
            component: ExamineInfo
          }
        ]
      },
      {
        path: '/deliverycenter/delivery',
        component: Delivery
      }
    ]
  },
  {
    path: '*',
    component: NotFound
  }
].filter(Boolean)
