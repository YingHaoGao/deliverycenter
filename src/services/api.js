import request from '@/utils/request'

export function getAnalysisCount () {
  return request.get('crm/api/analysis/count')
}

export function getAnalysisTendency (params) {
  return request.get('crm/api/analysis/tendency', { params })
}

export function getAnalysisPriority (params) {
  return request.get('crm/api/analysis/priority', { params })
}

export function getAnalysisDistribution (params) {
  return request.get('crm/api/analysis/distribution', { params })
}

export function getAnalysisStatistics (params) {
  return request.get('crm/api/analysis/statistics', { params })
}

export function getTable (params) {
  return request.get('crm/api/table')
}

export function postForm (data) {
  return request.post('crm/api/form', { data })
}

export function getUser (params) {
  return request.get('crm/api/user')
}

export function getJobs (params) {
  return request.get('crm/api/job')
}

export function getGroup (params) {
  return request.get('crm/api/group')
}

export function getOrganize (params) {
  return request.get('crm/api/organize')
}

export function getBacklog (params) {
  return request.get('crm/api/backlog')
}

export function getProjects (params) {
  return request.get('crm/api/ProjectInfo/queryAll', { params })
}

export function getProjectInfo (params) {
  return request.get('crm/api/ProjectInfo/queryById', { params })
}
// 报价单
export function getProjectQuotation (params) {
  return request.get('crm/api/Quotation/queryByProjectId', { params })
}

export function postSubStageAdd (params) {
  return request.post('crm/api/SubStage/add', params)
}
// 根据项目ID查询预算列表
export function getBudgetList (data) {
  return request.post('crm/api/BudgetDetail/queryByPid', data)
}
// 获取合同信息
export function getconAttInfo (data) {
  return request.post('crm/api/ContractAttachment/queryById', data)
}
// 获取回款计划
export function getConPayIdList (params) {
  return request.get('crm/api/ContractPayment/queryById', { params })
}

export function getWeightExceed100 (params) {
  return request.get('crm/api/SubStage/weightExceed100', { params })
}

export function getSubStageQueryStageTask (params) {
  return request.get('crm/api/SubStage/queryStageTask', { params })
}

export function delSubStage (params) {
  return request.get('crm/api/SubStage/delete', { params })
}

export function getContract (params) {
  return request.get('crm/api/ContractAttachment/queryByPid', { params })
}

export function getCompanyContact (params) {
  return request.get('crm/api/CompanyInfo/queryByProId', { params })
}

export function getStageDeliverable (params) {
  return request.get('crm/api/Deliverable/queryByStageId', { params })
}

export function getStageDeliverable1 (params) {
  return request.get('crm/api/Deliverable/queryBySubStageId', { params })
}

export function getApplyList (params) {
  return request.get('crm/api/ApplyInfo/queryAll', { params })
}

export function getBacklogList (params) {
  return request.get('crm/api/TaskInfo/queryByTaskLeaderOrStatus', { params })
}

export function getTaskInfo (params) {
  return request.get('crm/api/TaskInfo/queryById', { params })
}

export function getManagerList (params) {
  return request.get('crm/api/Tenant/getUserByDept', { params })
}

export function getManagerNameList (params) {
  return request.get('crm/api/Tenant/UserByDeptName', { params })
}

export function postAssignManager (params) {
  return request.get('crm/api/TaskInfo/queryByTaskLeaderOrStatus', { params })
}

export function postProjectUpdata (data) {
  return request.post('crm/api/ProjectInfo/update', data)
}

export function postConApproval (data) {
  return request.post('crm/api/ContractReview/approval', data)
}

export function postTaskAdd (data) {
  return request.post('crm/api/TaskInfo/add', data)
}

export function postFileUpload (data) {
  return request.post('crm/api/File/fileUpload', data)
}

export function delFile (params) {
  return request.get('crm/api/File/fileDelete', { params })
}

export function changeTaskStatus (data) {
  return request.post('crm/api/TaskInfo/changeStatus', data)
}

export function getRoleList (params) {
  return request.get('crm/api/Tenant/roleList', { params })
}

export function getDeliverableList (params) {
  return request.get('crm/api/Deliverable/queryByProjectId', { params })
}

export function addDeliverable (data) {
  return request.post('crm/api/Deliverable/add', data)
}

export function postTaskList (data) {
  return request.post('crm/api/TaskInfo/queryByCondition', data)
}

export function getTaskList (params) {
  return request.get('crm/api/TaskInfo/queryByProjectId', { params })
}

export function getUnDelList (params) {
  return request.get('crm/api/Deliverable/listBySubStageId', { params })
}

export function postAppAddPid (data) {
  return request.post('crm/api/ApplyInfo/addByPid', data)
}

export function postTaskChangeStatus (data) {
  return request.post('crm/api/TaskInfo/changeStatus', data)
}

export function delTask (data) {
  return request.post('crm/api/TaskInfo/delete', data)
}

export function postWorkList (data) {
  return request.post('crm/api/WorkLog/queryList', data)
}

export function postWorkLogList (data) {
  return request.post('crm/api/WorkLog/queryByEntity', data)
}

export function postWork (data) {
  return request.post('crm/api/WorkLog/add', data)
}

export function editTaskInfo (data) {
  return request.post('crm/api/TaskInfo/update', data)
}

export function postListUsers (data) {
  return request.post('crm/api/Tenant/listUsers', data)
}

export function getDeptList (params) {
  return request.get('crm/api/Tenant/getUserByDept', { params })
}

export function getDeptInfo (params) {
  return request.get('crm/api/Tenant/getUser', { params })
}

export function postApprovalList (data) {
  return request.post('crm/api/ApprovalInfo/queryAll', data)
}

export function postApprovalInfo (data) {
  return request.post('crm/api/ApprovalInfo/queryEntity', data)
}

export function getAttachmentList (params) {
  return request.get('crm/api/Attachment/queryByTaskId', { params })
}

export function getTaskDeliverList (params) {
  return request.get('crm/api/Deliverable/queryByTaskId', { params })
}

export function getWorkInfo (params) {
  return request.get('crm/api/WorkWeekly/queryById', { params })
}

export function getWorkList (params) {
  return request.get('crm/api/WorkWeekly/queryList', { params })
}

export function getTenantList (params) {
  return request.get('crm/api/Tenant/listDeparts', { params })
}

export function getWorkWeekly (params) {
  return request.get('crm/api/WorkWeekly/queryById', { params })
}

export function postWorkWeekly (data) {
  return request.post('crm/api/WorkWeekly/add', data)
}

export function getMonitoring (params) {
  return request.get('crm/api/BudgetDetail/costMonitoring', { params })
}

// 根据项目id获取项目成员
export function getProjectMember (params) {
  return request.get('crm/api/ProjectMember/queryByProjectId', { params })
}

// 修改子阶段
export function postSubUpdata (data) {
  return request.post('crm/api/SubStage/updateStage', data)
}

// 修改子阶段状态
export function changeSubStageStatus (data) {
  return request.post('crm/api/SubStage/changeStatus', data)
}

// 根据项目成员id获取项目成员
export function getProjectMemberIn (params) {
  return request.get('crm/api/ProjectMember/queryById', { params })
}

// 删除项目成员
export function delProjectMember (data) {
  return request.post('crm/api/ProjectMember/delete', data)
}

// 批量删除项目成员
export function delBatchProjectMember (data) {
  return request.post('crm/api/ProjectMember/deleteBatch', data)
}

// 新建项目成员
// "projectId":"String",   //項目id
// "memberId":"string",    //成员id
// "createUserId":"string"  //创建人
export function addProjectMember (data) {
  return request.post('crm/api/ProjectMember/add', data)
}

// 批量新建项目成员
export function addBatchProjectMember (data) {
  return request.post('crm/api/ProjectMember/addBatch', data)
}

// 年度业绩
export function getContractYear (params) {
  return request.get('crm/api/ContractPayment/yearPerformance', { params })
}

// 验收计划
export function getContractAcceptance (params) {
  return request.get('crm/api/ContractPayment/acceptancePlan', { params })
}

// 获取用户部门列表
export function getTenantdept (params) {
  return request.get('crm/api/Tenant/deptByUserId', { params })
}

// 工时统计
export function getWorkHours (params) {
  return request.get('crm/api/WorkLog/statisticalHours', { params })
}

// 删除交付物
export function delDeliverable (params) {
  return request.post('crm/api/Deliverable/delete', params)
}

// 回款点
export function getConPayList (data) {
  return request.post('crm/api/ContractPayment/queryByEntity', data)
}

// 收入点
export function getConIncomeList (data) {
  return request.post('crm/api/ContractIncome/queryByEntity', data)
}

// 通过项目id获取客户详情
export function getCompanyInfoByProId (params) {
  return request.get('/crm/api/CompanyInfo/queryByProId', { params })
}

// 根据项目id获取合同列表信息
export function getContractAttachment (params) {
  return request.get('/crm/api/ContractAttachment/queryAll', { params })
}

// 通过项目id获取沟通记录
export function getCommuniInfoByProId (params) {
  return request.get('/crm/api/Communication/queryByProjectId', { params })
}

// 预算详情
export function getBudgetDetail (params) {
  return request.get('/crm/api/BudgetDetail/queryAll', { params })
}

// 文件预览
export function getFileList (params) {
  return request.get('/crm/api/Attachment/queryByCommunId', { params })
}

// 根据id删除项目沟通记录
export function deleteProject (data) {
  return request.post('/crm/api/Communication/deleteProject', data)
}

// 根据申请id获取审批信息
export function getApprovalInfoById (params) {
  return request.get('/crm/api/ApprovalInfo/queryById', { params })
}

// 根据申请ID获取申请信息
export function getApplyInfoById (params) {
  return request.get('/crm/api/ApplyInfo/queryById', { params })
}

// 获取审批人(自上而下)
export function getNextApprovalList (params) {
  return request.get('/crm/api/Workflow/getNextApprova', { params })
}