/* eslint-disable */
const { areaNameList } = global.params

// 获取地址栏参数
export function getUrlParams (url) {
  // eslint-disable-next-line no-new-object
  var params = new Object()
  if (url.indexOf('?') != -1) {
    var str = url.substr(1)
    var strs = str.split('&')
    for (var i = 0; i < strs.length; i++) {
      params[strs[i].split('=')[0]] = unescape(strs[i].split('=')[1])
    }
  }
  return params
}

// 拼接省市区地址
export function exchangeArea (area) {
  if (area) {
    const addressArr = area.split(',')
    return areaNameList[addressArr[0]]['label'] + areaNameList[addressArr[0]][addressArr[1]]['label'] + areaNameList[addressArr[0]][addressArr[1]][addressArr[2]]
  }
}

// 转换竞争对手和销售产品
export function getCompititor (data, enumerated) {
  let competitorStr = ''
  data.map((item, index) => {
    if ((index + 1) < data.length) {
      competitorStr += enumerated[item] + ','
    } else {
      competitorStr += enumerated[item]
    }
  })
  return competitorStr
}
