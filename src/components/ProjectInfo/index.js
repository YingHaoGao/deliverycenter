import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Row, Col } from '@uyun/components'
import { exchangeArea, getCompititor } from '@/utils/common'

const { industryArr, customerDepartment, purchaseType, tenderType, salesProducts, competitorCompany } = global.params

@inject('projectsStore')
@observer
class Information extends Component {
  render () {
  	const projectsStore = this.props.projectsStore
    const { projectInfo, companyInfo } = this.props.projectsStore
    console.log('companyInfo', companyInfo)
    return (
      <div className="project-info-box">
        <div className="project-info-title">项目信息</div>
        <div className="project-info-content">
          <Row>
            <Col span={6}>客户名称：{ companyInfo.companyName }</Col>
            <Col span={6}>所属行业：{industryArr[projectsStore.industry[0]] ? industryArr[projectsStore.industry[0]].name : ''}
              {industryArr[projectsStore.industry[0]] ? industryArr[projectsStore.industry[0]].children[projectsStore.industry[1]] : ''}</Col>
            <Col span={6}>客户类别：{ customerDepartment[companyInfo.companyType] }</Col>
          </Row>
          <Row>
            <Col span={6}>省市地址：{ exchangeArea(companyInfo.companyArea) }</Col>
            <Col span={6}>详细地址：{ companyInfo.companyAddress }</Col>
          </Row>
          <Row>
            <Col span={6}>采购方式：{ purchaseType[projectInfo.purchaseType] }</Col>
            <Col span={6}>投标方式：{ tenderType[projectInfo.tenderType] }</Col>
            <Col span={6}>是否中标：{ projectInfo.isContract ? '是' : '否' }</Col>
          </Row>
          <Row>
            <Col span={6}>项目预算：{ projectInfo.budget ? projectInfo.budget : 0 }万</Col>
            <Col span={6}>预计签约金额：{ projectInfo.contractAmount ? projectInfo.contractAmount : 0 }万</Col>
            <Col span={6}>预计签约时间：{ projectInfo.contractTime }</Col>
          </Row>
          <div>销售产品：{getCompititor(projectsStore.salesProducts, salesProducts)}</div>
          <div>竞争对手：{getCompititor(projectsStore.competitor, competitorCompany)}</div>
          <div>现有运维产品：{projectInfo.isSales ? '有' : '无'}</div>
          {projectInfo.remarks ? <div>立项描述：<br />{projectInfo.remarks}</div> : ''}
        </div>
        <div className="project-info-title">联系人信息</div>
        { companyInfo.contactList
          ? <div>
            <Row className="link-man-header" type="flex">
              <Col span={4}>联系人姓名</Col>
              <Col span={4}>职位</Col>
              <Col span={4}>部门</Col>
              <Col span={4}>联系方式</Col>
              <Col span={4}>邮箱</Col>
            </Row>
            { companyInfo.contactList.map((item, index) => {
              return (
                <Row className="link-man-details" key={index}>
                  <Col span={4}>{item.contactName ? item.contactName : ''}</Col>
                  <Col span={4}>{item.contactPosition}</Col>
                  <Col span={4}>{item.contactDepartment}</Col>
                  <Col span={4}>{item.contactMobile}</Col>
                  <Col span={4}>{item.contactEmail}</Col>
                </Row>
              )
            })
            }
          </div> : ''}
      </div>
    )
  }
}

export default Information
