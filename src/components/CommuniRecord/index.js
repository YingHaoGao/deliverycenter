/* eslint-disable */
import React, { Component } from 'react'
import { Row, Col, Modal, Popconfirm, message, Form } from '@uyun/components'
import { exchangeArea } from '@/utils/common'
import { getFileList, deleteProject } from '@/services/api'
// import { purchaseType, tenderType } from '../../../public/static/variableMap'

export default class CommuniRecordCnt extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      comDetails: [],
      communicationAddr: [],
      fileList: []
    }
  }

  showModal = (id, index) => {
    // 获取项目沟通记录详情
    this.setState({ visible: true, comDetails: this.props.communiInfo.list[index] }, () => {
      this.setState({ communicationAddr: exchangeArea(this.state.comDetails.communicationAddr) })
      // const addressArr = this.state.comDetails.communicationAddr.split(',')
      // this.setState({ communicationAddr: areaNameList[addressArr[0]]['label'] + areaNameList[addressArr[0]][addressArr[1]]['label'] + areaNameList[addressArr[0]][addressArr[1]][addressArr[2]] })
    })
    // 获取附件列表
    getFileList({ communId: id }).then((res) => {
      if (res.code === 0) {
        this.setState({ fileList: res.resultMap.data })
      }
    })
    // this.props.projectStore.getFileList(id)
  }

  // 删除项目沟通记录
  confirm = (id) => {
    deleteProject({ id: id, userId: window.USER_INFO.userId }).then((res) => {
      if (res.code === 0) {
        message.success('删除成功')
        this.props.getCommunication()
      } else if (res.code === 1) {
        message.error('用户无权限')
      }
    })
  }

  // 关闭弹出框
  handleCancel = e => {
    this.setState({
      visible: false
    })
  };

  render () {
    const { communiInfo } = this.props
    const { comDetails, fileList } = this.state
    return (
      <div className="project-info-box">
        {
          communiInfo.list ? <div>
            <div className="project-info-title">沟通记录</div>
            <Row className="link-man-header">
              <Col span={1} order={1}>序号</Col>
              <Col span={3} order={2}>标题</Col>
              <Col span={4} order={3}>记录人</Col>
              <Col span={2} order={4}>附件</Col>
              {/* <Col span={2} order={5}>提交人</Col> */}
              <Col span={5} order={5}>创建时间</Col>
              <Col span={4} order={6}>操作</Col>
            </Row>
          </div> : ''
        }

        {
          communiInfo.list ? communiInfo.list.map((item, index) => {
            return (
              <Row className="link-man-details" key={index}>
                <Col span={1} order={1}>{index + 1}</Col>
                <Col span={3} order={2} style={{ color: '#3BA1FF', cursor: 'pointer' }} onClick={() => { this.showModal(item['id'], index) }}>{item['communicationTitle']}</Col>
                <Col span={4} order={3}>{item['createUser'] ? item['createUser'] : ''}</Col>
                <Col span={2} order={4}>{item['fileCount'] ? item['fileCount'] : '0'}</Col>
                <Col span={5} order={5}>{item['createTime'] ? item['createTime'] : ''}</Col>
                <Popconfirm title="你确定要删除么?" onConfirm={() => { this.confirm(item['id']) }} onCancel={this.cancel} okText="确定" cancelText="取消">
                  <Col span={1} order={6} style={{ color: '#3BA1FF', cursor: 'pointer' }}>删除</Col>
                </Popconfirm>
                <Modal
                  title="沟通记录"
                  className="commLog"
                  visible={this.state.visible}
                  onCancel={this.handleCancel}
                  footer={[]}
                  maskStyle={{ backgroundColor: 'rgba(0,0,0,.1)' }}
                >
                  <Form className="commLog-log" >
                    <Row><Col span={4}>主题：</Col><Col>{comDetails.communicationTitle}</Col></Row>
                    <Row><Col span={4}>沟通日期：</Col><Col>{comDetails.communicationTime}</Col></Row>
                    <Row><Col span={4}>沟通地址：</Col><Col>{this.state.communicationAddr}&ensp;&ensp;{comDetails.detailAddress}</Col></Row>
                    <Row><Col span={4}>沟通方式：</Col><Col>{comDetails.communicationWay === '1' ? '面谈拜访' : '电话会议' }</Col></Row>
                    <Row><Col span={4}>记录创建人：</Col><Col>{comDetails.createUser}</Col></Row>
                    <Row><Col span={4}>创建时间：</Col><Col>{comDetails.createTime}</Col></Row>
                    <Row><Col span={4}>沟通纪要：</Col><Col>{comDetails.communicationContent}</Col></Row>

                    {fileList.length > 0 ? <div>
                      <Row><Col span={3} className="commLog-docfile">文档附件</Col></Row>
                      <Row type="flex" className="commLog-log-title">
                        <Col span={2} order={1}>序号</Col>
                        <Col span={8} order={2}>文件名称</Col>
                        <Col span={9} order={3}>提交时间</Col>
                        <Col span={3} order={4}>操作</Col>
                      </Row>
                    </div> : ''}
                    {
                      fileList ? fileList.map((item, index) => {
                        return (
                          <Row type="flex" className="commLog-log-content" key={index}>
                            <Col span={2} order={1}>{index + 1}</Col>
                            <Col span={8} order={2}>{item.fileName}</Col>
                            <Col span={9} order={3}>{item.createTime}</Col>
                            <Col span={3} order={4}><a href={item.fileDownloadPath}>预览</a></Col>
                          </Row>
                        )
                      }) : ''
                    }
                  </Form>
                </Modal>
              </Row>
            )
          }) : ''
        }
      </div>
    )
  }
}
