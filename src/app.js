import moment from 'moment'
import router from './router'
import { stores } from './stores'
import Menus from './common/menu'
import { Provider } from 'mobx-react'
import cookie from '@uyun/utils/cookie'
import __, { intl } from '@uyun/utils/i18n'
import locales from './common/locales.json'
import React, { PureComponent } from 'react'
import BasicLayout from '@uyun/ec-basic-layout'
import { Router, Link } from 'react-router-dom'
import { LocaleProvider, Icon, Button } from '@uyun/components'
import { history, renderRoutes } from './utils/router'
import enUS from '@uyun/components/lib/locale-provider/en_US'
import zhCN from '@uyun/components/lib/locale-provider/zh_CN'
import 'moment/locale/zh-cn'

intl.merge(locales)
moment.locale('zh-cn')
moment.defaultFormat = 'YYYY-MM-DD HH:mm'

export default class App extends PureComponent {
  get menus () {
    return [
      {
        key: 'backlog',
        name: __('menu-backlog'),
        type: 'link',
        icon: <Icon type="home" />,
        path: 'deliverycenter/backlog'
      },
      {
        key: 'projects',
        name: __('menu-projects'),
        type: 'link',
        icon: <Icon type="line-chart" />,
        path: 'deliverycenter/projects',
        children: [
          {
            key: 'project',
            name: '项目详情',
            type: 'link',
            icon: <Icon type="line-chart" />,
            path: 'project',
            children: [
              {
                key: 'budget',
                name: '追加预算',
                type: 'link',
                icon: <Icon type="line-chart" />,
                path: 'deliverycenter/budget'
              }
            ]
          }
        ]
      },
      {
        key: 'logs',
        name: __('menu-logs'),
        type: 'link',
        icon: <Icon type="line-chart" />,
        path: 'deliverycenter/logs'
      },
      {
        key: 'tasks',
        name: __('menu-tasks'),
        type: 'link',
        icon: <Icon type="line-chart" />,
        path: 'deliverycenter/tasks',
        children: [
          {
            key: 'task',
            name: '任务详情',
            type: 'link',
            icon: <Icon type="line-chart" />,
            path: 'task'
          }
        ]
      },
      {
        key: 'weeklys',
        name: __('menu-weekly'),
        type: 'link',
        icon: <Icon type="line-chart" />,
        path: 'deliverycenter/weeklys',
        children: [
          {
            key: 'weekly',
            name: '周报详情',
            type: 'link',
            icon: <Icon type="line-chart" />,
            path: 'weekly'
          },
          {
            key: 'editweekly',
            name: '编辑周报',
            type: 'link',
            icon: <Icon type="line-chart" />,
            path: 'editweekly'
          }
        ]
      },
      {
        key: 'applys',
        name: __('menu-apply'),
        type: 'link',
        icon: <Icon type="solution" />,
        path: 'deliverycenter/applys',
        children: [
          {
            key: 'apply',
            name: '申请详情',
            type: 'link',
            icon: <Icon type="line-chart" />,
            path: 'apply'
          }
        ]
      },
      {
        key: 'examines',
        name: __('menu-examine'),
        type: 'link',
        icon: <Icon type="solution" />,
        path: 'deliverycenter/examines',
        children: [
          {
            key: 'examine',
            name: '审批详情',
            type: 'link',
            icon: <Icon type="line-chart" />,
            path: 'examine'
          }
        ]
      },
      {
        key: 'delivery',
        name: __('menu-delivery'),
        type: 'link',
        icon: <Icon type="solution" />,
        path: 'deliverycenter/delivery'
      }
    ]
  }

  render () {
    const locale = cookie.get('language') === 'en_US' ? enUS : zhCN

    return (
      <Provider {...stores}>
        <LocaleProvider locale={locale}>
          <Menus.Provider value={this.menus}>
            <Router history={history}>
              <BasicLayout sideMenu={{ items: this.menus, Link: Link }}>{renderRoutes(router)}</BasicLayout>
            </Router>
          </Menus.Provider>
        </LocaleProvider>
      </Provider>
    )
  }
}
