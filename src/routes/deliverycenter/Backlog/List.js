import __ from '@uyun/utils/i18n'
import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Select, Spin, Table } from '@uyun/components'

import './List.less'

const Option = Select.Option

const radioList = [
  {
    key: '0',
    label: __('state-pending')
  },
  {
    key: '1',
    label: __('state-dispose')
  },
  {
    key: '',
    label: __('state-all')
  }
]
const columns = [
  {
    title: __('task-dispatcher'),
    dataIndex: 'createUser',
    width: '87px',
    align: 'center'
  },
  {
    title: __('newTask-task-name'),
    dataIndex: 'taskTitle',
    width: '826px',
    align: 'center',
    render: (name, row) => <Link to={'/deliverycenter/tasks/task/' + row.id}>{name}</Link>
  },
  {
    title: __('task-state'),
    dataIndex: 'status',
    align: 'center',
    render: type => ((radioList.filter(k => k.key === type))[0] ? (radioList.filter(k => k.key === type))[0].label : '--')
  },
  {
    title: __('stageTask-creation-time'),
    dataIndex: 'createTime',
    align: 'center'
  }
]

@inject('backlogStore')
@observer
export default class BackList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      loading: false,
      params: {
        status: '',
        pageNum: 0,
        pageSize: 10
      }
    }
  }

  componentDidMount () {
    this.getTableData()
  }

  radioChange = (val) => {
    const { params } = this.state
    params.status = val
    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  paginChange = (pagination) => {
    this.setState(state => ({
      params: {
        ...state.params,
        pageSize: pagination.pageSize,
        pageNum: pagination.current
      }
    }), () => {
      this.getTableData()
    })
  }

  getTableData = () => {
    const { backlogStore } = this.props
    this.setState({ loading: true }, () => {
      backlogStore.getBacklogList(this.state.params).then(res => {
        this.setState({ loading: false })
      })
    })
  }

  render () {
    const { params, loading } = this.state
    const tableData = this.props.backlogStore.data || { list: [] }

    return (
      <div className="listBox">
        <header>
          <Select className="select" defaultValue={params.status} allowClear onChange={this.radioChange}>
            {radioList.map((item, i) => {
              return <Option key={i} value={item.key}>{item.label}</Option>
            })}
          </Select>
        </header>
        <Spin spinning={loading}>
          <Table
            rowKey="id"
            dataSource={tableData.list}
            columns={columns}
            pagination={{ total: tableData.total }}
            onChange={this.paginChange}
          />
        </Spin>
      </div>
    )
  }
}
