import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import { Card, Table, DatePicker, Input, Spin } from '@uyun/components'
import { getWorkList } from '@/services/api'

import PageHeader from '@/components/PageHeader'
import __ from '@uyun/utils/i18n'

import './index.less'

const Search = Input.Search
const { WeekPicker } = DatePicker

const columns = [
  {
    title: __('weekly-period'),
    dataIndex: 'weeklyCode',
    render: (name, row) => <Link to={'/deliverycenter/weeklys/weekly/' + row.id}>{name}</Link>
  },
  {
    title: __('weekly-times'),
    dataIndex: 'weeklyStart',
    render: (name, row) => <Link to={'/deliverycenter/weeklys/weekly/' + row.id}>{row.weeklyStart} - {row.weeklyEnd}</Link>
  },
  {
    title: __('budget-project-number'),
    dataIndex: 'projectCode',
    render: (name, row) => <Link to={'/deliverycenter/projects/project/' + row.projectId}>{name}</Link>
  },
  {
    title: __('table-title-project-name'),
    dataIndex: 'projectName'
  },
  {
    title: __('newTask-reporter'),
    dataIndex: 'createUser'
  },
  {
    title: __('weekly-job'),
    dataIndex: 'job'
  },
  {
    title: __('delivery-department'),
    dataIndex: 'department'
  },
  {
    title: __('table-title-project-time'),
    dataIndex: 'createTime'
  }
]

@inject('weeklysStore')
@observer
class ProjectsTable extends Component {
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      search: '',
      loading: false,
      params: {
        keyWord: '',
        weeklyCode: '',
        pageNum: 0,
        pageSize: 10
      }
    }
  }

  componentDidMount () {
    this.getTableData()
  }

  radioChange = (e) => {
    var params = Object.assign({}, this.state.params, { radio: e.target.value })

    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  paginChange = (pagination) => {
    const { params } = this.state

    params.pageSize = pagination.pageSize
    params.pageNum = pagination.current
    
    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  onSearch = (val) => {
    const { params } = this.state
    params.keyWord = val
    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  onClear = () => {
    const { params } = this.state
    params.keyWord = ''
    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  getTableData () {
    const { weeklysStore } = this.props
    this.setState({ loading: true }, () => {
      weeklysStore.getWorkList(this.state.params).then(() => {
        this.setState({ loading: false, tableData: weeklysStore.workObj })
      })
    })
  }

  weekChang = (data, str) => {
    this.setState(state => ({
      params: {
        ...state.params,
        weeklyCode: str
      }
    }), () => {
      this.getTableData()
    })
  }

  render () {
    const { tableData, loading } = this.state

    return (
      <div className="basic-weeklys">
        <PageHeader />

        <header>
          <Search
            placeholder={__('form-item-search')}
            onSearch={this.onSearch}
            allowClear
            onClear={this.onClear}
          />
          <WeekPicker onChange={this.weekChang}/>
        </header>
        <Card
          bordered={false}
          style={{ marginTop: 16 }}
        >
          <Spin spinning={loading}>
            <Table
              rowKey="id"
              dataSource={tableData.list}
              columns={columns}
              pagination={{ total: tableData.total }}
              onChange={this.paginChange}
            />
          </Spin>
        </Card>
      </div>
    )
  }
}

export default ProjectsTable
