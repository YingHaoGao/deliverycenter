import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Button, Row, Col, Table, Collapse, Form } from '@uyun/components'
import { getProjectInfo, postWorkWeekly, getWorkWeekly } from '@/services/api'

import PageHeader from '@/components/PageHeader'
import __ from '@uyun/utils/i18n'

import './index.less'

const columns = {
  resource: [
    {
      title: '姓名',
      dataIndex: 'name',
      key: 'name',
      width: '96px'
    },
    {
      title: '岗位',
      dataIndex: 'station',
      key: 'station',
      width: '150px'
    },
    {
      title: '填报工时(h)',
      dataIndex: 'working',
      key: 'working',
      width: '220px'
    },
    {
      title: '驻地',
      dataIndex: 'encampment',
      key: 'encampment'
    }
  ],
  issue: [
    {
      title: '描述',
      dataIndex: 'riskDescribe',
      key: 'riskDescribe',
      width: '220px'
    },
    {
      title: '影响',
      dataIndex: 'effect',
      key: 'effect',
      render: text => {
        let a = global.params.effectList.filter(k => k.key === text)[0]
        return a ? a.label : ''
      },
      width: '44px'
    },
    {
      title: '活动/补救',
      dataIndex: 'activity',
      key: 'activity',
      width: '290px'
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      render: text => {
        let a = global.params.status1.filter(k => k.key === text)[0]
        return a ? a.label : ''
      },
      width: '50px'
    },
    {
      title: '备注',
      dataIndex: 'remark',
      key: 'remark',
      width: '250px'
    },
    {
      title: '期望解决日期',
      dataIndex: 'expectDate',
      key: 'expectDate'
    }
  ],
  risk: [
    {
      title: '描述',
      dataIndex: 'riskDescribe',
      key: 'riskDescribe',
      width: '220px'
    },
    {
      title: '影响',
      dataIndex: 'effect',
      key: 'effect',
      render: text => {
        let a = global.params.effectList.filter(k => k.key === text)[0]
        return a ? a.label : ''
      },
      width: '44px'
    },
    {
      title: '活动/补救',
      dataIndex: 'activity',
      key: 'activity',
      width: '290px'
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      render: text => {
        let a = global.params.status1.filter(k => k.key === text)[0]
        return a ? a.label : ''
      },
      width: '50px'
    },
    {
      title: '备注',
      dataIndex: 'remark',
      key: 'remark',
      width: '250px'
    },
    {
      title: '期望解决日期',
      dataIndex: 'expectDate',
      key: 'expectDate'
    }
  ],
  deliverable: [
    {
      dataIndex: 'name'
    },
    {
      dataIndex: 'state'
    },
    {
      dataIndex: 'index',
      render: s => {
        return (<Button><a>下载</a></Button>)
      }
    }
  ]
}

var child = {}

@inject('weeklysStore')
@observer
@Form.create()
class ProjectInfo extends Component {
  constructor (props) {
    super(props)

    this.state = {
      projectId: '',
      weeklyId: '',
      visible: false,
      modelTitle: '',
      riskType: '0',
      projectInfo: {},
      weeklyInfo: {
        problemRisks: []
      },
      tableData: {
        resource: [],
        issue: [],
        risk: [],
        deliverable: []
      }
    }
    this.onClose = this.onClose.bind(this)
  }

  componentDidMount () {
    const { match, location } = this.props

    this.setState({ weeklyId: match.params.id }, () => {
      this.getWeekly()
    })
  }

  getProject () {
    const { weeklysStore } = this.props
    var params = { projectId: this.state.projectId }

    weeklysStore.getProjectInfo(params).then(() => {
        this.setState({ projectInfo: weeklysStore.projectInfo })
    })
  }

  getDel = () => {
    const { weeklysStore } = this.props

    weeklysStore.getDeliverableList({ projectId: this.state.projectId }).then(() => {
      this.setState(state => ({
        tableData: {
          ...state.tableData,
          deliverable: weeklysStore.delList
        }
      }))
    })
  }

  getWeekly () {
    const { weeklysStore } = this.props
    var params = { id: this.state.weeklyId }

    weeklysStore.getWorkWeekly(params).then(() => {
      var weeklyInfo = weeklysStore.weeklyInfo
 
      this.setState({ projectId: weeklyInfo.projectId, weeklyInfo: weeklyInfo }, () => {
        this.getProject()
        this.getDel()
      })
    })
  }

  onClose = () => {
    this.setState({ visible: false })
  }

  onOpen = (type) => {
    return (e) => {
      var title = type === '0' ? '新增问题' : '新增风险'
      this.setState({ riskType: type, modelTitle: title, visible: true })
    }
  }

  save = () => {
    const { weeklysStore } = this.props
    const { weeklyInfo } = this.state
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        values.weeklyCode = weeklyInfo.weeklyCode
        values.problemRisks = weeklyInfo.problemRisks
        weeklysStore.postWorkWeekly(values).then(res => {
          if (res.code === 0) {
            this.props.history.push('/weeklys')
          }
        })
      }
    })
  }

  onRef (ref) {
    child = ref
  }

  weekChang = (data, str) => {
    this.setState(state => ({
      weeklyInfo: {
        ...state.weeklyInfo,
        weeklyCode: str
      }
    }))
  }

  desChange = (e) => {
    console.log(e.target.value)
  }

  modalSave = () => {
    child.save(this)
  }

  rtu = (type, name = 'status1') => {
    var a = global.params[name].filter(k => k.key === (type + ''))
    return a[0] ? a[0].label : ''
  }

  render () {
    const { projectInfo, tableData, weeklyInfo } = this.state
    const formItemLayout = {
      labelCol: { span: 1 },
      wrapperCol: { span: 12 }
    }
    const formItemLayout1 = {
      labelCol: { span: 2 },
      wrapperCol: { span: 10 }
    }
    const layout = {
      formLayout: 'horizontal'
    }

    return (
      <div className="basic-weeklyinfo">
        <PageHeader />
        <Form layout={layout}>
          <Row>
            <Col span={20}>
              <header>
                <Form.Item {...formItemLayout1} label="项目名称">
                  <span>{projectInfo.projectName}</span>
                </Form.Item>
                <Form.Item {...formItemLayout1} label="项目编号">
                  <span>{projectInfo.projectName}</span>
                </Form.Item>
                <Form.Item {...formItemLayout1} label="报告人">
                  <span>{projectInfo.manageUser}</span>
                </Form.Item>
                <Row>
                  <Col span={8}>
                    <Form.Item {...formItemLayout1} label="报告周期">
                      {weeklyInfo.weeklyCode}
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item {...formItemLayout1} label="报告时间段">
                      <span>{projectInfo.startTime}-{projectInfo.endTime}</span>
                    </Form.Item>
                  </Col>
                </Row>
              </header>
            </Col>
          </Row>
          <Collapse defaultActiveKey={['0', '1', '2', '3']}>
            <Collapse.Card
              className="close"
              key="0"
              header="项目状态"
              showArrow={false}
            >
              <h3 className="blue">总体</h3>
              <Form.Item {...formItemLayout} label="状态">
                {this.rtu(weeklyInfo.weeklyStatus)}
              </Form.Item>
              <Form.Item label="备注">
                <div className="remark">{weeklyInfo.weeklyDescribe}</div>
              </Form.Item>
              <h3 className="blue">进度</h3>
              <Form.Item {...formItemLayout} label="状态">
                {this.rtu(weeklyInfo.scheduleStatus)}
              </Form.Item>
              <Form.Item label="备注">
                <div className="remark">{weeklyInfo.scheduleDescribe}</div>
              </Form.Item>
              <h3 className="blue">资源</h3>
              <Form.Item {...formItemLayout} label="状态">
                {this.rtu(weeklyInfo.resourceStatus)}
              </Form.Item>
              <Form.Item label="备注">
                <div className="remark">{weeklyInfo.resourceDescribe}</div>
              </Form.Item>
              <h3 className="blue">范围</h3>
              <Form.Item {...formItemLayout} label="状态">
                {this.rtu(weeklyInfo.scopeStatus)}
              </Form.Item>
              <Form.Item label="备注">
                <div className="remark">{weeklyInfo.scopeDescribe}</div>
              </Form.Item>
              <h3 className="blue">交付物</h3>
              <Form.Item {...formItemLayout} label="状态">
                {this.rtu(weeklyInfo.deliverableStatus)}
              </Form.Item>
              <Form.Item label="核心交付物">
                <Table
                  showHeader={false}
                  dataSource={tableData.deliverable}
                  columns={columns.deliverable}
                  pagination={false}
                />
              </Form.Item>
            </Collapse.Card>
            <Collapse.Card
              key="1"
              header="本周资源利用汇总"
              showArrow={false}
            >
              <Table
                rowKey="id"
                dataSource={tableData.resource}
                columns={columns.resource}
                pagination={false}
              />
            </Collapse.Card>
            <Collapse.Card
              key="2"
              header="问题及风险"
              showArrow={false}
            >
              <h3 className="marB">
                <span className="blue">问题</span>
              </h3>
              <Table
                rowKey={(record, index) => index}
                dataSource={weeklyInfo.problemRisks.filter(k => k.type === '0')}
                columns={columns.issue}
                pagination={false}
              />
              <h3 className="marB">
                <span className="blue">风险</span>
              </h3>
              <Table
                rowKey={(record, index) => index}
                dataSource={weeklyInfo.problemRisks.filter(k => k.type === '1')}
                columns={columns.risk}
                pagination={false}
              />
            </Collapse.Card>
            <Collapse.Card
              key="3"
              header="客户重大事件"
              showArrow={false}
            >
              <Form.Item label="描述">
                <div className="remark">{weeklyInfo.customerDescribe}</div>
              </Form.Item>
            </Collapse.Card>
          </Collapse>
        </Form>
      </div>
    )
  }
}

export default ProjectInfo
