import React, { Component } from 'react'
import { Row, Col, Tabs, Button, message } from '@uyun/components'
import PageHeader from '@/components/PageHeader'

import {
  getClueInfoById, getapprovalById, approvalAudit, getCompanyById, getComPanyContactById, getProjectInfoById, getUserClueList
} from '@/services/api'

import './index.less'

const TabPane = Tabs.TabPane

function callback (key) {
  console.log(key)
}
export default class ApplicationContent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      applyId: '',
      type: '0',
      pageType: 'apply',
      flag: 0,
      clueInfo: {},
      approvalInfo: {},
      headerApplyInfo: {},
      clueId: '',
      consinfo: {},
      contactinfo: {},
      approvalUser: '',
      code: ''
    }
  }

  componentWillMount () {
    // const arr = this.props.location.search.split('?')[1].split('&')
    // let applyId, type, pageType, approvalUser, flag, clueId, code
    // arr.map((item, index) => {
    //   var arrinset = item.split('=')
    //   if (arrinset[0] === 'id') {
    //     applyId = arrinset[1]
    //   } else if (arrinset[0] === 'type') {
    //     type = arrinset[1]
    //   } else if (arrinset[0] === 'pageType') {
    //     pageType = arrinset[1]
    //   } else if (arrinset[0] === 'user') {
    //     approvalUser = arrinset[1]
    //   } else if (arrinset[0] === 'flag') {
    //     flag = arrinset[1]
    //   } else if (arrinset[0] === 'clueId') {
    //     clueId = arrinset[1]
    //   } else if (arrinset[0] === 'code') {
    //     code = arrinset[1]
    //   }
    // })

    // this.setState({ applyId, type, pageType, approvalUser, flag, clueId, code }, () => {
    //   // 根据线索ID查询线索信息
    //   getClueInfoById({ id: this.state.clueId }).then((res) => {
    //     // console.log('lvyi===>', res)
    //     if (res.code === 0 && res.resultMap.data.length !== 0) {
    //       this.setState({
    //         clueInfo: res.resultMap.data[0]
    //       })
    //       // console.log(this.state.clueInfo)
    //       // getComPanyContactById({ id: res.resultMap.data[0].companyContactId }).then(res1 => {
    //       //   console.log(res)
    //       //   console.log(res1)
    //       // })
    //       // console.log(this.state.clueInfo, 'clue')
    //     }
    //   })
    //   // 根据线索ID查询审批信息
    //   getapprovalById({ id: this.state.clueId, code: this.state.code }).then(res => {
    //     // console.log('approval===>', res)
    //     if (res.code === 0 && res.resultMap.data.length !== 0) {
    //       this.setState({
    //         approvalInfo: res.resultMap.data.list[0]
    //       })
    //       // console.log(this.state.approvalInfo)
    //       this.getapprovalInfo()
    //     }
    //   })
    // })
  }

  componentDidMount () {
    // console.log(this.state)
    if (!this.state.code) {
      console.log(1)
      this.getApplyInfo()
    } else {
      console.log(2)
      this.getapprovalInfo()
    }
    // this.getApplyInfo()
  }

  // 根据线索id查询申请信息
  getApplyInfo = () => {
    let title = []
    if (this.state.flag === '0') {
      getClueInfoById({ id: this.state.clueId }).then((res) => {
        // console.log(res, 'zmj')
        if (res.code === 0 && res.resultMap.data.length !== 0) {
          title = res.resultMap.data[0]
          this.setState({
            clueInfo: title
          })
        }
        console.log(this.state, 'state')
        getCompanyById({ id: this.state.clueInfo.companyId }).then((res) => {
          console.log(res, 2, this.state.clueInfo)
          this.setState({ consinfo: res.resultMap.data })
        })
        const companyContactId = title.companyContactId
        getComPanyContactById({ id: companyContactId }).then((res) => {
          // console.log(res, 5)
          this.setState({ contactinfo: res.resultMap.data })
          // console.log(this.state.contactinfo)
        })
      })
    } else if (this.state.flag === '1') {
      getProjectInfoById({ id: this.state.clueId }).then((res) => {
        console.log(res, 1)
      })
    }
  }

  // 根据申请id查询申请信息
  getapprovalInfo = () => {
    if (this.state.flag === '0') {
      // console.log('flag=0')
      const data = {
        clueCode: this.state.code,
        userId: window.USER_INFO.userId
      }
      getUserClueList(data).then((res) => {
        // console.log(res)
        if (res.code === 0) {
          this.setState({
            clueInfo: res.resultMap.data.list[0],
            consinfo: res.resultMap.data.list[0]['companyInfo']
          })
        }
      })
      getComPanyContactById({ id: this.state.clueInfo.companyContactId }).then((res) => {
        if (res.code === 0) {
          this.setState({ contactinfo: res.resultMap.data })
        }
      })
    } else if (this.state.flag === '1') {
      getProjectInfoById({ id: this.state.clueId }).then((res) => {
        console.log(res, 1)
      })
    }
  }

  render () {
    const { headerApplyInfo } = this.state
    return (
      <div className="application-page">
        <PageHeader />
        <div className="application-page-title">{headerApplyInfo.applyTitle}</div>
        <div className="application-content">
          <div className="application-top-info">
            <div className="application-main-info">
              <div>PJ1352345324</div>
              <div>项目阶段：<span style={{ color: '#FF9900' }}>{headerApplyInfo.applyType === '1' ? '10%首次交流' : '20%方案建立'}</span></div>
              <div>
                <span>销售负责人：孙权</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span>售前负责人：胡八一</span>
              </div>
            </div>
            <div className="application-right-status">
              <div className="approval-type">
                <div>审批类型</div>
                <div><span>项目立项</span></div>
              </div>
              <div className="project-status">
                <div>状态</div>
                <div><span style={{ color: '#FF9900' }}>待审批</span></div>
              </div>
            </div>
          </div>
          <Tabs defaultActiveKey="审签单" onChange={callback}>
            <TabPane tab="审签单" key="审签单">
              {/* <div className="application-flow">
                <div className="application-flow-title">流程进度</div>
                <Timeline mode="horizontal">
                  <Timeline.Item>发起:售前支持-投标</Timeline.Item>
                  <Timeline.Item>售前负责人:配置'产品模块清单'</Timeline.Item>
                  <Timeline.Item>招标评审组:产品模块清单品胜</Timeline.Item>
                  <Timeline.Item color="green">完成</Timeline.Item>
                </Timeline>
                <Steps progressDot current={1}>
                  <Step title="发起:售前支持-投标" description={<div><p>张三四</p><p>2019-09-11 12:00</p></div>} />
                  <Step title="售前负责人:配置'产品模块清单'" description={<div><p>周毛毛</p><p> </p></div>} />
                  <Step title="招标评审组:产品模块清单品胜" description={<div><p>王五六</p><p> </p></div>} />
                  <Step title="完成" description="完成" />
                </Steps>
              </div> */}
              <div className="customer-basicinfo-box">
                <div className="customer-basicinfo-title">客户信息</div>
                {/* <Row>
                  <Col span={5}>客户名称:北京XXX科技股份有限公司</Col>
                  <Col span={3}>客户类别:普通会员</Col>
                  <Col span={3}>所属行业:金融行业</Col>
                </Row>
                <div>省市地址:北京市海淀区上地西街333号</div>
                <div>客户来源:拜访</div> */}
                <div className="customer-basicinfo-text">
                  <div>
                    <span>客户名称：<span>{this.state.consinfo.companyName}</span></span>
                    <span>客户类别：<span>{this.state.consinfo.companyType}</span></span>
                    <span> 所属行业：<span>{this.state.consinfo.companyIndustry}</span></span>
                  </div>
                  <div>省市地址：<span>{this.state.consinfo.companyAddress}</span></div>
                  <div>客户来源：<span>{this.state.consinfo.companySource}</span></div>
                </div>
                <Row className="linkman-info-header">
                  <Col span={3}>联系人姓名</Col>
                  <Col span={4}>职位</Col>
                  <Col span={4}>部门</Col>
                  <Col span={5}>联系方式</Col>
                  <Col span={8}>邮箱</Col>
                </Row>
                <Row className="linkman-info-body">
                  <Col span={3}>{this.state.contactinfo.contactName}</Col>
                  <Col span={4}>{this.state.contactinfo.contactPosition}</Col>
                  <Col span={4}>{this.state.contactinfo.contactDepartment}</Col>
                  <Col span={5}>{this.state.contactinfo.contactMobile}</Col>
                  <Col span={8}>{this.state.contactinfo.contactEmail}</Col>
                </Row>
              </div>
              <div className="clues-info-box">
                <div className="clues-info-title">线索信息</div>
                <div>&ensp;&ensp;线索主题：{this.state.clueInfo.clueTitle ? this.state.clueInfo.clueTitle : ''}</div>
                <div>&ensp;&ensp;线索信息：</div>
                <div>&ensp;&ensp;{this.state.clueInfo.clueDesc}</div>
              </div>
              <div className="clues-document-accessory">
                <div className="clues-document-title">文档附件</div>
                <Row className="clues-document-header">
                  <Col span={2}>序号</Col>
                  <Col span={13}>文件名称</Col>
                  <Col span={3}>格式</Col>
                  <Col span={3}>附件大小</Col>
                  <Col span={3}>操作</Col>
                </Row>
                {/* <Row className="clues-document-body">
                  <Col span={2}>1</Col>
                  <Col span={13}>XXX项目附件</Col>
                  <Col span={3}>MP4</Col>
                  <Col span={3}>1M</Col>
                  <Col span={3}><a>预览</a>  <a>下载</a></Col>
                </Row> */}
              </div>
            </TabPane>
            <TabPane tab="基本信息" key="基本信息">基本信息</TabPane>
          </Tabs>
        </div>
      </div>
    )
  }
}
