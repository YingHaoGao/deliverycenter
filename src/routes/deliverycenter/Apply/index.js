import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import { Card, Table, Input, Select, Spin } from '@uyun/components'

import PageHeader from '@/components/PageHeader'
import __ from '@uyun/utils/i18n'

import './index.less'

const Search = Input.Search
const { Option } = Select

const radioList = [
  {
    key: '',
    label: __('state-all')
  },
  {
    key: '0',
    label: __('state-onaylanmam')
  },
  {
    key: '1',
    label: __('state-approved')
  }
]
const typeList = [
  {
    key: '',
    label: __('state-all')
  },
  ...global.params.typeList
]
const columns = [
  {
    title: __('table-title-project-name'),
    width: '30%',
    align: 'center',
    dataIndex: 'clue_title',
    render: (name, row) => <Link to={'/deliverycenter/applys/apply/' + row.id}>{name}</Link>
  },
  {
    title: __('apply-type-approval'),
    dataIndex: 'apply_type',
    width: '20%',
    align: 'center',
    render: (type) => {
      let typeObj = typeList.filter(item => item.key === type)

      return (typeObj.length > 0 ? typeObj[0].label : '')
    }
  },
  {
    title: __('deliverables-submitter'),
    dataIndex: 'create_user',
    align: 'center',
    width: '15%'
  },
  {
    title: __('postproject-submission-time'),
    dataIndex: 'apply_time',
    align: 'center',
    width: '20%'
  },
  {
    title: __('apply-status'),
    dataIndex: 'status',
    align: 'center',
    width: '15%',
    render: (status) => {
      const statusObj = radioList.filter(item => item.key === status)

      return (statusObj.length > 0 ? statusObj[0].label : '')
    }
  }
]

@inject('applyStore')
@observer
class Apply extends Component {
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      search: '',
      loading: false,
      params: {
        applyUserId: '',
        companyIndustry: '',
        pageNum: 0,
        pageSize: 10,
        projectName: '',
        applyType: '',
        status: ''
      }
    }
  }

  componentDidMount () {
    this.getTableData()
  }

  radioChange = (val) => {
    const params = Object.assign({}, this.state.params, { status: val })

    this.setState({
      params: params
    }, () => {
      this.getTableData()
    })
  }

  typeChange = (val) => {
    const params = Object.assign({}, this.state.params, { applyType: val })
    this.setState({
      params: params
    }, () => {
      this.getTableData()
    })
  }

  onSearch = (val) => {
    const { params } = this.state
    params.applyUserId = val
    params.companyIndustry = val
    params.projectName = val
    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  onClear = () => {
    const { params } = this.state
    params.applyUserId = ''
    params.companyIndustry = ''
    params.projectName = ''
    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  paginChange = (pagination) => {
    const { params } = this.state
    params.pageSize = pagination.pageSize
    params.pageNum = pagination.current
   
    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  getTableData () {
    const { applyStore } = this.props

    this.setState({ loading: true }, () => {
      applyStore.getApplyList(this.state.params).then(() => {
        this.setState({ tableData: applyStore.applyObj, loading: false })
      })
    })
  }

  render () {
    const { params, tableData, loading } = this.state

    return (
      <div className="basic-Apply">
        <PageHeader />

        <header>
          <Search
            placeholder={__('form-item-search')}
            onSearch={this.onSearch}
            allowClear
            onClear={this.onClear}
          />
          <Select placeholder={__('apply-select-type')} onChange={this.typeChange}>
            {typeList.map(item => {
              return <Option value={item.key} key={item.key}>{item.label}</Option>
            })}
          </Select>
          <Select className="radio" defaultValue={params.status} onChange={this.radioChange}>
            {radioList.map(item => {
              return <Option value={item.key} key={item.key}>{item.label}</Option>
            })}
          </Select>
        </header>
        <Card
          bordered={false}
          style={{ marginTop: 16 }}
        >
          <Spin spinning={loading}>
            <Table
              border
              rowKey="id"
              dataSource={tableData.list}
              columns={columns}
              pagination={{ total: tableData.total }}
              onChange={this.paginChange}
            />
          </Spin>
        </Card>
      </div>
    )
  }
}

export default Apply
