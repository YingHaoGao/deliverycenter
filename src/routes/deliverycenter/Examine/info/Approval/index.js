import React, { Component } from 'react'
import {
  Button, Modal, Form, Input, Row, Col, Radio, Select, DatePicker, Cascader
} from '@uyun/components'
import { inject, observer } from 'mobx-react'
import { mapData } from '../../../../../../public/static/cities'

import './index.less'

const FormItem = Form.Item
const Option = Select.Option
const RadioGroup = Radio.Group
const { TextArea } = Input
const { industry, contractType, contractPart, developer } = global.params

const CollectionCreateForm = Form.create()(
  // eslint-disable-next-line
  class extends Component {
    render () {
      const {
        visible, onCancel, onCreate, form, projectInfo
      } = this.props
      const { getFieldDecorator } = form
      return (
        <Modal
          visible={visible}
          title="
          项目启动"
          onCancel={onCancel}
          onOk={onCreate}
          footer={[
            <Button key="back" onClick={onCancel}>取消</Button>,
            <Button key="submit" type="primary" onClick={onCreate}>确定</Button>
          ]}
        >
          <Form className="ContractReview">
            <Row>项目编号：{projectInfo.projectCode}</Row>
            <FormItem
              label="项目名称：">
              {getFieldDecorator('projectName')(
                <Input
                  style={{ width: 200 }}
                  onChange={this.handleSelectChange}
                />)}
            </FormItem>
            <Row>销售负责人：{projectInfo.applyUser}</Row>
            <FormItem
              label="合同甲方：">
              {getFieldDecorator('contractParty')(
                <Input
                  style={{ width: 200 }}
                  placeholder="请输入合同甲方"
                  onChange={this.handleSelectChange}
                />)}
            </FormItem>
            <FormItem
              label="最终用户：">
              {getFieldDecorator('endUser')(
                <Input
                  style={{ width: 200 }}
                  placeholder="请输入最终用户"
                  onChange={this.handleSelectChange}
                />)}
            </FormItem>
            <FormItem
              label="行业：">
              {getFieldDecorator('industry', {
                rules: [{ required: true, message: '请选择所属行业' }]
              })(
                <Cascader options={industry} />)}
            </FormItem>
            <FormItem
              label="合同类型：">
              {getFieldDecorator('contractType', {
                rules: [{ required: true, message: '请选择所属行业' }]
              })(
                <Cascader options={contractType} />)}
            </FormItem>
            <FormItem
              label="合同分类：">
              {getFieldDecorator('contractPart', {
                rules: [{ required: true, message: '请选择所属行业' }]
              })(
                <Cascader options={contractPart} />)}
            </FormItem>
            <FormItem
              label="实施方：">
              {getFieldDecorator('developer', {
                rules: [{ required: true, message: '请选择所属行业' }]
              })(
                <Cascader options={developer} />)}
            </FormItem>
            <FormItem
              label="项目所在地：">
              {getFieldDecorator('projectAddr', {
                rules: [{ required: true, message: '请选择省市区!' }]
              })(
                <Cascader options={mapData} />
              )}
            </FormItem>
            <FormItem
              label="合同签订日期：">
              {getFieldDecorator('contractDate')(
                <DatePicker
                  style={{ width: 150 }} />)}
            </FormItem>
            <FormItem
              label="是否维保项目：">
              {getFieldDecorator('maintainProject')(
                <RadioGroup>
                  <Radio value={1}>否</Radio>
                  <Radio value={2}>是</Radio>
                </RadioGroup>)}
            </FormItem>
            <Row>备注：</Row>
            <FormItem
              style={{ height: 80 }}
              label="">
              {getFieldDecorator('applyReason', {
                rules: [{ required: true, message: 'Please input the title of collection!' }]
              })(
                <TextArea
                  style={{ width: 400, marginLeft: 25 }}
                  rows={4}
                  onChange={this.handleSelectChange}
                />)}
            </FormItem>
            <div className="communication-log-box">
              <div className="communication-log-title">文档附件
                <Button type="primary">上传</Button>
              </div>
              <Row type="flex" className="communication-log-content">
                <Col span={2} order={1}>序号</Col>
                <Col span={14} order={2}>文件</Col>
                <Col span={6} order={3}>提交</Col>
                <Col span={2} order={4}>操作</Col>
              </Row>
              <Row type="flex" className="communication-log">
                <Col span={2} order={1}>1</Col>
                <Col span={14} order={2}>xx公司 沟通纪要文档0906_1</Col>
                <Col span={6} order={3}>2019-09-07 12:25</Col>
                <Col span={2} order={4} className="delete-communication-log">删除</Col>
              </Row>
              <Row type="flex" className="communication-log">
                <Col span={2} order={1}>1</Col>
                <Col span={14} order={2}>xx公司 沟通纪要文档0906_1</Col>
                <Col span={6} order={3}>2019-09-07 12:25</Col>
                <Col span={2} order={4} className="delete-communication-log">删除</Col>
              </Row>
            </div>
          </Form>
        </Modal>
      )
    }
  }
)

@inject('projectsStore')
@observer
export default class ApprovalComponent extends Component {
  state = {
    visible: false,
    formRef: null
  }

  showModal = () => {
    this.setState({ visible: true })
  }

  handleCancel = () => {
    this.setState({ visible: false })
  }

  handleCreate = () => {
    const form = this.formRef.props.form
    form.validateFields((err, values) => {
      if (err) {
        return
      }
      console.log(values)
      values.applyTitle = '项目启动'
      values.projectId = this.props.projectInfo.projectCode
      values.applyType = '3'
      values.clueId = null
      values.createUser = window.USER_INFO.realname
      values.createUserId = window.USER_INFO.userId
      values.flag = '1'
      values.modeId = '3'
      values.approvalUser = '邹麒'
      values.approvalUserId = 'e0fccc216f434af693cff8b09565473a'
      values.status = '0'
      values.applyJson = '{\"applyTitle\":\"' + values.applyTitle +
                          '\",\"applyType\":\"' + values.applyType +
                          '\",\"contractParty\":\"' + values.contractParty +
                          '\",\"contractType\":\"' + values.contractType +
                          '\",\"contractPart\":\"' + values.contractPart +
                          '\",\"developer\":\"' + values.developer +
                          '\",\"projectAddr\":\"' + values.projectAddr +
                          '\",\"contractDate\":\"' + values.contractDate +
                          '\",\"maintainProject\":\"' + values.maintainProject +
                          '\",\"endUser\":\"' + values.endUser +
                          '\",\"industry\":\"' + values.industry +
                          '\",\"applyReason\":\"' + values.applyReason +
                          '\",\"projectName\":\"' + values.projectName +
                          '\",\"projectId\":\"' + values.projectId +
                          '\",\"applyUser\":\"' + values.applyUser +
                          '\",\"applyUserId\":\"' + values.applyUserId +
                          '\",\"clueId\":\"' + values.clueId +
                          '\",\"applyType\":\"' + values.applyType +
                          '\",\"modeId\":\"' + values.modeId +
                          '\",\"flag\":\"' + values.flag +
                          '\",\"status\":\"' + values.status + '\"}'
      delete values.contractParty
      delete values.contractDate
      delete values.contractPart
      delete values.contractType
      delete values.developer
      delete values.endUser
      delete values.industry
      delete values.maintainProject
      delete values.projectAddr
      delete values.projectName
      projectsStore.postAppAddPid(values).then(() => {
        console.log(projectsStore.pid)
      })
      console.log('Received values of form: ', values)
      form.resetFields()
      this.setState({ visible: false })
    })
  }

  saveFormRef = (formRef) => {
    this.formRef = formRef
  }

  render () {
    return (
      <div>
        <Button type="primary" onClick={this.showModal}>项目启动</Button>
        <CollectionCreateForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
          projectInfo={this.props.projectInfo}
        />
      </div>
    )
  }
}
