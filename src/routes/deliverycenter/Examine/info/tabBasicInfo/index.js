/* eslint-disable */
import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Row, Col, Collapse, Table } from '@uyun/components'
import { exchangeArea } from '@/utils/common'

import './index.less'

const { contractType, contractCategories, customerType, developer, industryArr, budgetType, approvalStatus, stageType: supportType } = global.params

@inject('examineStore')
@observer
export default class TabBasicInfo extends Component {
    constructor (props) {
        super(props)
        this.state = {
          applicationInfo: this.props.applicationInfo,
          projectInfo: this.props.projectInfo,
          budgetInfo: this.props.budgetInfo,
          contractInfo: this.props.contractInfo,
          approvalInfo: this.props.approvalStore,
          applyType: this.props.applyType
        }
    }

    componentWillMount(){
        console.log("props", this.props.applicationInfo)
        console.log("WillMount", this.props.budgetInfo)
    }
    componentDidUpdate (prevProps, prevState){
        // 这里数据更新有点问题，后期有时间可考虑优化
        // console.log("propsDid", this.props.applicationInfo)
        console.log("propsDid", this.props.budgetInfo)
        console.log("this.props.applicationInfo", this.props.applicationInfo)
        // console.log("prevProps", prevProps.applicationInfo)
        // console.log("prevState", prevState.applicationInfo)
        // console.log("prevState", prevState.applicationInfo)
        if(prevProps.applicationInfo !== this.props.applicationInfo){
            // console.log("我来更新")
            this.setState({applicationInfo: this.props.applicationInfo, projectInfo: this.props.projectInfo, approvalInfo: this.props.approvalInfo})
        }
        
    }

    formatIndustryArr = (str) => {
        // 现在给得是'1,1'
        let arr = str.split(',')
        str = industryArr[arr[0]].name + ' ' + industryArr[arr[0]].children[arr[1]]
        return str
    }

  render () {
    const { applicationInfo, projectInfo, budgetInfo, contractInfo, approvalInfo } = this.state
    return (
      <div className="tab-two-content">
          {/* {approvalInfo[0].applyId}
          {projectInfo.projectName} */}
        <Collapse defaultActiveKey={[]} onChange={this.callback3}>
            {
              this.state.applyType === '2'?
              <Collapse.Card header="审签单信息" key="1" style={{ width: '80%' }}>
                <div >
                    {/* 申请说明没了？？不敢去觉得还会加回来 */}
                    {/* <Row>
                        <Col span={4} order={1}>申请说明:</Col>
                        <Col span={8} order={2}>{applicationInfo['applyReason'] ? applicationInfo['applyReason'] : ''}</Col>
                    </Row> */}
                    <Row>
                        <Col span={4} order={3}>合同名称:</Col>
                        <Col span={6} order={4}>{projectInfo['projectName'] ? projectInfo['projectName'] : ''}</Col>
                        <Col span={4} order={1}>项目编号:</Col>
                        <Col span={6} order={2}>{projectInfo['projectCode'] ? projectInfo['projectCode'] : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>销售负责人:</Col>
                        <Col span={6} order={2}>{projectInfo['applyUser'] ? projectInfo['applyUser'] : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={3}>合同甲方:</Col>
                        <Col span={6} order={4}>{applicationInfo['firstParty'] ? applicationInfo['firstParty'] : ''}</Col>
                        <Col span={4} order={1}>最终用户:</Col>
                        <Col span={6} order={2}>{applicationInfo['endUser'] ? applicationInfo['endUser'] : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>所属行业:</Col>
                        <Col span={6} order={2}>{applicationInfo['industry'] ? this.formatIndustryArr(applicationInfo['industry']) : ''}</Col>
                        <Col span={4} order={3}>合同类型:</Col>
                        <Col span={6} order={4}>{applicationInfo['contractType'] ? contractType[applicationInfo['contractType']] : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>合同分类:</Col>
                        <Col span={6} order={2}>{applicationInfo['contractCategories'] ? contractCategories[applicationInfo['contractCategories']] : ''}</Col>
                        <Col span={4} order={3}>客户类别:</Col>
                        <Col span={6} order={4}>{applicationInfo['customerType'] ? customerType[applicationInfo['customerType']] : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>实施方:</Col>
                        <Col span={6} order={2}>{applicationInfo['implementer'] ? developer[applicationInfo['implementer']] : ''}</Col>
                        <Col span={4} order={3}>项目所在地:</Col>
                        <Col span={6} order={4}>{applicationInfo['region'] ? exchangeArea(applicationInfo['region']) : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>履约/质量保证金:</Col>
                        <Col span={6} order={2}>{applicationInfo['bond'] ? applicationInfo['bond'] : ''}</Col>
                        <Col span={4} order={3}>是否含集成商付款条件:</Col>
                        <Col span={6} order={4}>{applicationInfo['isIntegratePayment'] ? (applicationInfo['isIntegratePayment']==1 ? '否' : '是') : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>初验时间:</Col>
                        <Col span={6} order={2}>{applicationInfo['preliminaryInspection'] ? applicationInfo['preliminaryInspection'] : ''}</Col>
                        <Col span={4} order={3}>终验时间:</Col>
                        <Col span={6} order={4}>{applicationInfo['lastVerificationTime'] ? applicationInfo['lastVerificationTime'] : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>质保期:</Col>
                        <Col span={6} order={2}>{applicationInfo['warranty'] ? applicationInfo['warranty'] + '年' : ''}</Col>
                        <Col span={4} order={3}>合同金额:</Col>
                        <Col span={6} order={4}>{applicationInfo['quotationId'] ? applicationInfo['quotationId'] : ''}</Col>
                    </Row>
                    {/* <Row>
                        <Col span={4} order={1}>项目预算:</Col>
                        <Col span={8} order={2}>{applicationInfo['budgetId'] ? applicationInfo['budgetId'] : ''}</Col>
                    </Row> */}
                </div>
            </Collapse.Card>:''
            }
            {
              this.state.applyType === '1'?
              <Collapse.Card header="任务信息" key="1" style={{ width: '80%' }}>
                <div >
                    <Row>
                        <Col span={4} order={1}>支持类型:</Col>
                        <Col span={12} order={2}>{applicationInfo['substageType'] ? supportType[applicationInfo['substageType']] : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>任务名称:</Col>
                        <Col span={12} order={2}>{applicationInfo['taskTitle'] ? applicationInfo['taskTitle'] : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>任务类型:</Col>
                        <Col span={8} order={2}>{applicationInfo['taskType'] ? (applicationInfo['taskType']==='1'?'普通任务':'问题任务') : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>报告人:</Col>
                        <Col span={8} order={2}>{applicationInfo['taskLeader'] ? applicationInfo['taskLeader'] : ''}</Col>
                        <Col span={4} order={3}>任务负责人:</Col>
                        <Col span={8} order={4}>{applicationInfo['reportUserId'] ? applicationInfo['reportUserId'] : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>预计工时（h）:</Col>
                        <Col span={8} order={2}>{applicationInfo['planExpend'] ? applicationInfo['planExpend'] : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>支持时间:</Col>
                        <Col span={8} order={2}>{applicationInfo['startTime'] ? applicationInfo['startTime']+ ' - '+ applicationInfo['endTime']  : ''}</Col>
                    </Row>
                    <Row>
                        <Col span={4} order={1}>说明:</Col>
                        <Col span={8} order={2}>{applicationInfo['planExpend'] ? applicationInfo['planExpend'] : ''}</Col>
                    </Row>
                </div>
                </Collapse.Card>:''
            }
            {
            this.state.applyType === '1'?
            <Collapse.Card header="附件" key="2" style={{ width: '80%' }}>

                    <div className="file-table">
                        <Row className="file-table-header" style={{ fontWeight: 'bold' }}>
                            <Col span={14}>名称</Col>
                            <Col span={6}>操作</Col>
                        </Row>
                        <Row className="file-table-content">
                            <Col span={14}>xx公司 沟通纪要文档0906_1</Col>
                            <Col span={6}>
                                <div className="down-load-file" onClick={(url) => { this.goPage(url) }}>下载</div>
                            </Col>
                        </Row>
                    </div>

            </Collapse.Card>:''
          }
            {
              this.state.applyType === '2'?
              <Collapse.Card header="合同金额" key="2" style={{ width: '80%' }}>
                <div >
                    <Row>
                        <Col span={4} order={5}>报价单:</Col>
                        <Col span={6} order={6}>2019投标一次报价</Col>
                        <Col span={4} order={3}>合同金额:</Col>
                        <Col span={6} order={4}>2,000,000 元</Col>
                    </Row>  
                    {/* {
                        applicationInfo['paymentObject'] ? applicationInfo['paymentObject'].map((item, index) => {
                            return (
                                <Row key = {index}>
                                    <Col span={2} order={5}>名称:</Col>
                                    <Col span={6} order={6}>{item.names ? item.names : ''}</Col>
                                    <Col span={2} order={3}>比例:</Col>
                                    <Col span={6} order={4}>{item.scale ? item.scale : ''}</Col>
                                    <Col span={2} order={1}>付款条件:</Col>
                                    <Col span={6} order={2}>{item.paymentTerms ? item.paymentTerms : ''}</Col>
                                </Row>  
                            )
                        }) : ''
                    } */}
                </div>
              </Collapse.Card>:''
          }
          {
            this.state.applyType === '2'?
            <Collapse.Card header="付款方式" key="3" style={{ width: '80%' }}>
                <div >
                    {
                        applicationInfo['paymentObject'] ? applicationInfo['paymentObject'].map((item, index) => {
                            return (
                                <Row key = {index}>
                                    <Col span={2} order={5}>名称:</Col>
                                    <Col span={6} order={6}>{item.names ? item.names : ''}</Col>
                                    <Col span={2} order={3}>比例:</Col>
                                    <Col span={6} order={4}>{item.scale ? item.scale : ''}</Col>
                                    <Col span={2} order={1}>付款条件:</Col>
                                    <Col span={6} order={2}>{item.paymentTerms ? item.paymentTerms : ''}</Col>
                                </Row>  
                            )
                        }) : ''
                    }
                </div>
            </Collapse.Card>:''
        }
        {
            this.state.applyType === '2'?
            <Collapse.Card header="项目预算" key="4" style={{ width: '80%' }}>
                <div>
                    {
                    //   budgetInfo? budgetInfo.map((item, index) => {
                    //     return (
                    //         <Row>
                    //             <Col span={4} order={5}>{budgetType[item.budget_type]}:</Col>
                    //             <Col span={6} order={6}>{budgetType[item.budget_type] == 3?item.budget_name + '万元':item.budget_name + '人年'}</Col>
                    //         </Row>  
                    //     )
                    //   }) : ''
                    }
                    <Row>
                        <Col span={4} order={5}>合同毛利率:</Col>
                        <Col span={6} order={6}>{contractInfo.grossMargin}</Col>
                    </Row>  
                    <Row>
                        <Col span={4} order={5}>备注:</Col>
                        <Col span={6} order={6}>{contractInfo.remark}</Col>
                    </Row>
                    <div className="file-table">
                        <Row className="file-table-header" style={{ fontWeight: 'bold' }}>
                            <Col span={14}>名称</Col>
                            <Col span={6}>操作</Col>
                        </Row>
                        <Row className="file-table-content">
                            <Col span={14}>xx公司 沟通纪要文档0906_1</Col>
                            <Col span={6}>
                                <div className="down-load-file" onClick={(url) => { this.goPage(url) }}>下载</div>
                            </Col>
                        </Row>
                    </div>
                </div>
            </Collapse.Card>:''
          }
          <Collapse.Card header="审批记录" key="5" style={{ width: '80%' }}>
            <div >
                <Row>
                    <Col span={4} order={5}>申请人:</Col>
                    <Col span={6} order={6}>张三</Col>
                    <Col span={4} order={5}>申请时间:</Col>
                    <Col span={6} order={6}>2019-09-25  12 : 12</Col>
                </Row>
                {/* <Table rowKey= 'id' dataSource= { approvalInfo } columns= { this.state.columns } pagination= { false } /> */}
                <Row className="approval-history-header">
                    <Col span={3} order={1}>审批人</Col>
                    <Col span={4} order={2}>审批结果</Col>
                    <Col span={4} order={3}>审批状态</Col>
                    <Col span={5} order={4}>审批时间</Col>
                </Row>
                {
                approvalInfo? approvalInfo.map((item, index) => {
                return (
                    <Row className="approval-history-content" key={index}>
                        <Col span={3} order={1}>{item['approvalUser']}</Col>
                        <Col span={4} order={2}>{item['approvalStatus']?(item['approvalStatus'] === '1'?'通过':'驳回'):'待审批'}</Col>
                        <Col span={4} order={3}>{item['approvalOpinion']}</Col>
                        <Col span={5} order={4}>{item['approvalTime']}</Col>
                    </Row>
                )
                }) : ''
            }
            </div>
          </Collapse.Card>
        </Collapse>
      </div>
    )
  }
}

TabBasicInfo.defaultProps = {
    applicationInfo: {},
    projectInfo: {},
    budgetInfo: {},
    contractInfo: {},
    approvalInfo: [],
    applyType: '0'
  }
