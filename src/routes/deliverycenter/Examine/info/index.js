/* eslint-disable */
import React, { Component } from 'react'
import { Row, Col, Tabs } from '@uyun/components'
import { inject, observer } from 'mobx-react'
import { getUrlParams } from '@/utils/common'

import './index.less'

import PageHeader from '@/components/PageHeader'
import ApponitTask from './GiveTask'
import TabBasicInfo from './tabBasicInfo'
import ServicePhase from './servicePhase'

const TabPane = Tabs.TabPane
const { subStatus, projectType, successRate } = global.params

@inject('examineStore')
@observer
@inject('projectsStore')
@observer
export default class ApprovalContent extends Component {
  state = {
    applyId: '',
    applyType: ''
  }

  componentDidMount () {
    // const params = getUrlParams(this.props.location.search)
    const { match } = this.props
    this.props.examineStore.getApprovalInfoById({ applyId: match.params.id }).then(() => {
      console.log('approvalInfo: ', this.props.examineStore.approvalInfo)
    })
    
    // // 获取项目信息
    // this.props.projectsStore.getProjectInfo(params.projectId)
    // // 获取合同信息
    // this.props.projectsStore.getContractAttachmentInfo({projectId: params.projectId})
    // this.setState({ applyId: params.id, applyType: params.applyType },
    //   () => { 
    //       // 获取申请信息
    //       this.props.examineStore.getApplyInfo(this.state.applyId, (budgetId)=>{
    //         console.log('项目总表ID', budgetId)
    //         if(!budgetId){
    //            this.props.projectsStore.getBudgetDetail(budgetId, params.projectId)
    //         }
    //       });
    //       // 获取审批信息
    //       this.props.examineStore.getApprovalInfo({ applyId: this.state.applyId }, ()=>{});
    // })
  }

  // componentDidMount () {
  //   console.log("审批", this.props.examineStore)
  //   console.log("projectsStore", this.props.projectsStore)
  // }

  render () {
    const { projectsStore, examineStore } = this.props
    let applicationInfo = examineStore.applicationInfo
    return (
      <div className="approval-page">
        <PageHeader />
        <div className="apponit-task">
          {/* <ApponitTask approvalStatus={1} applyId={this.state.applyId} titleWords="任务指派" />
          <ApponitTask approvalStatus={4} titleWords="驳回" /> */}
          <div className="item-box">
            <ApponitTask applicationInfo={applicationInfo} approvalStatus={1} applyId={this.state.applyId} titleWords="通过审批" />
            <ApponitTask applicationInfo={applicationInfo} approvalStatus={4} titleWords="驳回申请" />
          </div>
          <div className="item-box">
            {/* <div>审批类型：{ examineStore.applicationInfo.applyTitle }</div>
            <div>状态：{ approvalStatus[examineStore.applicationInfo.status] }</div> */}
          </div>
        </div>
        <div className="approval-content">
        {/*
          <div className="approval-info">
            <div className="project-name">{projectsStore.projectInfo.projectName}</div>
            <div className="project-code">{projectsStore.projectInfo.projectCode}</div>
            <Row>
              <Col span={4} order={1}>项目阶段:{subStatus[projectsStore.projectInfo.projectStage]}</Col>
              <Col span={4} order={2}>项目状态:{projectStatus[projectsStore.projectInfo.status]}</Col>
              <Col span={4} order={3}>成功率:{successRate[projectsStore.projectInfo.stageType]}</Col>
              <Col span={4} order={4}>销售负责人:{projectsStore.projectInfo.applyUser}</Col>
              <Col span={4} order={5}>售前负责人:{projectsStore.projectInfo.presalesUser}</Col>
            </Row>
          </div>
        */}
          <div className="approval-details">
            <Tabs defaultActiveKey="审签单">
              <TabPane tab="审签单" key="审签单">
                {/* <div className="customer-basicinfo-box">
                  <div className="customer-basicinfo-title">审签信息</div>
                  <div>申请类型:&ensp;{examineStore.applicationInfo.applyTitle}</div>
                  <div>任务名称:&ensp;{examineStore.applicationInfo.taskTitle}</div>
                  <div>任务类型:&ensp;{examineStore.applicationInfo.taskType === '1' ? '问题任务' : '重要任务'}</div>
                  <div>预计工时(h):&ensp;{examineStore.applicationInfo.planExpend}</div>
                  <div>申请说明:&ensp;{examineStore.applicationInfo.applyReason}</div>
                  <div>所属项目:&ensp;{examineStore.applicationInfo.projectName}</div>
                  <div>申请人姓名:&ensp;{examineStore.applicationInfo.applyUser}</div>
                </div> */}
                {/* 原先得审签单 */}
                <TabBasicInfo applyType= {this.state.applyType} applicationInfo={ applicationInfo } projectInfo= { projectsStore.projectInfo } budgetInfo= { projectsStore.budgetInfo } contractInfo= { projectsStore.contractInfo } approvalInfo= { examineStore.approvalInfo }/>
              </TabPane>
              <TabPane tab="基本信息" key="基本信息">基本信息</TabPane>
              <TabPane tab="售前阶段" key="售前阶段">售前阶段</TabPane>
              <TabPane tab="合同阶段" key="合同阶段">合同阶段</TabPane>
              <TabPane tab="服务阶段" key="服务阶段">
                <ServicePhase projectInfo = {projectsStore.projectInfo}/>
              </TabPane>
            </Tabs>
          </div>
        </div>
      </div>
    )
  }
}
