import React, { Component } from 'react'
import {
  Button, Modal, Form, Input, Select
} from '@uyun/components'
import { inject, observer } from 'mobx-react'

import './index.less'

const FormItem = Form.Item
const { TextArea } = Input
const Option = Select.Option

@inject('examineStore')
@observer
class ApponitTaskComponent extends Component {
  state = {
    visible: false
  }

  componentDidMount () {
    // this.props.examineStore.getTaskPeopleList('售前')
  }

  showModal = () => {
    this.setState({ visible: true })
    this.props.examineStore.getApprovalInfoById({ applyId: this.props.applyId },
      () => {
        this.props.examineStore.getNextApprovalList(this.props.examineStore.approvalInfo[0].workId)
      })
  }

  handleCancel = () => {
    this.setState({ visible: false })
  }

  handleSubmit = (e) => {
    this.setState({ visible: false })
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        values.applyId = this.props.examineStore.approvalInfo.applyId
        values.approvalStatus = this.props.approvalStatus
        values.approvalUserId = window.USER_INFO.userId
        values.currentNode = this.props.examineStore.currentNode
        values.id = this.props.examineStore.approvalInfo.id
        values.modeId = this.props.applicationInfo['modeId']
        values.type = this.props.examineStore.approvalInfo.type
        values.applyType = this.props.applicationInfo['applyType']
        values.workId = this.props.examineStore.approvalInfo.workId
        values.taskLeader = window.USER_INFO.userId
        values.createUserId = this.props.applicationInfo.applyUserId
        values.approvalJson = Object.assign(this.props.applicationInfo, values)
        values.approvalJson = JSON.stringify(values.approvalJson)
        // delete values.taskLeader
        console.log('Received values of form: ', values)
        this.props.examineStore.approvalPreSales(values)
        history.go(-1)
      }
    })
  }

  render () {
    const { visible } = this.state
    const { getFieldDecorator } = this.props.form
    const { titleWords, examineStore } = this.props
    const formItemLayout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 20 }
    }
    return (
      <div>
        <Button type="primary" onClick={this.showModal}>{titleWords}</Button>
        <Modal
          destroyOnClose
          visible={visible}
          title={titleWords}
          okText="确定"
          className="add-childtask-page"
          onCancel={this.handleCancel}
          onOk={this.handleSubmit}
        >
          <Form layout="vertical">
            {
              titleWords === '任务指派' ? <FormItem
                {...formItemLayout}
                label="任务指派给:">
                {getFieldDecorator('taskLeader', {
                  rules: [{ required: true, message: '请选择任务类型' }]
                })(
                  <Select style={{ width: '200px' }}>
                    {
                      examineStore.taskPeopleList.map((item, index) => {
                        return (
                          <Option key={index} value={item.userId}>{item.realname}</Option>
                        )
                      })
                    }
                  </Select>)}
              </FormItem> : ''
            }

            <FormItem label="说明："
              {...formItemLayout}
            >
              {getFieldDecorator('approvalOpinion', {
                rules: [{ required: true, message: '请输入备注信息' }]
              })(
                <TextArea
                  style={{ width: 400 }}
                  rows={4}
                  onChange={this.handleSelectChange}
                />
              )}
            </FormItem>
          </Form>
        </Modal>
      </div>
    )
  }
}

const ApponitTask = Form.create()(ApponitTaskComponent)
export default ApponitTask
