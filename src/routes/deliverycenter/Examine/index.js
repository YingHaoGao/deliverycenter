import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import { Table, Input, Spin, Select } from '@uyun/components'

import PageHeader from '@/components/PageHeader'
import __ from '@uyun/utils/i18n'

import './index.less'

const Search = Input.Search
const { Option } = Select

const columns = [
  {
    title: '项目名称',
    dataIndex: 'name',
    key: 'name',
    width: '30%',
    align: 'center',
    render: (name, row) => <Link to={'/deliverycenter/examines/examine/' + row.apply_id}>{name}</Link>
  },
  {
    title: '审批类型',
    dataIndex: 'apply_type',
    key: 'apply_type',
    width: '20%',
    align: 'center',
    render: text => {
      var i = typeList.filter(e => e.key === text)[0]
      return i ? i.label : ''
    }
  },
  {
    title: '提交人',
    dataIndex: 'apply_name',
    key: 'apply_name',
    align: 'center',
    width: '15%'
  },
  {
    title: '提交时间',
    dataIndex: 'apply_time',
    key: 'apply_time',
    align: 'center',
    width: '20%'
  },
  {
    title: '审批状态',
    dataIndex: 'status',
    key: 'status',
    align: 'center',
    width: '15%',
    render: text => {
      var i = radioList.filter(e => e.key === text)[0]
      return i ? i.label : ''
    }
  }
]
const radioList = [
  {
    key: '',
    label: __('state-all')
  },
  {
    key: '0',
    label: __('state-onaylanmam')
  },
  {
    key: '1',
    label: __('state-approved')
  }
]
const typeList = [
  {
    key: '',
    label: __('state-all')
  },
  ...global.params.typeList
]

@inject('examineStore')
@observer
class BasicTable extends Component {
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      search: '',
      loading: false,
      params: {
        applyUserId: window.USER_INFO.userId,
        companyIndustry: '',
        pageNum: 0,
        pageSize: 10,
        projectName: '',
        status: '',
        applyType: ''
      }
    }
  }

  componentDidMount () {
    this.getTableData()
  }

  radioChange = (val) => {
    var params = Object.assign({}, this.state.params, { status: val })

    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  typeChange = (val) => {
    var params = Object.assign({}, this.state.params, { applyType: val })

    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  paginChange = (pagination) => {
    this.setState(state => ({
      params: {
        ...state.params,
        pageSize: pagination.pageSize,
        pageNum: pagination.current
      }
    }), () => {
      this.getTableData()
    })
  }

  onSearch = (val) => {
    this.setState(state => ({
      params: {
        ...state.params,
        applyUserId: val,
        companyIndustry: val,
        projectName: val
      }
    }), () => {
      this.getTableData()
    })
  }

  onClear = () => {
    this.setState(state => ({
      params: {
        ...state.params,
        applyUserId: '',
        companyIndustry: '',
        projectName: ''
      }
    }), () => {
      this.getTableData()
    })
  }

  getTableData () {
    const { examineStore } = this.props

    this.setState({ loading: true }, () => {
      examineStore.postApprovalList(this.state.params).then(() => {
        this.setState({ tableData: examineStore.examineObj, loading: false })
      })
    })
  }

  render () {
    const { params, tableData, loading } = this.state

    return (
      <div className="basic-examine">
        <PageHeader />
        <header>
          <Search
            placeholder={__('form-item-search')}
            onSearch={this.onSearch}
            allowClear
            onClear={this.onClear}
          />
          <Select placeholder={__('apply-select-type')} onChange={this.typeChange}>
            {typeList.map(item => {
              return <Option value={item.key} key={item.key}>{item.label}</Option>
            })}
          </Select>
          <Select className="radio" defaultValue={params.status} onChange={this.radioChange}>
            {radioList.map(item => {
              return <Option value={item.key} key={item.key}>{item.label}</Option>
            })}
          </Select>
        </header>
        <Spin spinning={loading}>
          <Table
            rowKey="id"
            dataSource={tableData.list}
            columns={columns}
            pagination={{ total: tableData.total }}
            onChange={this.paginChange}
          />
        </Spin>
      </div>
    )
  }
}

export default BasicTable
