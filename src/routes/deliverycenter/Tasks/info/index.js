import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Form, Row, Col, Table, Button, Select, Modal, Popconfirm } from '@uyun/components'

import PageHeader from '@/components/PageHeader'
import __ from '@uyun/utils/i18n'

import PostLog from './PostLog.js'
import EditTask from './EditTask.js'

import './index.less'

const { Option } = Select

const tyleList = global.params.taskStatus
const statusList = global.params.taskStatus

const columns = [
  {
    title: '任务名称',
    dataIndex: 'taskTitle'
  },
  {
    title: '任务类型',
    dataIndex: 'taskType'
  },
  {
    title: '负责人',
    dataIndex: 'createUser'
  },
  {
    title: '计划工时',
    dataIndex: 'planExpend'
  },
  {
    title: '截至时间',
    dataIndex: 'endTime'
  },
  {
    title: '状态',
    dataIndex: 'status',
    render: s => ((statusList.filter(k => k.key === s))[0] ? ( statusList.filter(k => k.key === s))[0].label : '--' ) 
  }
]

const columns1 = [
  {
    title: '提交时间',
    dataIndex: 'createTime'
  },
  {
    title: '填报工时(h)',
    dataIndex: 'h'
  },
  {
    title: '日志内容',
    dataIndex: 'log'
  },
  {
    title: '提交人',
    dataIndex: 'createUser'
  }
]

var child = {}

@inject('taskStore')
@observer
@Form.create()
class TaskInfo extends Component {
  constructor (props) {
    super(props)
    this.state = {
      taskId: '',
      taskInfo: {},
      attachmentInfo: {},
      deliverInfo: {},
      visibleEdit: false,
      editStatus: '0',
      visibleWorking: false,
      subTaskTable: {},
      hourTable: {},
      delCloumn: [
        {
          title: '名称',
          dataIndex: 'deliverableTitle'
        },
        {
          title: 'status',
          dataIndex: 'status',
          render: text => global.params.stageType.find(k => k.key === text).label
        },
        {
          title: 'createUser',
          dataIndex: 'createUser'
        },
        {
          title: 'createTime',
          dataIndex: 'createTime'
        },
        {
          title: '操作',
          render: (text, row) => (
            <div>
              <Popconfirm placement="top" title="删除交付物" onConfirm={this.delDel(item, '1')}>
                删除
              </Popconfirm>
              <span className="info-list-download" onClick={this.downDel(item)}>下载</span>
            </div>
          )
        }
      ],
      subDelCloumn: [
        {
          title: '名称',
          dataIndex: 'deliverableTitle'
        },
        {
          title: 'createUser',
          dataIndex: 'createUser'
        },
        {
          title: 'createTime',
          dataIndex: 'createTime'
        },
        {
          title: '操作',
          render: (text, row) => (
            <div>
              <Popconfirm placement="top" title="删除交付物" onConfirm={this.delDel(item, '1')}>
                删除
              </Popconfirm>
              <span className="info-list-download" onClick={this.downDel(item)}>下载</span>
            </div>
          )
        }
      ],
      attCloumn: [
        {
          title: 'fileName',
          dataIndex: 'fileName'
        },
        {
          title: '操作',
          render: (text, row) => (<a href={row.fileDownloadPath} className="info-list-download">下载</a>)
        }
      ]
    }
    this.onClose = this.onClose.bind(this)
  }

  componentDidMount () {
    const { match } = this.props

    this.setState({ taskId: match.params.id }, () => {
      this.getTask()
    })
  }

  onClose = () => {
    this.setState({ visibleWorking: false, visibleEdit: false })
  }

  showEdit = (editStatus) => {
    return (e) => {
      this.setState({
        visibleEdit: true,
        editStatus
      })
    }
  }

  getTask = () => {
    const { taskStore } = this.props
    var params = { id: this.state.taskId }

    taskStore.getTaskInfo(params).then(() => {
      var taskInfo = taskStore.taskInfo
console.log(taskStore.taskInfo)
      this.setState({ taskInfo: taskInfo }, () => {
        this.getAttaList()
        this.getDelList()
        this.getSubTask()
        this.getHourTable()
      })
    })
  }

  onRef (ref) {
    child = ref
  }

  showWorking = () => {
    this.setState({
      visibleWorking: true
    })
  }

  handleOkWorking = (e) => {
    this.setState({
      visibleWorking: false
    })
  }

  handleCancelWorking = (e) => {
    this.setState({
      visibleWorking: false,
      visibleEdit: false
    })
  }

  getAttaList = () => {
    const { taskStore } = this.props
    taskStore.getAttachmentList({ taskId: this.state.taskInfo.id }).then(() => {
      this.setState({ attachmentInfo: taskStore.attachmentInfo })
    })
  }

  getDelList = () => {
    const { taskStore } = this.props
    taskStore.getTaskDeliverList({ taskId: this.state.taskInfo.id }).then(res => {
      this.setState({ deliverInfo: taskStore.deliverInfo })
    })
  }

  getHourTable = () => {
    const { taskInfo } = this.state
    const { taskStore } = this.props
    taskStore.postWorkLogList(taskInfo).then(() => {
      this.setState({ hourTable: taskStore.hourTable })
    })
  }

  getSubTask = () => {
    const { taskInfo } = this.state
    const { taskStore } = this.props
    taskStore.getTaskList({ projectId: taskInfo.projectId, taskLeader: taskInfo.taskLeader }).then(res => {
      this.setState({ subTaskTable: taskStore.subTask })
    })
  }

  onDel = () => {
    const { taskStore } = this.props
    taskStore.delTask({ id: this.state.taskInfo.id }).then(() => {
      this.props.history.push('/tasks')
    })
  }

  delDel = (item, type = '1') => {
    const { taskStore, taskInfo } = this.props
    return () => {
      var f = this.state.fileList.filter(k => k.file === item.file)

      if (f[0]) {
        if (type === '1') {
          taskStore.delFile({
            createUser: window.USER_INFO.realname,
            createUserId: window.USER_INFO.userId,
            deliverableTitle: item.deliverableTitle,
            substageId: taskInfo.substageId
          }).then(() => {
            if (taskStore.delFileFn) {
              this.setState((state) => {
                return {
                  fileList: state.fileList.filter(k => k.file !== item.file)
                }
              })
            }
          })
        } else {

        }
      }
    }
  }

  typeChange = (val) => {
    const { taskStore } = this.props
    var _this = this
    Modal.confirm({
      iconType: '',
      content: '是否修改任务状态',
      okText: '确认',
      cancelText: '取消',
      onOk: function () {
        var taskInfo = { ..._this.props.taskInfo, status: val }
        taskStore.postTaskChangeStatus(taskInfo).then(() => {
          if (taskStore.statusCode === 0) {
            _this.props.history.push('/tasks')
          }
        })
      }
    })
  }

  downDel = (del) => {
    return () => {

    }
  }

  handleWorking = () => {
    child.save(this)
  }

  filter = (list, v) => {
    var arr = list.filter(k => k.key === v)
    return arr[0] ? arr[0].label : '--'
  }

  render () {
    const { visibleEdit, delCloumn, subDelList, attCloumn, subDelCloumn, editStatus, visibleWorking, taskInfo, attachmentInfo, deliverInfo, subTaskTable, hourTable } = this.state
    const { attList = [] } = attachmentInfo
    const { delList = [] } = deliverInfo
    return (
      <div className="basic-taskInfo">
        <PageHeader />
        <div className="title">
          <h2>需求文档编写</h2>
          <div>
            <Popconfirm title="确认删除？" onConfirm={this.onDel}>
              <span className="title-del">删除</span>
            </Popconfirm>
            <span className="title-edit" onClick={this.showEdit('0')}>编辑</span>
            <Select value={taskInfo.taskType} className="title-select" onChange={this.typeChange}>
              {statusList.map(item => {
                return <Option value={item.key} key={item.key}>{item.label}</Option>
              })}
            </Select>
            <Button onClick={this.showWorking}>填报工时</Button>
          </div>
        </div>
        <div>
          <Row>
            <Col span={12}>
              <div className="info">
                <span>所属项目：</span>
                <Link to={'project/' + taskInfo.projectId}>{taskInfo.projectName}</Link>
              </div>
              <div className="info">
                <span>所属任务：</span>
                <Link to={'task/' + taskInfo.id}>{taskInfo.taskTitle}</Link>
              </div>
              <div className="info">
                <span>所属子阶段：</span>
                <span>{taskInfo.substageTitle || '--'}</span>
              </div>
              <div className="info">
                <span>任务类型：</span>
                <span>{this.filter(tyleList, taskInfo.taskType)}</span>
              </div>
            </Col>
            <Col span={12}>
              <div className="state">
                <div className="state-title">状态</div>
                <div className="state-text">{this.filter(statusList, taskInfo.taskType)}</div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <div className="info">
                <span>关联收入点：</span>
                <span>{taskInfo.incomeId || '--'}</span>
              </div>
              <div className="info">
                <span>创建人：</span>
                <span>{taskInfo.createUser || '--'}</span>
              </div>
              <div className="info">
                <span>报告人：</span>
                <span>{taskInfo.reportUser || '--'}</span>
              </div>
              <div className="info">
                <span>实际/预计工时(h)：</span>
                <span>{taskInfo.actualExpend || '--'} / {taskInfo.planExpend || '--'}</span>
              </div>
              <div className="info">
                <span>计划开始/计划完成：</span>
                <span>{taskInfo.startTime || '--'} / {taskInfo.endTime || '--'}</span>
              </div>
            </Col>
            <Col span={8}>
              <div className="info">
                <span>关联回款点：</span>
                <span>--</span>
              </div>
              <div className="info">
                <span>创建时间：</span>
                <span>{taskInfo.createTime || '--'}</span>
              </div>
              <div className="info">
                <span>任务负责人：</span>
                <span>{taskInfo.createUser || '--'}</span>
              </div>
              <div className="info">
                <span>实际完成：</span>
                <span>{taskInfo.actualTime || '--'}</span>
              </div>
            </Col>
          </Row>
          <div className="info">
            <span>任务描述：</span>
            <span>{taskInfo.taskDesc || '--'}</span>
          </div>
          <div className="info-list">
            <div className="info-list-title">
              <h2>附件</h2>
            </div>
            <ul className="info-list-content">
              <Table
                rowKey="id"
                pagination={false}
                showHeader={false}
                dataSource={attList}
                columns={attCloumn}
              />
            </ul>
          </div>
          <div className="info-list">
            <div className="info-list-title">
              <h2>核心交付物</h2>
            </div>
            <ul className="info-list-content">
              <Table
                rowKey="id"
                pagination={false}
                showHeader={false}
                dataSource={delList}
                columns={delCloumn}
              />
            </ul>
          </div>
          <div className="info-list">
            <div className="info-list-title">
              <h2>子任务</h2>
              <span onClick={this.showEdit('1')}>添加子任务</span>
            </div>
            <div className="info-list-table">
              <Table
                rowKey="id"
                pagination={false}
                dataSource={subTaskTable.list}
                columns={columns}
              />
            </div>
          </div>
          <div className="info-list">
            <div className="info-list-title">
              <h2>交付物</h2>
            </div>
            <Table
              rowKey="id"
              pagination={false}
              showHeader={false}
              dataSource={subDelList}
              columns={subDelCloumn}
            />
          </div>
          <div className="info-list">
            <div className="info-list-title">
              <h2>工时记录</h2>
            </div>
            <div className="info-list-table">
              <Table
                rowKey="id"
                dataSource={hourTable.list}
                columns={columns1}
              />
            </div>
          </div>
        </div>
        <Modal
          title={ (editStatus === '0' ? '编辑任务' : '新建子任务') }
          visible={visibleEdit}
          onOk={this.handleWorking}
          onCancel={this.handleCancelWorking}
          className="create-subtask"
        >
          <EditTask edit={editStatus} taskInfo={taskInfo} onRef={this.onRef} />
        </Modal>
        <Modal
          title="工作日志填报"
          visible={visibleWorking}
          onOk={this.handleWorking}
          onCancel={this.handleCancelWorking}
          className="working-hours"
        >
          <PostLog onRef={this.onRef} />
        </Modal>
      </div>
    )
  }
}

export default TaskInfo
