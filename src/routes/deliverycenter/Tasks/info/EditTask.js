import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Form, DatePicker, Input, InputNumber, Button, Upload, Select, Row, Col, Table } from '@uyun/components'
import { postTaskAdd, postFileUpload, delFile, postListUsers, getTaskInfo } from '@/services/api'
import moment from 'moment'
import './index.less'

const { TextArea } = Input
const { RangePicker } = DatePicker
const Option = Select.Option


const columns = [
  {
    title: '名称',
    dataIndex: 'substageTitle',
    key: 'substageTitle'
  }
]
const getTimes = () => {
  var date = new Date()
  var timestamp = date.getTime()
  var plus = '-'
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var strDate = date.getDate()

  var eDate = new Date(timestamp - 31536000000)
  var eYear = eDate.getFullYear()
  var eMonth = eDate.getMonth() + 1
  var eStrDate = eDate.getDate()

  if (month >= 1 && month <= 9) {
    month = '0' + month
  }
  if (strDate >= 1 && strDate <= 9) {
    strDate = '0' + strDate
  }
  if (eMonth >= 1 && eMonth <= 9) {
    eMonth = '0' + eMonth
  }
  if (eStrDate >= 1 && eStrDate <= 9) {
    eStrDate = '0' + eStrDate
  }

  var endTime = year + plus + month + plus + strDate
  var startTime = eYear + plus + eMonth + plus + eStrDate

  return [startTime, endTime]
}

@inject('taskStore')
@observer
@Form.create()
class ModalTable extends Component {
	constructor (props) {
    super(props)
    var times = getTimes()

    this.state = {
      startDate: times[0],
      endDate: times[1],
    	fileList: [],
    	accessory: [],
      selDel: [],
      unDelList : [],
      conIncomeList: [],
      managerList: [],
      conPayList: [],
      taskParenInfo: {},
    	uploading: false,
    	accUploading: false
    }
  }

  componentDidMount () {
    this.props.onRef(this)
    this.getPay()
    this.getUnDel()
    this.getIncome()
    this.getPrincipal()
    this.getTask()
  }

  getTask = () => {
    const { taskStore, taskInfo } = this.props

    if (taskInfo.parentId !== '0') {
      var params = { id: taskInfo.parentId }

      taskStore.getTaskInfo(params).then(res => {
        var taskParenInfo = taskStore.taskInfo
        this.setState({ taskParenInfo: taskParenInfo })
      })
    }
  }

  save = (_this) => {
    const { taskStore, taskInfo, edit } = this.props
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {

        var data = {}
        // var filePath = this.state.fileList.map(k => k.path.filePath)
        var deliverable = this.state.selDel.map(k => k.id)
        data.startTime = moment(values.time[0]).format('YYYY-MM-DD HH:MM:ss') || ''
        data.endTime = moment(values.time[1]).format('YYYY-MM-DD HH:MM:ss') || ''
        // data.filePath = filePath.join(',')
        data.incomeId = values.incomeId
        data.createUser = window.USER_INFO.realname
        data.createUserId = window.USER_INFO.userId
        data.deliverable = deliverable.join(',')
        data.isCoreDeliverors = (values.deliverable || []).length > 0 ? 1 : 0
        data.projectId = taskInfo.projectId
        data.paymentId = values.paymentId
        data.planExpend = values.planExpend
        data.relatedDeliverable = (values.deliverable || []).length
        // data.uploadDeliverable = (filePath || []).length
        console.log(this.state.managerList.find(k => k.userId === values.reportUserId))
        data.reportUser = this.state.managerList.find(k => k.userId === values.reportUserId).realname
        data.reportUserId = values.reportUserId
        data.substageId = taskInfo.substageId
        data.taskDesc = values.taskDesc
        console.log(this.state.managerList.find(k => k.userId === values.taskLeaderId))
        data.taskLeaderName = this.state.managerList.find(k => k.userId === values.taskLeaderId).realname
        data.taskLeader = values.taskLeaderId
        data.taskTitle = values.taskTitle
        data.taskType = values.taskType
        data.parentId = taskInfo.id

        // this.state.fileList.map(item => {
        // 	value.deliverables.push({
        // 		attachment: {
        // 			fileDownloadPath: item.path.filePath,
        // 			fileName: item.file.name
        // 		},
        // 		createTime: getTimes()[1],
        // 		createUser: window.USER_INFO.realname,
        // 		createUserId: window.USER_INFO.userId,
        // 		deliverableTitle: item.file.name,
        // 		filepath: item.path.filePath,
        // 		substageId: _this.state.taskInfo.substageId
        // 	})
        // })
        // value.isCoreDeliverors = this.state.fileList.length > 0 ? '0' : '1'
        // value.relatedDeliverable = this.state.fileList.length
        // value.uploadDeliverable = this.state.fileList.length

        if (edit === '0') {
          data.id = taskInfo.id
          taskStore.editTaskInfo(data).then(() => {
            taskStore.getTaskInfo({ id: taskInfo.id })
            _this.onClose()
          })
        } else {
          taskStore.postTaskAdd(data).then(() => {
            taskStore.getTaskInfo({ id: taskInfo.id })
            _this.onClose()
          })
        }
      }
    })
  }

  // 未关联的交付物
  getUnDel = () => {
    const { taskInfo, taskStore, stageInfo } = this.props

    taskStore.getUnDelList({ subStageId: taskInfo.subStageId }).then(() => {
      console.log('unDelList', taskStore.unDelList)
      this.setState({ unDelList: taskStore.unDelList })
    })
  }

  // 收入点
  getIncome = () => {
    const { taskStore } = this.props
    taskStore.getConIncomeList({}).then(() => {
      console.log('incom', taskStore.conIncomeList)
      this.setState({ conIncomeList: taskStore.conIncomeList })
    })
  }

  // 回款点
  getPay = () => {
    const { taskStore } = this.props
    taskStore.getConPayList({}).then(() => {
      var conPayList = [{ id: '', paymentStage: '不关联' }]
      taskStore.conPayList.map(item => {
        if (item) {
          conPayList.push(item)
        }
      })
      console.log('pay:',conPayList)
      this.setState({ conPayList: conPayList })
    })
  }

  delDel = (item, type = '1') => {
  	return () => {
  		var f = this.state.fileList.filter(k => k.file === item.file)

    	if (f[0]) {
    		if (type === '1') {
    			delFile({ filepath: f[0].path.filePath }).then(res => {
	    			if (res.code === 0) {
	    				this.setState((state) => {
			          return {
			            fileList: state.fileList.filter(k => k.file !== item.file)
			          }
			        })
	    			}
	    		})
    		} else {

    		}
    	}
  	}
  }

  getPrincipal = () => {
    const { taskStore } = this.props
    taskStore.getManagerNameList({ deptName : '交付中心', tenantId: window.USER_INFO.tenantId }).then(() => {
      this.setState({ managerList: taskStore.manNameList })
    })
  }

	render () {
    const { edit, taskInfo } = this.props
		const { getFieldDecorator } = this.props.form
		const { uploading, fileList, unDelList , conIncomeList, accessory, accUploading, managerList, taskParenInfo, conPayList } = this.state
    const { paymentType, taskType } = global.params

    const props1 = {
      beforeUpload: (file) => {
        this.setState({ accUploading: true })
        const formData = new FormData()
        formData.append('file', file)

        postFileUpload(formData).then(res => {
          if (res.code === 0) {
            this.setState(state => ({
              accessory: [...state.accessory, { file, path: res.resultMap.data }]
            }))
          }
          this.setState({ accUploading: false })
        })
        return false
      },
      fileList: []
    }
    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        this.setState({ selDel: selectedRows })
      }
    };

		return (
      <Form layout="inline" className="modalTable">
        {
          taskInfo.taskType === '0'
          ? (
            <div className="create-subtask-info">
              <Form.Item label="任务名称">
                {taskInfo.taskTitle}
              </Form.Item>
            </div>
          )
          : (
            <div className="create-subtask-info">
              <Form.Item label="任务名称">
                {getFieldDecorator('taskTitle', { initialValue: edit === '0' ? taskInfo.taskTitle : '', rules: [{ required: true, message: '必填项!' }] })(<Input />)}
              </Form.Item>
            </div>
          )
        }
        {
          edit === '0'
          ? <div className="create-subtask-info">
            <Form.Item label="子阶段">
              {taskInfo.substageTitle}
            </Form.Item>
          </div>
          : <div className="create-subtask-info">
            <Form.Item label="所属任务">
              {taskParenInfo.taskTitle}
            </Form.Item>
          </div>
        }
        <Row>
          <Col span={12}>
            <div className="create-subtask-info">
              <Form.Item label="关联回款点">
                {getFieldDecorator('paymentId', { initialValue: edit === '0' ? taskInfo.paymentId : '' })(
                  <Select className="create-subtask-info-select">
                    {conPayList.map(item => {
                      return <Option value={item.id} key={item.id}>{item.paymentStage}</Option>
                    })}
                  </Select>
                )}
              </Form.Item>
            </div>
          </Col>
          <Col span={12}>
            <div className="create-subtask-info">
              <Form.Item label="关联收入点">
                {getFieldDecorator('incomeId', { initialValue: edit ? taskInfo.incomeId : '' })(
                  <Select className="create-subtask-info-select">
                    {conIncomeList.map(item => {
                      return <Option value={item.id} key={item.id}>{item.incomeTitle}</Option>
                    })}
                  </Select>
                )}
              </Form.Item>
            </div>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <div className="create-subtask-info">
              <Form.Item label="任务负责人">
                {getFieldDecorator('taskLeaderId', { initialValue: edit === '0' ? taskInfo.taskLeader : '', rules: [{ required: true, message: '必填项!' }] })(
                  <Select className="create-subtask-info-select">
                    {managerList.map(item => {
                      return <Option value={item.userId} key={item.userId}>{item.realname}</Option>
                    })}
                  </Select>
                )}
              </Form.Item>
            </div>
          </Col>
          <Col span={12}>
            <div className="create-subtask-info">
              <Form.Item label="任务类型">
                {getFieldDecorator('taskType', { initialValue: edit === '0' ? taskInfo.taskType : '', rules: [{ required: true, message: '必填项!' }] })(
                  <Select className="create-subtask-info-select">
                    {taskType.map(item => {
                      return <Option value={item.key} key={item.key}>{item.label}</Option>
                    })}
                  </Select>
                )}
              </Form.Item>
            </div>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <div className="create-subtask-info">
              <Form.Item label="报告人">
                {getFieldDecorator('reportUserId', { initialValue: edit === '0' ? taskInfo.reportUserId : '', rules: [{ required: true, message: '必填项!' }] })(
                  <Select className="create-subtask-info-select">
                    {managerList.map(item => {
                      return <Option value={item.userId} key={item.userId}>{item.realname}</Option>
                    })}
                  </Select>
                )}
              </Form.Item>
            </div>
          </Col>
          <Col span={12}>
            <div className="create-subtask-info">
              <Form.Item label="预计工时(h)">
                {getFieldDecorator('planExpend', { initialValue: edit === '0' ? taskInfo.planExpend : '', rules: [{ required: true, message: '必填项!' }] })(
                  <InputNumber />
                )}
              </Form.Item>
            </div>
          </Col>
        </Row>
        <div className="create-subtask-info">
          <Form.Item label="计划开始/计划完成">
            {getFieldDecorator('time', {
              initialValue: edit === '0' ? [moment(taskInfo.startTime, 'YYYY-MM-DD'), moment(taskInfo.endTime, 'YYYY-MM-DD')] : null,
              rules: [{ required: true, message: '必填项!' }]
            })(
              <RangePicker />
            )}
          </Form.Item>
        </div>
        <div className="create-subtask-info">
          <Form.Item label="任务描述">
            {getFieldDecorator('taskDesc', {
              initialValue: edit === '0' ? taskInfo.taskDesc : '',
              rules: [{ required: true, message: '必填项!' }]
            })(
              <TextArea className="create-subtask-info-textArea" />
            )}
          </Form.Item>
        </div>
        <div className="create-subtask-list">
          <div className="create-subtask-list-title">
            <h2>核心交付物</h2>
          </div>
          <Table
            columns={columns}
            rowSelection={rowSelection}
            pagination={false}
            showHeader={false}
            dataSource={unDelList} />
        </div>
        <div className="create-subtask-list">
          <div className="create-subtask-list-title">
            <h2>附件</h2>
            <Upload {...props1}>
              <Button type="primary" loading={accUploading}>上传</Button>
            </Upload>
          </div>
          <ul>
            {accessory.map((item, i) => {
              return (
                <li key={i}>
                  <span>{item.file.name}</span>
                  <span className="working-hours-list-del" onClick={this.delDel(item, '2')}>删除</span>
                </li>
              )
            })}
          </ul>
        </div>
      </Form>
    )
	}
}

export default ModalTable