import React, { Component } from 'react'
import { Form, DatePicker, Input, Button, Upload } from '@uyun/components'
import { postWork, postFileUpload, delFile } from '@/services/api'
import './index.less'

const { TextArea } = Input

const getTimes = () => {
  var date = new Date()
  var timestamp = date.getTime()
  var plus = '-'
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var strDate = date.getDate()

  var eDate = new Date(timestamp - 31536000000)
  var eYear = eDate.getFullYear()
  var eMonth = eDate.getMonth() + 1
  var eStrDate = eDate.getDate()

  if (month >= 1 && month <= 9) {
    month = '0' + month
  }
  if (strDate >= 1 && strDate <= 9) {
    strDate = '0' + strDate
  }
  if (eMonth >= 1 && eMonth <= 9) {
    eMonth = '0' + eMonth
  }
  if (eStrDate >= 1 && eStrDate <= 9) {
    eStrDate = '0' + eStrDate
  }

  var endTime = year + plus + month + plus + strDate
  var startTime = eYear + plus + eMonth + plus + eStrDate

  return [startTime, endTime]
}

@Form.create()
class WorkDate extends Component {
	constructor (props) {
    super(props)

    this.state = {
    	workDate: getTimes()[1],
    	fileList: [],
    	accessory: [],
    	uploading: false,
    	accUploading: false
    }
  }

  componentDidMount () {
    this.props.onRef(this)
  }

  save = (_this) => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        values.workDate = values.workDate.format('YYYY-MM-DD')
        values.createTime = getTimes()[1]
        values.createUserId = window.USER_INFO.userId
        values.createUser = window.USER_INFO.realname
        values.substageId = _this.state.taskInfo.substageId
        values.deliverables = []
        this.state.fileList.map(item => {
        	values.deliverables.push({
        		attachment: {
        			fileDownloadPath: item.path.filePath,
        			fileName: item.file.name
        		},
        		createTime: getTimes()[1],
        		createUser: window.USER_INFO.realname,
        		createUserId: window.USER_INFO.userId,
        		deliverableTitle: item.file.name,
        		filepath: item.path.filePath,
        		substageId: _this.state.taskInfo.substageId
        	})
        })

        postWork(values).then(res => {
        	if (res.code === 0) {
        		_this.onClose()
        	}
        })
      }
    })
  }

  delDel = (item, type = '1') => {
  	return () => {
  		var f = this.state.fileList.filter(k => k.file === item.file)

    	if (f[0]) {
    		if (type === '1') {
    			delFile({ filepath: f[0].path.filePath }).then(res => {
	    			if (res.code === 0) {
	    				this.setState((state) => {
			          return {
			            fileList: state.fileList.filter(k => k.file !== item.file)
			          }
			        })
	    			}
	    		})
    		} else {

    		}
    	}
  	}
  }

	render () {
		const { getFieldDecorator } = this.props.form
		const { uploading, fileList, accessory, accUploading } = this.state
    const props = {
      beforeUpload: (file) => {
      	this.setState({ uploading: true })
      	const formData = new FormData()
		    formData.append('file', file)

      	postFileUpload(formData).then(res => {
      		if (res.code === 0) {
      			this.setState(state => ({
		          fileList: [...state.fileList, { file, path: res.resultMap.data }]
		        }))
      		}
      		this.setState({ uploading: false })
      	})
        return false
      },
      fileList: []
    }
    const props1 = {
      beforeUpload: (file) => {
      	this.setState({ accUploading: true })
      	const formData = new FormData()
		    formData.append('file', file)

      	postFileUpload(formData).then(res => {
      		if (res.code === 0) {
      			this.setState(state => ({
		          accessory: [...state.accessory, { file, path: res.resultMap.data }]
		        }))
      		}
      		this.setState({ accUploading: false })
      	})
        return false
      },
      fileList: []
    }

		return (
      <Form>
        <div className="working-hours-info">
          <Form.Item label="填报日期">
            {getFieldDecorator('workDate', { initValue: this.state.workDate })(<DatePicker />)}
          </Form.Item>
        </div>
        <div className="working-hours-info">
          <Form.Item label="填报工时(h)">
            {getFieldDecorator('actualExpend')(<Input />)}
          </Form.Item>
        </div>
        <div className="working-hours-info">
          <Form.Item label="任务描述">
            {getFieldDecorator('workDesc')(<TextArea />)}
          </Form.Item>
        </div>
        <div className="working-hours-list">
          <div className="working-hours-list-title">
            <h2>核心交付物</h2>
            <Upload {...props}>
            	<Button loading={uploading} type="primary">上传</Button>
            </Upload>
          </div>
          <ul>
          	{fileList.map((item, i) => {
          		return (
          			<li key={i}>
		              <span>{item.file.name}</span>
		              <span className="working-hours-list-del" onClick={this.delDel(item)}>删除</span>
		            </li>
          		)
          	})}
          </ul>
          <div className="working-hours-list-title">
            <h2>附件</h2>
            <Upload {...props1}>
            	<Button type="primary" loading={accUploading}>上传</Button>
            </Upload>
          </div>
          <ul>
          	{accessory.map((item, i) => {
          		return (
          			<li key={i}>
		              <span>{item.file.name}</span>
		              <span className="working-hours-list-del" onClick={this.delDel(item, '2')}>删除</span>
		            </li>
          		)
          	})}
          </ul>
        </div>
      </Form>
    )
	}
}

export default WorkDate