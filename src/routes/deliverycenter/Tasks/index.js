import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import { Table, DatePicker, Input, Spin, Select, Form, Row, Button, Col, Icon, } from '@uyun/components'
import moment from 'moment'

import PageHeader from '@/components/PageHeader'
import __ from '@uyun/utils/i18n'

import './index.less'

const { RangePicker } = DatePicker
const { Search } = Input
const { Option } = Select

var taskStatusList = global.params.taskStatus
taskStatusList.unshift({
  key: '',
  label: '全部'
})

const columns = [
  {
    title: __('stageTask-task-name'),
    dataIndex: 'taskTitle',
    render: (name, row) => <Link to={'/deliverycenter/tasks/task/' + row.id}>{name}</Link>
  },
  {
    title: __('table-title-project-number'),
    dataIndex: 'projectCode',
    render: (name, row) => <Link to={'/deliverycenter/projects/project/' + row.id}>{name}</Link>
  },
  {
    title: __('table-title-project-name'),
    dataIndex: 'projectName'
  },
  {
    title: __('task-state'),
    dataIndex: 'status',
    render: status => {
      let text = ''
      switch (status) {
        case '0':
          text = '未开始'
          break
        case '1':
          text = '进行中'
          break
        case '2':
          text = '已完成'
          break
        case '3':
          text = '已关闭'
          break
      }
      return <span>{text}</span>
    }
  },
  {
    title: __('task-subphase-name'),
    dataIndex: 'substageTitle'
  },
  {
    title: __('task-is-overdue'),
    dataIndex: 'is',
    render: endTime => {
      const endDate = new Date(endTime)
      const getEndTime = endDate.getTime()
      const nowData = new Date()
      const getNowData = nowData.getTime()
      return <span>{getNowData > getEndTime ? '是' : '否'}</span>
    }
  },
  {
    title: __('task-owner'),
    dataIndex: 'taskLeaderName'
  },
  {
    title: '报告人',
    dataIndex: 'createUser'
  },
  {
    title: '是否关联节点',
    dataIndex: 'isPayment',
    render: (isPayment) => {
      return <span>{isPayment === '0' ? '未关联' : '已关联'}</span>
    }
  },
  {
    title: __('task-schedule-start-time'),
    dataIndex: 'startTime'
  },
  {
    title: __('task-schedule-end-time'),
    dataIndex: 'endTime'
  },
  {
    title: __('task-create-time'),
    dataIndex: 'createTime'
  }
]
const getTimes = () => {
  var date = new Date()
  var timestamp = date.getTime()
  var plus = '-'
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var strDate = date.getDate()

  var eDate = new Date(timestamp - 31536000000)
  var eYear = eDate.getFullYear()
  var eMonth = eDate.getMonth() + 1
  var eStrDate = eDate.getDate()

  if (month >= 1 && month <= 9) {
    month = '0' + month
  }
  if (strDate >= 1 && strDate <= 9) {
    strDate = '0' + strDate
  }
  if (eMonth >= 1 && eMonth <= 9) {
    eMonth = '0' + eMonth
  }
  if (eStrDate >= 1 && eStrDate <= 9) {
    eStrDate = '0' + eStrDate
  }

  var endTime = year + plus + month + plus + strDate
  var startTime = eYear + plus + eMonth + plus + eStrDate

  return [startTime, endTime]
}

@inject('taskStore')
@observer
@Form.create()
class Tasks extends Component {
  constructor (props) {
    super(props)
    var times = getTimes()

    this.state = {
      tableData: {},
      loading: false,
      search: '',
      expand: true,
      params: {
        createUserId: window.USER_INFO.userId,
        pageNum: 0,
        pageSize: 10,
        status: '',
        startTime: times[0],
        endTime: times[1]
      }
    }
  }

  componentDidMount () {
    this.props.taskStore.getTenantList()
    this.getTableData()
  }

  getTableData () {
    const { taskStore } = this.props
    var params = this.state.params
    this.setState({ loading: true }, () => {
      params.taskLeaderId = window.USER_INFO.userId
      taskStore.getBacklogList(params).then(() => {
        console.log(taskStore.taskObj)
        this.setState({ tableData: taskStore.taskObj, loading: false })
      })
    })
  }

  paginChange = (pagination) => {
    const { params } = this.state

    params.pageSize = pagination.pageSize
    params.pageNum = pagination.current

    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  onSearch = (val) => {
    const { params } = this.state
    params.keyWord = val
    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  onClear = () => {
    const { params } = this.state
    params.keyWord = ''
    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  handleChange = (value) => {
    const { params } = this.state
    params.status = value
    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  onChangeDate = (date, dateString) => {
    const { params } = this.state
    params.startTime = dateString[0]
    params.endTime = dateString[1]

    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  toggle = () => {
    const { expand } = this.state
    this.setState({ expand: !expand })
  }

  handleSearch = (e) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState(state => ({
          params: {
            ...state.params,
            ...values,
            createTime: values.createTime.format('YYYY-MM-DD'),
            keyWord: val
          }
        }), () => {
          this.getTableData()
        })
      }
    })
  }

  handleReset = () => {
    this.props.form.resetFields()
  }

  render () {
    const { tableData, loading, params, expand } = this.state
    const { getFieldDecorator } = this.props.form
    const tenants = this.props.taskStore.tenants
    console.log(taskStatusList)
    var cls = 'advanced-search-form'
    if (!expand) {
      cls += ' advanced-search-form-collapse'
    }

    return (
      <div className="basic-tasks">
        <PageHeader />
        <div className="filter">
          <header>
            <Search
              placeholder={__('form-item-search')}
              onSearch={this.onSearch}
              allowClear
              onClear={this.onClear}
              className="filter-input"
            />
            <RangePicker
              defaultValue={[moment(params.startTime, 'YYYY-MM-DD'), moment(params.endTime, 'YYYY-MM-DD')]}
              placeholder={[__('form-item-date-start-time'), __('form-item-date-end-tiem')]}
              onChange={this.onChangeDate}
            />
            <Select defaultValue="" onChange={this.handleChange}>
              {taskStatusList.map((item, i) => {
                return <Option key={i} value={item.key}>{item.label}</Option>
              })}
            </Select>
            <span className="advanced-search-form-toggle" onClick={this.toggle}>
              高级筛选 <Icon type={expand ? 'up' : 'down'} style={{ marginLeft: 8 }} />
            </span>
            <div className={cls}>
              <Form
                hideRequiredMark
                className="advanced-search-form-fields"
                onSubmit={this.handleSearch}
              >
                <Row gutter={24}>
                  <Col span={6}>
                    <Form.Item label="销售区域" labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}>
                      {getFieldDecorator('department')(
                        <Select>
                          {tenants.map((item, i) => {
                            return <Option key={i} value={item.departId}>{item.name}</Option>
                          })}
                        </Select>
                      )}
                    </Form.Item>
                  </Col>
                  <Col span={6}>
                    <Form.Item label="售前负责人" labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}>
                      {getFieldDecorator('presalesUser')(<Input />)}
                    </Form.Item>
                  </Col>
                  <Col span={6}>
                    <Form.Item label="项目经理" labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}>
                      {getFieldDecorator('manageUser')(<Input />)}
                    </Form.Item>
                  </Col>
                  <Col span={6}>
                    <Form.Item label="项目阶段" labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}>
                      {getFieldDecorator('projectStage')(
                        <Select>
                          {global.params.stageList.map((item, i) => {
                            return <Option key={i} value={item.key}>{item.label}</Option>
                          })}
                        </Select>
                      )}
                    </Form.Item>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col span={6}>
                    <Form.Item label="启动时间" labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}>
                      {getFieldDecorator('createTime')(
                        <DatePicker />
                      )}
                    </Form.Item>
                  </Col>
                  <Col span={18} style={{ textAlign: 'right' }}>
                    <Button type="primary" htmlType="submit">查询</Button>
                    <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
                      重置
                    </Button>
                  </Col>
                </Row>
              </Form>
            </div>
          </header>
        </div>
        <Spin spinning={loading}>
          <Table
            rowKey="id"
            dataSource={tableData.list}
            pagination={{ total: tableData.total }}
            columns={columns}
            onChange={this.paginChange}
          />
        </Spin>
      </div>
    )
  }
}

export default Tasks
