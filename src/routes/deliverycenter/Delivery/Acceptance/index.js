import React, { Component } from 'react'
import { toJS } from 'mobx'
import ReactEchartsCore from 'echarts-for-react/lib/core'
import echarts from 'echarts/lib/echarts'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Select, Button, Spin } from '@uyun/components'
import 'echarts/lib/chart/line'
import 'echarts/lib/chart/bar'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/title'
import 'echarts/lib/component/dataZoom'
import 'echarts/lib/component/legend'

import __ from '@uyun/utils/i18n'

import './index.less'

const ButtonGroup = Button.Group

@inject('deliveryStore')
@observer
class Acceptance extends Component {
  constructor (props) {
    super(props)
    var date = new Date()
  	var year = date.getFullYear()

    this.state = {
      tableData: {},
      loading: false,
      params: {
        deptId: props.deptId,
        year: year
      }
    }
  }
  
  componentWillReceiveProps(nextProps) {
    this.setState(state => ({
      params: {
        ...params,
        deptId: nextProps.deptId
      }
    }), () => {
      this.getData()
    })
  }

	getOption = data => {
    const { deliveryStore } = this.props
    const { acceptance } = deliveryStore

    var list = acceptance.filter(k => !!k)
    list.reverse()
    list = list.map(item => ([item.clueMonth, item.payment_money, item.actual_money]))
    return {
    	grid: {
    		top: '10px',
    		right: '100px'
    	},
    	legend: {
    		align:'left',
    		data: ['计划验收额', '实际验收额'],
    		orient: 'vertical',
       	y: 'center',
       	x: 'right'
    	},
      color: [ '#4B98CA', '#9254DE' ],
	    tooltip: {},
	    dataset: {
	        source: [
	            ['product', '计划验收额', '实际验收额'],
	            ...list
	        ]
	    },
	    xAxis: {
	    	type: 'category',
        splitLine: {
          lineStyle: {
            type: 'dashed'
          }
        }
	    },
	    yAxis: {
        splitLine: {
          lineStyle: {
            type: 'dashed'
          }
        },
        axisTick: {
          show: false
        },
        axisLine: {
          show: false
        }
      },
	    series: [
	        { type: 'bar' },
	        { type: 'bar' }
	    ]
    }
  }

  getData = () => {
    this.setState({ loading: true }, () => {
      this.props.deliveryStore.getContractAcceptance({
        tenantId: window.USER_INFO.tenantId,
        deptId: this.state.params.deptId,
        year: this.state.params.year
      }).then(() => {
        this.setState({ loading: false })
      })
    })
  }

  nowYear = () => {
  	var date = new Date()
  	var year = date.getFullYear()

  	this.setState(state => ({
  		params: {
  			...state.params,
  			year: year
  		}
  	}), () => {
  		this.getData()
  	})
  }

  yearChange = (val) => {
  	this.setState(state => ({
  		params: {
  			...state.params,
  			year: val
  		}
  	}), () => {
  		this.getData()
  	})
  }

  setYear = () => {
  	var date = new Date()
  	var year = date.getFullYear()
  	var arr = []

  	for ( let i = year; i >= 1970; i--) {
  		arr.push(i)
  	}
  	return arr
  }

  render () {
  	const yearList = this.setYear()
    const { loading } = this.state

    return (
      <div className="basic-acceptance">
        <Spin spinning={loading}>
        	<header>
        		<span className="title">验收计划</span>
        		<div className="absR">
        			<ButtonGroup type="link">
  				      <a onClick={this.nowYear}>今年</a>
  				    </ButtonGroup>&nbsp;
  				    <Select defaultValue={this.state.params.year} onChange={this.yearChange}>
  				    	{yearList.map(item => {
  				    		return <Select.Option vlaue={item} key={item}>{item}</Select.Option>
  				    	})}
  				    </Select>
        		</div>
        	</header>
        	<ReactEchartsCore
            echarts={echarts}
            option={this.getOption()}
          />
        </Spin>
      </div>
    )
  }
}

export default Acceptance
