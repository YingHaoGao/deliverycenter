import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import ReactEchartsCore from 'echarts-for-react/lib/core'
import echarts from 'echarts/lib/echarts'
import { Select, Button, List, Row, Col, Spin } from '@uyun/components'
import 'echarts/lib/chart/line'
import 'echarts/lib/chart/bar'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/title'
import 'echarts/lib/component/dataZoom'
import 'echarts/lib/component/legend'

import __ from '@uyun/utils/i18n'

import './index.less'

const ButtonGroup = Button.Group

@inject('deliveryStore')
@observer
class WorkHour extends Component {
  constructor (props) {
    super(props)
    var date = new Date()
  	var year = date.getFullYear()

    this.state = {
      tableData: {},
      total: [],
      series: [],
      yearType: 2,
      background: ['#2E4554', '#C23431', '#B1D7C7', '#D48265', '#61A0A8'],
      loading: false,
      params: {
        year: year
      }
    }
  }

	getOption = data => {
    const list = [
      ['一季度', '二季度', '三季度', '四季度'],
      ['上半年', '下半年'],
      ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月']
    ]
    const arr = list[this.state.yearType]

    return {
    	grid: {
    		top: '50px'
    	},
    	legend: {
    		show: false
    	},
	    tooltip: {},
	    xAxis: {
        type: 'value'
	    },
	    yAxis: {
        type: 'category',
        name: '单位：h',
        nameGap: '-100%',
        data: arr
      },
	    series: this.state.series[this.state.yearType]
    }
  }

  componentDidMount () {
    this.getData()
  }

  getData = () => {
    this.setState({ loading: true }, () => {
      this.props.deliveryStore.getWorkHours({
        tenantId: window.USER_INFO.tenantId,
        year: this.state.params.year
      }).then(() => {
        this.setSeries()
        this.setState({ loading: false })
      })
    })
  }

  yearChange = (val) => {
  	this.setState(state => ({
  		params: {
  			...state.params,
  			year: val
  		}
  	}), () => {
  		this.getData()
  	})
  }

  yearType = (type) => {
    return () => {
      this.setState({ yearType: type })
    }
  }

  setYear = () => {
  	var date = new Date()
  	var year = date.getFullYear()
  	var arr = []

  	for ( let i = year; i >= 1970; i--) {
  		arr.push(i)
  	}
  	return arr
  }

  setSeries = () => {
    const { deliveryStore } = this.props
    const { yearType } = this.state
    const { workHours } = deliveryStore

    var list = workHours.filter(k => !!k)
    var series = []
    var total = []
    var quarter = []
    var semester = []
    var year = []

    list.map(item => {
      var i = series.find(k => k.name === item.department)
      const month = parseInt(item.clueMonth)

      if (!i) {
        var data = []
        data[month - 1] = item.actual_expend

        series.push({
          name: item.department,
          type: 'bar',
          stack: '总量',
          label: {
              normal: {
                  show: true,
                  position: 'insideRight'
              }
          },
          data: data
        })
      } else {
        i.data[parseInt(item.clueMonth) - 1] = item.actual_expend
      }
    })
    series.map((item, i) => {
      total.push({
        department: item.name,
        total: item.data.reduce((num, item) => num + item)
      })

      var item1 = Object.assign({}, item)
      var item2 = Object.assign({}, item)
      var arr1 = []
      var arr2 = []

      item1.data.map((_item, _i) => {
        var _i = _i + 1
        var k = 0
        if (0 < _i && _i <=3) {
          k = 0
        }
        else if (3 < _i && _i <=6) {
          k = 1
        }
        else if (6 < _i && _i <=9) {
          k = 2
        }
        else if (9 < _i && _i <=12) {
          k = 3
        }

        isNaN(arr1[k]) ? (arr1[k] = _item) : arr1[k] += _item
      })

      item2.data.map((_item, _i) => {
        var _i = _i + 1
        var k = 0
        if (0 < _i && _i <=6) {
          k = 0
        }
        else if (6 < _i && _i <=12) {
          k = 1
        }
        isNaN(arr2[k]) ? (arr2[k] = _item) : arr2[k] += _item
      })

      item1.data = arr1
      item2.data = arr2

      quarter.push(item1)
      semester.push(item2)
    })
    total.sort((a, b) => b - a)
    total = total.slice(0, 5)

    this.setState({ total: total, series: [quarter, semester, series] })
  }

  bgIdx = (i) => {
    return { background: this.state.background[i] }
  }

  render () {
  	const yearList = this.setYear()
    const { loading, yearType } = this.state

    return (
      <div className="basic-workhour">
        <Spin spinning={loading}>
        	<header>
        		<span className="title">工时统计</span>
        		<div className="absR">
        			<ButtonGroup type="link">
                <a className={yearType !== 0 ? 'default' : null} onClick={this.yearType(0)}>季度</a>
                <a className={yearType !== 1 ? 'default' : null} onClick={this.yearType(1)}>半年</a>
  				      <a className={yearType !== 2 ? 'default' : null} onClick={this.yearType(2)}>全年</a>
  				    </ButtonGroup>&nbsp;&nbsp;
  				    <Select defaultValue={this.state.params.year} onChange={this.yearChange}>
  				    	{yearList.map(item => {
  				    		return <Select.Option vlaue={item} key={item}>{item}</Select.Option>
  				    	})}
  				    </Select>
        		</div>
        	</header>
          <Row>
            <Col span={18}>
              <ReactEchartsCore
                echarts={echarts}
                option={this.getOption()}
              />
            </Col>
            <Col span={6}>
              <List
                className="total"
                header={<h2>工时排名</h2>}
                dataSource={this.state.total}
                renderItem={(item, i) => (<List.Item>
                  <span className="totalIdx" style={this.bgIdx(i)}>{i + 1}</span>
                  <span className="totalDep">{item.department}</span>
                  <span className="totalNum">{item.total}h</span>
                </List.Item>)}
              />
            </Col>
          </Row>
        </Spin>
      </div>
    )
  }
}

export default WorkHour
