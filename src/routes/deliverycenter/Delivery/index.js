import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Select, Row, Col } from '@uyun/components'

import Acceptance from './Acceptance/index'
import Objective from './Objective/index'
import WorkHour from './WorkHour/index'

import 'echarts/lib/chart/line'
import 'echarts/lib/chart/bar'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/title'
import 'echarts/lib/component/dataZoom'
import 'echarts/lib/component/legend'

import __ from '@uyun/utils/i18n'

import './index.less'

const Option = Select.Option

@inject('deliveryStore')
@observer
class Delivery extends Component {
  constructor (props) {
    super(props)
    this.state = {
      deptList: [],
      deptId: ''
    }
  }

  componentDidMount () {
    this.getDept()
  }

  getDept = () => {
    this.props.deliveryStore.getTenantList({ userId: window.USER_INFO.userId }).then(() => {
      const deptList = this.props.deliveryStore.deptList
      this.setState({ deptList: deptList, deptId: deptList[0].departId })
    })
  }

  deptChange = (val) => {
    this.setState({ deptId: val })
  }

  render () {
    return (
      <div className="basic-delivery">
        <header>
          <Select className="dept" value={this.state.deptId} onChange={this.deptChange}>
            {this.state.deptList.map(item => {
              return <Option key={item.departId} value={item.departId}>{item.name}</Option>
            })}
          </Select>
        </header>
        <Row>
          <Col span={10}>
            <Objective deptId={this.state.deptId} />
          </Col>
          <Col span={14}>
            <Acceptance deptId={this.state.deptId} />
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <WorkHour deptId={this.state.deptId} />
          </Col>
        </Row>
      </div>
    )
  }
}

export default Delivery
