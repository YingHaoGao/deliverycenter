import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Button, Progress, Modal, InputNumber, Select, Popover, Spin } from '@uyun/components'

import __ from '@uyun/utils/i18n'

import './index.less'

const ButtonGroup = Button.Group

@inject('deliveryStore')
@observer
class Objective extends Component {
  constructor (props) {
    super(props)
    this.state = {
      maxLeft: { left: '50%' },
      visible: false,
      loading: false,
      target: 0,
      deptId: ''
    }
  }

  componentDidMount () {
    this.getData()
  }

  getData = () => {
    this.setState({ loading: true }, () => {
      this.props.deliveryStore.getContractYear({
        tenantId: window.USER_INFO.tenantId,
        deptId: this.state.deptId
      }).then(() => {
        this.setState({ loading: false })
      })
    })
  }

  componentWillReceiveProps(nextProps) {
    this.setState({deptId: nextProps.deptId}, () => {
      this.getData()
    })
  }

  save = () => {

  }

  onClose = () => {
    this.setState({ visible: false })
  }

  open = () => {
    this.setState({ visible: true })
  }
  
  targetInput = (val) => {
    this.setState({ target: val })
  }

  myriad = (n) => {
    if (n >= 10000) {
      return (n / 10000).toFixed(2) + 'W'
    }
    return n
  }

  render () {
    const { visible, loading } = this.state
    const { deliveryStore } = this.props
    const { contractYear } = deliveryStore
    const maxLeft = {
      // left: (contractYear.income_money / contractYear.performance) * 100 + '%'
      left: '80%'
    }

    return (
      <div className="basic-objective">
        <Spin spinning={loading}>
          <header>
            <span className="title">{__('objective-annual-performance')}</span>
            <span className="btnEdit">
              <ButtonGroup type="link" onClick={this.open}><a>{__('button-edit')}</a></ButtonGroup>
            </span>
          </header>
          <div className="slider bor">
            <div className="item pad0">
              <span>{__('objective-confirmed-amount')}</span>
              <span className="black">
                <Popover content={contractYear.income_money + '元'} trigger="hover">
                  {this.myriad(contractYear.income_money)}
                </Popover>
              </span>
              <span className="aboustRb">
                <Popover content={contractYear.performance + '元'} trigger="hover">
                  {this.myriad(contractYear.performance)}
                </Popover>
              </span>
            </div>
            <div className="progressBox">
              <div className="prog">
                <Progress percent={(contractYear.income_money / contractYear.performance) * 100} showInfo={false} />
              </div>
              <div className="goal" style={maxLeft}></div>
            </div>
          </div>
          <div className="item bor">
            <span>交付池金额</span>
            <span className="aboustR">
              <Popover content={contractYear.payment_money + '元'} trigger="hover">
                {this.myriad(contractYear.payment_money)}
              </Popover>
            </span>
          </div>
          <div className="item">
            <span>{__('objective-performance-amount')}</span>
            <span className="aboustR">
              <Popover content={contractYear.performance - deliveryStore.income_money + '元'} trigger="hover">
                {this.myriad(contractYear.performance - deliveryStore.income_money)}
              </Popover>
            </span>
          </div>
        </Spin>
        <Modal
          title={__('objective-edito')}
          className="objectiveMode"
          width="300px"
          visible={visible}
          onOk={this.save}
          onCancel={this.onClose}
          destroyOnClose={true}
        >
          <span>{__('objective-target')}(万): </span>
          <InputNumber className="target" onChange={this.targetInput} />
        </Modal>
      </div>
    )
  }
}

export default Objective
