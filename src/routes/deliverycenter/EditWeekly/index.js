import React, { Component } from 'react'
import { Button, Row, Col, Table, Modal, Collapse, Form, DatePicker, Select, Input } from '@uyun/components'
import { getProjectInfo, postWorkWeekly, getWorkWeekly } from '@/services/api'
import { Link, hashHistory } from 'react-router'

import PageHeader from '@/components/PageHeader'
import __ from '@uyun/utils/i18n'

import ModalFrom from './modalFrom.js'

import './index.less'

const { WeekPicker } = DatePicker
const { TextArea } = Input
const { Option } = Select

const columns = {
  resource: [
    {
      title: '姓名',
      dataIndex: 'name',
      key: 'name',
      width: '96px'
    },
    {
      title: '岗位',
      dataIndex: 'station',
      key: 'station',
      width: '150px'
    },
    {
      title: '填报工时(h)',
      dataIndex: 'working',
      key: 'working',
      width: '220px'
    },
    {
      title: '驻地',
      dataIndex: 'encampment',
      key: 'encampment'
    }
  ],
  issue: [
    {
      title: '描述',
      dataIndex: 'riskDescribe',
      key: 'riskDescribe',
      width: '220px'
    },
    {
      title: '影响',
      dataIndex: 'effect',
      key: 'effect',
      render: text => {
        let a = global.params.effectList.filter(k => k.key === text)[0]
        return a ? a.label : ''
      },
      width: '44px'
    },
    {
      title: '活动/补救',
      dataIndex: 'activity',
      key: 'activity',
      width: '290px'
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      render: text => {
        let a = global.params.status1.filter(k => k.key === text)[0]
        return a ? a.label : ''
      },
      width: '50px'
    },
    {
      title: '备注',
      dataIndex: 'remark',
      key: 'remark',
      width: '250px'
    },
    {
      title: '期望解决日期',
      dataIndex: 'expectDate',
      key: 'expectDate'
    }
  ],
  risk: [
    {
      title: '描述',
      dataIndex: 'riskDescribe',
      key: 'riskDescribe',
      width: '220px'
    },
    {
      title: '影响',
      dataIndex: 'effect',
      key: 'effect',
      render: text => {
        let a = global.params.effectList.filter(k => k.key === text)[0]
        return a ? a.label : ''
      },
      width: '44px'
    },
    {
      title: '活动/补救',
      dataIndex: 'activity',
      key: 'activity',
      width: '290px'
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      render: text => {
        let a = global.params.status1.filter(k => k.key === text)[0]
        return a ? a.label : ''
      },
      width: '50px'
    },
    {
      title: '备注',
      dataIndex: 'remark',
      key: 'remark',
      width: '250px'
    },
    {
      title: '期望解决日期',
      dataIndex: 'expectDate',
      key: 'expectDate'
    }
  ],
  deliverable: [
    {
      dataIndex: 'name'
    },
    {
      dataIndex: 'state'
    },
    {
      dataIndex: 'index',
      render: s => {
        return (<Button><a>下载</a></Button>)
      }
    }
  ]
}

var child = {}

@Form.create()
class ProjectInfo extends Component {
  constructor (props) {
    super(props)

    this.state = {
      projectId: '',
      weeklyId: '',
      visible: false,
      modelTitle: '',
      riskType: '0',
      projectInfo: {},
      weeklyInfo: {
        problemRisks: [],
      },
      tableData: {
        resource: [],
        issue: [],
        risk: [],
        deliverable: []
      }
    }
    this.onClose = this.onClose.bind(this)
  }

  componentDidMount () {
    const { location } = this.props
    const { query } = location

    this.setState({ projectId: query.projectId }, () => {
      this.getProject()
      if (query.weeklyId) {
        this.setState({ weeklyId: query.weeklyId }, () => {
          this.getWeekly()
        })
      }
    })
  }

  getProject () {
    var params = { projectId: this.state.projectId }

    getProjectInfo(params).then(res => {
      if (res.code === 0) {
        var projectInfo = res.resultMap.data
        this.setState({ projectInfo: projectInfo })
      }
    })
  }

  getWeekly () {
    var params = { id: this.state.weeklyId }

    getWorkWeekly(params).then(res => {
      if (res.code === 0) {
        var weeklyInfo = res.resultMap.data
        this.setState({ weeklyInfo: weeklyInfo })
      }
    })
  }

  onClose = () => {
    this.setState({ visible: false })
  }

  onOpen = (type) => {
    return (e) => {
      var title = type === '0' ? '新增问题' : '新增风险'
      this.setState({ riskType: type, modelTitle: title, visible: true })
    }
  }

  save = () => {
    const { weeklyInfo } = this.state
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        values.weeklyCode = weeklyInfo.weeklyCode
        values.problemRisks = weeklyInfo.problemRisks
        postWorkWeekly(values).then(res => {
          if (res.code === 0) {
            this.props.history.push('/weeklys')
          }
        })
      }
    })
  }

  onRef (ref) {
    child = ref
  }

  weekChang = (data, str) => {
    this.setState(state => ({
      weeklyInfo: {
        ...state.weeklyInfo,
        weeklyCode: str
      }
    }))
  }

  desChange = (e) => {
    console.log(e.target.value)
  }

  modalSave = () => {
    child.save(this)
  }

  render () {
    const { projectInfo, visible, modelTitle, tableData, weeklyInfo } = this.state
    const { getFieldDecorator } = this.props.form
    const formItemLayout = {
      labelCol: { span: 1 },
      wrapperCol: { span: 12 }
    }
    const formItemLayout1 = {
      labelCol: { span: 2 },
      wrapperCol: { span: 10 }
    }
    const layout = {
      formLayout: 'horizontal'
    }

    return (
      <div className="basic-editweeklyinfo">
        <PageHeader />
        <Form layout={layout}>
          <Row>
            <Col span={20}>
              <header>
                <Form.Item {...formItemLayout1} label="项目名称">
                  <span>{projectInfo.projectName}</span>
                </Form.Item>
                <Form.Item {...formItemLayout1} label="项目编号">
                  <span>{projectInfo.projectName}</span>
                </Form.Item>
                <Form.Item {...formItemLayout1} label="报告人">
                  <span>{projectInfo.manageUser}</span>
                </Form.Item>
                <Row>
                  <Col span={8}>
                    <Form.Item {...formItemLayout1} label="报告周期">
                      {getFieldDecorator('weeklyCode', { rules: [{ required: true }] })(
                        <WeekPicker onChange={this.weekChang} />
                      )}
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item {...formItemLayout1} label="报告时间段">
                      <span>{projectInfo.startTime}-{projectInfo.endTime}</span>
                    </Form.Item>
                  </Col>
                </Row>
              </header>
            </Col>
          </Row>
          <Collapse defaultActiveKey={['0', '1', '2', '3']}>
            <Collapse.Card
              className="close"
              key="0"
              header="项目状态"
              showArrow={false}
            >
              <h3 className="blue">总体</h3>
              <Form.Item {...formItemLayout} label="状态">
                {getFieldDecorator('weeklyStatus', { initialValue: '0', rules: [{ required: true }] })(
                  <Select onChange={this.substageLeaderChange}>
                    {global.params.status1.map(item => {
                      return <Option value={item.key} key={item.key}>{item.label}</Option>
                    })}
                  </Select>
                )}
              </Form.Item>
              <Form.Item label="备注">
                {getFieldDecorator('weeklyDescribe', { rules: [{ required: true }] })(
                  <TextArea placeholder="请输入备注描述" rows={4} />
                )}
              </Form.Item>
              <h3 className="blue">进度</h3>
              <Form.Item {...formItemLayout} label="状态">
                {getFieldDecorator('scheduleStatus', { initialValue: '0', rules: [{ required: true }] })(
                  <Select onChange={this.substageLeaderChange}>
                    {global.params.status1.map(item => {
                      return <Option value={item.key} key={item.key}>{item.label}</Option>
                    })}
                  </Select>
                )}
              </Form.Item>
              <Form.Item label="备注">
                {getFieldDecorator('scheduleDescribe', { rules: [{ required: true }] })(
                  <TextArea placeholder="请输入备注描述" rows={4} />
                )}
              </Form.Item>
              <h3 className="blue">资源</h3>
              <Form.Item {...formItemLayout} label="状态">
                {getFieldDecorator('resourceStatus', { initialValue: '0', rules: [{ required: true }] })(
                  <Select onChange={this.substageLeaderChange}>
                    {global.params.status1.map(item => {
                      return <Option value={item.key} key={item.key}>{item.label}</Option>
                    })}
                  </Select>
                )}
              </Form.Item>
              <Form.Item label="备注">
                {getFieldDecorator('resourceDescribe', { rules: [{ required: true }] })(
                  <TextArea placeholder="请输入备注描述" rows={4} />
                )}
              </Form.Item>
              <h3 className="blue">范围</h3>
              <Form.Item {...formItemLayout} label="状态">
                {getFieldDecorator('scopeStatus', { initialValue: '0', rules: [{ required: true }] })(
                  <Select onChange={this.substageLeaderChange}>
                    {global.params.status1.map(item => {
                      return <Option value={item.key} key={item.key}>{item.label}</Option>
                    })}
                  </Select>
                )}
              </Form.Item>
              <Form.Item label="备注">
                {getFieldDecorator('scopeDescribe', { rules: [{ required: true }] })(
                  <TextArea placeholder="请输入备注描述" rows={4} />
                )}
              </Form.Item>
              <h3 className="blue">交付物</h3>
              <Form.Item {...formItemLayout} label="状态">
                {getFieldDecorator('deliverableStatus', { initialValue: '0', rules: [{ required: true }] })(
                  <Select onChange={this.substageLeaderChange}>
                    {global.params.status1.map(item => {
                      return <Option value={item.key} key={item.key}>{item.label}</Option>
                    })}
                  </Select>
                )}
              </Form.Item>
              <Form.Item label="核心交付物">
                <Table
                  showHeader={false}
                  dataSource={tableData.deliverable}
                  columns={columns.deliverable}
                  pagination={false}
                />
              </Form.Item>
            </Collapse.Card>
            <Collapse.Card
              key="1"
              header="本周资源利用汇总"
              showArrow={false}
            >
              <Table
                rowKey="id"
                dataSource={tableData.resource}
                columns={columns.resource}
                pagination={false}
              />
            </Collapse.Card>
            <Collapse.Card
              key="2"
              header="问题及风险"
              showArrow={false}
            >
              <h3 className="marB">
                <span className="blue">问题</span>
                <Button type="primary" className="absR" onClick={this.onOpen('0')}>添加</Button>
              </h3>
              <Table
                rowKey={(record, index) => index}
                dataSource={weeklyInfo.problemRisks.filter(k => k.type === '0')}
                columns={columns.issue}
                pagination={false}
              />
              <h3 className="marB">
                <span className="blue">风险</span>
                <Button type="primary" className="absR" onClick={this.onOpen('1')}>添加</Button>
              </h3>
              <Table
                rowKey={(record, index) => index}
                dataSource={weeklyInfo.problemRisks.filter(k => k.type === '1')}
                columns={columns.risk}
                pagination={false}
              />
            </Collapse.Card>
            <Collapse.Card
              key="3"
              header="客户重大事件"
              showArrow={false}
            >
              <Form.Item label="描述">
                {getFieldDecorator('remark')(
                  <TextArea row={4} />
                )}
              </Form.Item>
            </Collapse.Card>
          </Collapse>
          <Row className="marT">
            <Col span={24}>
              <Button type="primary" onClick={this.save}>提交</Button>
            </Col>
          </Row>
        </Form>

        <Modal
          destroyOnClose={true}
          title={modelTitle}
          visible={visible}
          onOk={this.modalSave}
          onCancel={this.onClose} >
          <ModalFrom onRef={this.onRef} riskType={this.state.riskType} />
        </Modal>
      </div>
    )
  }
}

export default ProjectInfo
