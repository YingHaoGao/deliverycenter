import React, { Component } from 'react'
import { Form, DatePicker, Select, Input } from '@uyun/components'
import './index.less'

const { TextArea } = Input
const { Option } = Select

@Form.create()
class WeeklyModal extends Component {
	constructor (props) {
    super(props)

    this.state = {
      activity: '',
      createTime: '',
      createUser: window.USER_INFO.realname,
      createUserId: window.USER_INFO.userId,
      effect: '',
      expectDate: '',
      remark: '',
      riskDescribe: '',
      status: '',
      type: this.props.riskType,
      weeklyId: ''
    }
  }

  componentDidMount () {
    this.props.onRef(this)
  }

  save = (_this) => {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
      	values.type = _this.state.riskType
        values.expectDate = values.expectDate.format('YYYY-MM-DD')

        _this.setState(state => ({
        	weeklyInfo: {
        		...state.weeklyInfo,
        		problemRisks: [ ...state.weeklyInfo.problemRisks, values ]
        	}
        }), () => {
        	_this.onClose()
        })
      }
    })
  }

	render () {
		const { getFieldDecorator } = this.props.form

		return (
      <Form layout="vertical">
        <Form.Item label="影响">
          {getFieldDecorator('effect', { initialValue: '0', rules: [{ required: true }] })(
            <Select onChange={this.substageLeaderChange}>
              {global.params.effectList.map(item => {
                return <Option value={item.key} key={item.key}>{item.label}</Option>
              })}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="状态">
          {getFieldDecorator('status', { initialValue: '0', rules: [{ required: true }] })(
            <Select onChange={this.substageLeaderChange}>
              {global.params.status1.map(item => {
                return <Option value={item.key} key={item.key}>{item.label}</Option>
              })}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="期望解决日期">
          {getFieldDecorator('expectDate', { rules: [{ required: true }] })(
            <DatePicker />
          )}
        </Form.Item>
        <Form.Item label="描述">
          {getFieldDecorator('riskDescribe', { rules: [{ required: true }] })(
            <TextArea placeholder="请输入备注描述" rows={4} />
          )}
        </Form.Item>
        <Form.Item label="活动/补救">
          {getFieldDecorator('activity', { rules: [{ required: true }] })(
            <TextArea placeholder="请输入备注描述" rows={4} />
          )}
        </Form.Item>
      </Form>
    )
	}
}

export default WeeklyModal