import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Input, Table, Spin, notification } from '@uyun/components'

import __ from '@uyun/utils/i18n'
import { getManagerList, postAssignManager } from '@/services/api'

import './AssignManager.less'

const Search = Input.Search

const columns = [{
  title: 'Name',
  dataIndex: 'realname',
  render: text => <a href="javascript:;">{text}</a>
}, {
  title: 'Age',
  dataIndex: 'age'
}, {
  title: 'Address',
  dataIndex: 'address'
}]

@inject('projectsStore')
@observer
class AssignManager extends Component {
  constructor (props) {
    super(props)
    this.state = {
      managerList: [],
      loading: false,
      selectedRowKeys: [],
      selecUser: {},
      params: {
        userId: window.USER_INFO.userId,
        tenantId: window.USER_INFO.tenantId,
        name: ''
      }
    }
  }

  componentDidMount () {
    this.props.onRef(this)
    this.getList()
  }

  tableChange = (record, item) => {
    this.setState({ selectedRowKeys: [record[record.length - 1]], selecUser: item[0] })
  }

  getList = () => {
    const { projectsStore } = this.props

    this.setState({ loading: true }, () => {
      projectsStore.getManagerNameList({ deptName: '交付中心', tenantId: window.USER_INFO.tenantId }).then(() => {
        this.setState({ managerList: projectsStore.manNameList })
      })
      this.setState({ loading: false })
    })
  }

  save = () => {
    this.setState({ loading: true }, () => {
      var { projectInfo, projectsStore } = this.props
      const { selecUser } = this.state

      projectInfo = { ...projectInfo, manageUser: selecUser.realname, manageUserId: selecUser.userId }
      projectsStore.postProjectUpdata(projectInfo).then(() => {
        notification.success({
          message: '指派项目经理成功'
        })
        projectsStore.getProjectInfo({ projectId: projectInfo.id })
        this.props.onClose()
        this.setState({ loading: false })
      })
    })
  }

  onSearch = (val) => {
    const { params } = this.state
    params.name = val
    this.setState({ params: params }, () => {
      this.getList()
    })
  }

  onClear = () => {
    const { params } = this.state
    params.name = ''
    this.setState({ params: params }, () => {
      this.getList()
    })
  }

  render () {
    const { managerList, loading, selectedRowKeys } = this.state
    const rowSelection = {
      selectedRowKeys,
      onChange: this.tableChange
    }

    return (
      <div className="basic-assignManager">
        <header>
          <Search
            placeholder={__('assign-sales-phone')}
            onSearch={this.onSearch}
            allowClear
            onClear={this.onClear}
          />
        </header>
        <Spin spinning={loading}>
          <Table
            rowSelection={rowSelection}
            columns={columns}
            pagination={false}
            showHeader={false}
            dataSource={managerList} />
        </Spin>
      </div>
    )
  }
}

export default AssignManager
