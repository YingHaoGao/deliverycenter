import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Form, Input, DatePicker, InputNumber, Button, Select, Spin, notification } from '@uyun/components'

import __ from '@uyun/utils/i18n'
// import { postSubStageAdd, getWeightExceed100, getDeptList } from '@/services/api'

import './AddSubphase.less'

let id = 0

const { RangePicker } = DatePicker
const { TextArea } = Input
const { Option } = Select
const ButtonGroup = Button.Group

@inject('projectsStore')
@observer
@Form.create()
class AddSubphase extends Component {
  constructor (props) {
    super(props)
    this.state = {
      managerList: [],
      params: {},
      loading: false
    }
  }

  componentDidMount () {
    this.props.onRef(this)
    this.getPrincipal()
  }

  getPrincipal = () => {
    var params = {
      // name: window.USER_INFO.realname,
      // tenantId: window.USER_INFO.tenantId,
      name: '交付人员',
      tenantId: 'e10adc3949ba59abbe56e057f20f88dd',
      userId: window.USER_INFO.userId
    }
    this.props.projectsStore.getDeptList(params).then(() => {
      this.setState({ managerList: this.props.projectsStore.deptList })
    })
  }

  save = () => {
    const { projectInfo, projectsStore } = this.props
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({ loading: true }, () => {
          values.startTime = values.time[0] || ''
          values.endTime = values.time[1] || ''
          values.projectId = projectInfo.id
          values.createUserId = window.USER_INFO.userId
          values.stageId = 3
          values.substageType = 0
          if (values.deliverableTitle) {
            values.deliverableTitle = values.deliverableTitle.filter(k => k !== null)
          }        
          projectsStore.getWeightExceed100({
            projectId: projectInfo.id,
            stageId: 3,
            weight: values.weight
          }).then(() => {
            if (projectsStore.weight) {
              projectsStore.postSubStageAdd(values).then(() => {
                notification.success({
                  message: '添加子阶段成功'
                })
                projectsStore.getSubStageQueryStageTask({
                  projectId: projectInfo.id,
                  stageId: 3
                })
                this.props.onClose()
                this.setState({ loading: false })
              })
            }
            this.setState({ loading: false })
          })
        })
      }
    })
  }

  removeDeliverables = k => {
    const { form } = this.props
    const keys = form.getFieldValue('keys')

    form.setFieldsValue({
      keys: keys.filter(item => item !== k)
    })
  }

  addDeliverables = () => {
    const { form } = this.props
    const keys = form.getFieldValue('keys')
    const nextKeys = keys.concat(id++)

    form.setFieldsValue({
      keys: nextKeys
    })
  }

  substageLeaderChange = (val) => {
    const { form } = this.props
    form.setFieldsValue({
      substageLeader: val
    })
  }

  render () {
    const { getFieldDecorator, getFieldValue } = this.props.form
    const { managerList, loading } = this.state

    getFieldDecorator('keys', { initialValue: [] })

    const keys = getFieldValue('keys')

    const formItems = keys.map((k, index) => (
      <Form.Item
        className="border"
        label={__('addSubphase-delivery-name')}
        required={false}
        key={k}
      >
        {getFieldDecorator('deliverableTitle[' + k + ']', {
          rules: [
            {
              type: 'string',
              required: true,
              message: "Please input passenger's name or delete this field."
            }
          ]
        })(<Input className="name" />)}
        <ButtonGroup type="link">
          <a className="Rabsolute" onClick={() => this.removeDeliverables(k)}>{__('button-del')}</a>
        </ButtonGroup>
      </Form.Item>
    ))

    return (
      <div className="basic-addSubphase">
        <Spin spinning={loading}>
          <Form>
            <Form.Item label={__('addSubphase-phase-name')}>
              {getFieldDecorator('substageTitle', { rules: [{ required: true, message: '必填项!' }] })(<Input />)}
            </Form.Item>
            <Form.Item label={__('addSubphase-weight')}>
              {getFieldDecorator('weight', { rules: [{ required: true, message: '必填项!' }] })(<InputNumber className="weight" />)}%
            </Form.Item>
            <Form.Item label={__('addSubphase-stage-manager')}>
              {getFieldDecorator('substageLeader', { rules: [{ required: true, message: '必填项!' }] })(
                <Select onChange={this.substageLeaderChange}>
                  {managerList.map(item => {
                    return <Option value={item.userId} key={item.userId}>{item.realname}</Option>
                  })}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={__('addSubphase-planned-days')}>
              {getFieldDecorator('planExpend', { rules: [{ required: true, message: '必填项!' }] })(<InputNumber />)}
            </Form.Item>
            <Form.Item label={__('addSubphase-schedule-start-deadline')}>
              {getFieldDecorator('time', { rules: [{ type: 'array', required: true, message: '必填项!' }] })(<RangePicker />)}
            </Form.Item>
            <Form.Item label={__('addSubphase-description-stages')}>
              {getFieldDecorator('substageDesc', { rules: [{ required: true, message: '必填项!' }] })(<TextArea style={{ minHeight: 32 }} placeholder={__('form-item-goal-placeholder')} rows={4} />)}
            </Form.Item>
            <h3>
              {__('addSubphase-core-deliverables')}
              <Button type="primary" onClick={this.addDeliverables}>{__('button-newly')}</Button>
            </h3>
            {formItems}
          </Form>
        </Spin>
      </div>
    )
  }
}

export default AddSubphase
