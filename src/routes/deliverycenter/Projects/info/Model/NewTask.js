import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import moment from 'moment'
import { Form, Input, InputNumber, DatePicker, Button, Select, Row, Col, Upload, Spin, Table } from '@uyun/components'

import __ from '@uyun/utils/i18n'

import './NewTask.less'

const { RangePicker } = DatePicker
const { TextArea } = Input
const { Option } = Select

const columns = [
  {
    title: '名称',
    dataIndex: 'substageTitle',
    key: 'substageTitle'
  }
]

@inject('projectsStore')
@observer
@Form.create()
class NewTask extends Component {
  constructor (props) {
    super(props)
    this.state = {
      deliverList: [],
      reporterList: [],
      managerList: [],
      initLoading: false,
      loading: false,
      fileList: [],
      filepathList: [],
      unDelList: [],
      conPayList: [],
      conIncomeList: [],
      selDel: [],
      uploading: false
    }
  }

  componentDidMount () {
    this.props.onRef(this)
    this.getUnDel()
    this.getPrincipal()
    this.getPay()
    this.getIncome()
  }

  save = () => {
    const { projectsStore, projectInfo, stageInfo } = this.props
    this.setState({ loading: true }, () => {
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
          var data = {}
          var filePath = this.state.filepathList.map(k => k.filePath)
          var deliverable = this.state.selDel.map(k => k.id)
          console.log(this.state.managerList.find(k => k.id === values.reportUserId))
          console.log(this.state.managerList.find(k => k.id === values.taskLeaderId))
          data.startTime = moment(values.time[0]).format('YYYY-MM-DD HH:MM:ss') || ''
          data.endTime = moment(values.time[1]).format('YYYY-MM-DD HH:MM:ss') || ''
          data.filePath = filePath.join(',')
          data.incomeId = values.incomeId
          data.createUser = window.USER_INFO.realname
          data.createUserId = window.USER_INFO.userId
          data.deliverable = deliverable.join(',')
          data.isCoreDeliverors = (values.deliverable || []).length > 0 ? 1 : 0
          data.projectId = projectInfo.id
          data.paymentId = values.paymentId
          data.planExpend = values.planExpend
          data.relatedDeliverable = (values.deliverable || []).length
          data.uploadDeliverable = (filePath || []).length
          data.reportUser = this.state.managerList.find(k => k.userId === values.reportUserId).realname
          data.reportUserId = values.reportUserId
          data.substageId = stageInfo.id
          data.taskDesc = values.taskDesc
          data.taskLeaderName = this.state.managerList.find(k => k.userId === values.taskLeaderId).realname
          data.taskLeader = values.taskLeaderId
          data.taskTitle = values.taskTitle
          data.taskType = values.taskType
          data.parentId = 0

          projectsStore.postTaskAdd(data).then(() => {
            projectsStore.getSubStageQueryStageTask({
              projectId: projectInfo.id,
              stageId: 3
            })
            this.props.onClose()
            this.setState({ loading: false })
          })
        }
      })
    })
  }

  // 回款点
  getPay = () => {
    const { projectsStore } = this.props
    projectsStore.getConPayList({ contractId: '' }).then(() => {
      var conPayList = [{ id: '', paymentStage: '不关联' }]
      projectsStore.conPayList.map(item => {
        if (item) {
          conPayList.push(item)
        }
      })
      console.log('pay:', conPayList)
      this.setState({ conPayList: conPayList })
    })
  }

  // 收入点
  getIncome = () => {
    const { projectsStore } = this.props
    projectsStore.getConIncomeList({ contractId: '' }).then(() => {
      console.log('incom', projectsStore.conIncomeList)
      this.setState({ conIncomeList: projectsStore.conIncomeList })
    })
  }

  // 未关联的交付物
  getUnDel = () => {
    const { projectsStore, stageInfo } = this.props

    projectsStore.getUnDelList({ subStageId: stageInfo.id }).then(() => {
      console.log('unDelList', projectsStore.unDelList)
      this.setState({ unDelList: projectsStore.unDelList })
    })
  }

  getPrincipal = () => {
    const { projectsStore } = this.props
    projectsStore.getManagerNameList({ deptName: '交付中心', tenantId: window.USER_INFO.tenantId }).then(() => {
      this.setState({ managerList: projectsStore.manNameList })
    })
  }

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e
    }
    return e && e.fileList
  }

  render () {
    const { stageInfo = {} } = this.props
    const { getFieldDecorator } = this.props.form
    const { managerList, initLoading, loading, fileList, uploading, unDelList, conPayList, conIncomeList } = this.state
    const updataObj = {
      onRemove: (file) => {
        this.setState((state) => {
          const index = state.fileList.indexOf(file)
          const newFileList = state.fileList.slice()
          const newFilepathList = state.filepathList.slice()
          newFileList.splice(index, 1)
          newFilepathList.splice(index, 1)
          return {
            fileList: newFileList,
            filepathList: newFilepathList
          }
        })
      },
      beforeUpload: (file) => {
        this.setState({
          uploading: true
        })
        const formData = new FormData()
        formData.append('file', file)
        this.props.projectsStore.postFileUpload(formData).then(() => {
          this.setState(state => ({
            fileList: [...state.fileList, file],
            filepathList: [...state.filepathList, this.props.projectsStore.fileUpload]
          }))
          this.setState({ uploading: false })
        })

        return false
      },
      fileList
    }
    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        this.setState({ selDel: selectedRows })
      },
      getCheckboxProps: record => ({
        // disabled: record.name === 'Disabled User', // Column configuration not to be checked
        // name: record.name,
      })
    }

    return (
      <div className="basic-postProject">
        <Spin spinning={loading}>
          <Form>
            <Row>
              <Col span={24}>
                <Form.Item label={__('newTask-task-name')}>
                  {getFieldDecorator('taskTitle', { rules: [{ required: true, message: '必填项!' }] })(<Input />)}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Form.Item label={__('subphase')}>
                  {stageInfo.substageTitle}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <Form.Item label={__('newTask-collection-point')}>
                  {getFieldDecorator('paymentId')(
                    <Select>
                      {conPayList.map(item => {
                        return <Option value={item.id} key={item.id}>{item.paymentStage}</Option>
                      })}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label={__('newTask-revenue-point')}>
                  {getFieldDecorator('incomeId')(
                    <Select>
                      {conIncomeList.map(item => {
                        return <Option value={item.id} key={item.id}>{item.incomeTitle}</Option>
                      })}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <Form.Item label={__('newTask-mission-leader')}>
                  {getFieldDecorator('taskLeaderId', { rules: [{ required: true, message: '必填项!' }] })(
                    <Select>
                      {managerList.map(item => {
                        return <Option value={item.userId} key={item.userId}>{item.realname}</Option>
                      })}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label={__('newTask-task-type')}>
                  {getFieldDecorator('taskType', { rules: [{ required: true, message: '必填项!' }] })(
                    <Select>
                      {global.params.taskType.map(item => {
                        return <Option value={item.key} key={item.key}>{item.label}</Option>
                      })}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <Form.Item label={__('newTask-reporter')}>
                  {getFieldDecorator('reportUserId', { rules: [{ required: true, message: '必填项!' }] })(
                    <Select>
                      {managerList.map(item => {
                        return <Option value={item.userId} key={item.userId}>{item.realname}</Option>
                      })}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label={__('newTask-plan-is')}>
                  {getFieldDecorator('planExpend', { rules: [{ required: true, message: '必填项!' }] })(<InputNumber />)}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Form.Item label={__('newTask-schedule-start-deadline')}>
                  {getFieldDecorator('time', { rules: [{ type: 'array', required: true, message: '必填项!' }] })(<RangePicker />)}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Form.Item label="任务描述">
                  {getFieldDecorator('taskDesc', { rules: [{ required: true, message: '必填项!' }] })(<TextArea placeholder={__('form-item-goal-placeholder')} rows={4} />)}
                </Form.Item>
              </Col>
            </Row>
            <h3>核心交付物</h3>
            <Table
              columns={columns}
              loading={initLoading}
              rowSelection={rowSelection}
              pagination={false}
              showHeader={false}
              dataSource={unDelList} />
            <h3>{__('newTask-accessory')}</h3>
            <Form.Item>
              <Upload {...updataObj}>
                <Button type="primary" loading={uploading}>
                  {__('newTask-uploading')}
                </Button>
              </Upload>
            </Form.Item>
          </Form>
        </Spin>
      </div>
    )
  }
}

export default NewTask
