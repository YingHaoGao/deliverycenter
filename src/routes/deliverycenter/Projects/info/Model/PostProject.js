import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import moment from 'moment'
import { Form, Input, Select, Row, Col, Table, Spin } from '@uyun/components'

import __ from '@uyun/utils/i18n'
import { getContract, getDeliverableList, postAppAddPid, getDeptList } from '@/services/api'

import './PostProject.less'

const { TextArea } = Input
const { Option } = Select

const columns = [{
  title: '序号',
  dataIndex: 'index',
  key: 'index',
  align: 'center',
  width: '40px',
  render: (t, r, i) => i
}, {
  title: '名称',
  dataIndex: 'deliverableTitle',
  key: 'deliverableTitle'
}, {
  title: '阶段',
  dataIndex: 'substageTitle',
  key: 'substageTitle',
  align: 'center',
  width: '150px'
}, {
  title: '状态',
  dataIndex: 'status',
  key: 'status',
  align: 'center',
  width: '70px'
}, {
  title: '提交时间',
  dataIndex: 'createTime',
  key: 'createTime',
  align: 'center',
  width: '135px'
}]

@inject('projectsStore')
@observer
@Form.create()
class PostProject extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      deliverList: [],
      managerList: [],
      contractInfo: { actualCost: []},
      whether: 0
    }
  }

  componentDidMount () {
    this.props.onRef(this)
    this.getContractList()
    this.getDeliverable()
    this.getPrincipal()
  }

  getPrincipal = () => {
    const { projectInfo, projectsStore } = this.props
    projectsStore.getManagerNameList({ deptName : '交付中心', tenantId: window.USER_INFO.tenantId }).then(() => {
      this.setState({ managerList: projectsStore.manNameList })
    })
  }

  save = () => {
    const { projectInfo, projectsStore } = this.props
   
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({ loading: true })
        var obj = {}
        obj.applyJson = JSON.stringify(projectInfo)
        obj.modeId = 12
        obj.approvalUserId = values.approvalUserId
        obj.applyReason = values.applyReason
        obj.applyTime = moment((new Date()).getTime()).format('YYYY-MM-DD HH:MM:ss')
        obj.applyTitle = projectInfo.projectName
        obj.applyType = 12
        obj.projectId = projectInfo.id
        projectsStore.postAppAddPid(obj).then(() => {
          this.props.onClose()
          this.setState({ loading: false })
        })
      }
    })
  }

  getDeliverable = () => {
    const { projectInfo, projectsStore } = this.props
    projectsStore.getDeliverableList({ projectId: projectInfo.id }).then(() => {
      this.setState({ deliverList: projectsStore.delList })
    })
  }

  getContractList = () => {
    const { projectInfo, projectsStore } = this.props
    projectsStore.getMonitoring({
      projectId: this.props.projectInfo.id,
      stageId: this.props.projectInfo.stageType
    }).then(() => {
      this.setState({ contractInfo: projectsStore.monitor })
    })
  }

  whetherChange = (e) => {
    this.setState({
      whether: e.target.value
    })
  }

  render () {
    const { getFieldDecorator } = this.props.form
    const { contractInfo, loading, deliverList, managerList } = this.state
    console.log(contractInfo)
    const actual = contractInfo.actualCost.filter(k => k) || []
    const employees = actual.length
    var hours = 0
    actual.map(item => hours += (item.actual_expend || 0))

    return (
      <div className="basic-postProjects">
        <Spin spinning={loading}>
          <Row>
            <Col span={8}>{__('postproject-contract-no')}: --</Col>
            <Col span={8}>{__('postproject-reception-time')}: --</Col>
          </Row>
          <h3>{__('postproject-personnel-investment')}</h3>
          <Row>
            <Col span={8}>{__('postproject-total-employees')}: {employees}</Col>
            <Col span={8}>{__('postproject-working-days')}: {hours}</Col>
          </Row>
          <h3>{__('postproject-core-deliverables')}</h3>
          <Row>
            <Col span={8}>{__('postproject-total-deliverables')}: {deliverList.length}</Col>
          </Row>
          <Table columns={columns} pagination={false} dataSource={deliverList} />
          <Form>
            <Row>
              <Col span={24}>
                <Form.Item label={__('postproject-apply-instructions')}>
                  {getFieldDecorator('applyReason')(
                    <TextArea placeholder={__('form-item-goal-placeholder')} rows={4} />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Form.Item label="审批人">
                  {getFieldDecorator('approvalUserId', { rules: [{ required: true, message: '必填项!' }] })(
                    <Select onChange={this.substageLeaderChange}>
                      {managerList.map((item, i) => {
                        return <Option value={item.userId} key={i}>{item.realname}</Option>
                      })}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Spin>
      </div>
    )
  }
}

export default PostProject
