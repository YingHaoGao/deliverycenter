import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Form, Input, DatePicker, Button, Select, Row, Col, Spin, InputNumber, notification } from '@uyun/components'
import moment from 'moment'

import __ from '@uyun/utils/i18n'

import './Edit.less'

const { RangePicker } = DatePicker
const { TextArea } = Input
const { Option } = Select
const ButtonGroup = Button.Group

const dateFormat = 'YYYY-MM-DD'

var id = 0

@inject('projectsStore')
@observer
@Form.create()
class Edit extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      loadingDel: false,
      params: {},
      deliverablesKeys: [],
      deliverList: []
    }
  }

  componentDidMount () {
    this.props.onRef(this)
    this.getDeliverTable()
    this.getPrincipal()
  }

  save = () => {
    const { projectInfo, projectsStore, stageInfo } = this.props

    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        projectsStore.getWeightExceed100({
          projectId: projectInfo.id,
          stageId: stageInfo.id,
          weight: values.weight
        }).then(() => {
          if (projectsStore.weight.data) {
            this.setState({ loading: true }, () => {
              var deliverableTitle = []
              values.deliverableTitle.map(item => {
                deliverableTitle.push({
                  deliverableTitle: item,
                  id: ''
                })
              })
              this.state.deliverList.map(item => {
                if (!item.end) {
                  deliverableTitle.push({
                    deliverableTitle: item.deliverableTitle,
                    id: item.id
                  })
                }
              })

              const sub = { ...stageInfo, ...values, deliverableTitle: deliverableTitle }
              projectsStore.postSubUpdata(sub).then(() => {
                this.props.onClose()
                this.props.onRefresh()
                this.setState({ loading: false })
                notification.success({
                  message: '修改子阶段成功'
                })
              })
            })
          }
        })
      }
    })
  }

  getPrincipal = () => {
    var params = {
      name: window.USER_INFO.realname,
      tenantId: window.USER_INFO.tenantId,
      userId: window.USER_INFO.userId
    }
    this.props.projectsStore.getDeptList(params)
  }

  getDeliverTable = () => {
    const { projectInfo, stageInfo, projectsStore } = this.props

    this.setState({ loadingDel: true }, () => {
      projectsStore.getStageDeliverable1({
        subStageId: stageInfo.id
      }).then(() => {
        this.setState({ deliverList: projectsStore.deliverable1, loadingDel: false })
      })
    })
  }

  removeDeliverables = id => {
    const deliverList = this.state.deliverList

    this.setState({
      deliverList: deliverList.filter(key => key.id !== id)
    })
  }

  addDeliverables = () => {
    const deliverList = this.state.deliverList
    const nextKeys = deliverList.concat({ id: id++, end: true })
    this.setState({
      deliverList: nextKeys
    })
  }

  render () {
    const { stageInfo, projectsStore } = this.props
    const { getFieldDecorator } = this.props.form
    const { deliverList, loading, loadingDel } = this.state
    const { deptList = [] } = projectsStore

    const formItems = deliverList.map((item, index) => (
      <Form.Item
        className="border"
        label={__('addSubphase-delivery-name')}
        required={false}
        key={index}
      >
        &nbsp;
        { !item.end ? (item.deliverableTitle) : (getFieldDecorator('deliverableTitle[' + item.id + ']', {
          rules: [
            {
              type: 'string',
              required: true,
              message: '必填项!'
            }
          ]
        })(<Input className="name" />)) }
        <ButtonGroup type="link">
          <a className="Rabsolute" onClick={() => this.removeDeliverables(item.id)}>{__('button-del')}</a>
        </ButtonGroup>
      </Form.Item>
    ))

    return (
      <div className="basic-Edit">
        <Spin spinning={loading}>
          <Row>
            <Col span={24}>
            <Form.Item label={__('addSubphase-phase-name')}>
              {stageInfo.substageTitle}
            </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
            <Form.Item label={__('stageTask-stage-manager')}>
              {getFieldDecorator('substageLeader', { initialValue: stageInfo.substageLeader, rules: [{ required: true, message: '必填项!' }] })(
                <Select>
                  {deptList.map((item, i) => {
                    return <Option value={item.userId} key={i}>{item.realname}</Option>
                  })}
                </Select>
              )}
            </Form.Item>
            </Col>
            <Col span={12}>
            <Form.Item label={__('addSubphase-weight')}>
              {getFieldDecorator('weight', { initialValue: stageInfo.weight, rules: [{ required: true, message: '必填项!' }] })(<InputNumber />)}%
            </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
            <Form.Item label={__('addSubphase-planned-days')}>
              {getFieldDecorator('planExpend', { initialValue: stageInfo.planExpend, rules: [{ required: true, message: '必填项!' }] })(<InputNumber />)}
            </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item label={__('newTask-schedule-start-deadline')}>
                {getFieldDecorator('time', { initialValue: [moment(stageInfo.startTime, dateFormat), moment(stageInfo.endTime, dateFormat)], rules: [{ type: 'array', required: true, message: '必填项!' }] })(<RangePicker format={dateFormat} />)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item label={__('addSubphase-description-stages')}>
                {getFieldDecorator('substageDesc', { initialValue: stageInfo.substageDesc, rules: [{ required: true, message: '必填项!' }] })(<TextArea placeholder={__('form-item-goal-placeholder')} rows={4} />)}
              </Form.Item>
            </Col>
          </Row>
          <h3>
            <Form.Item label={__('newTask-core-deliverables')}>
              <Button type="primary" onClick={this.addDeliverables}>{__('button-newly')}</Button>
            </Form.Item>
          </h3>
        </Spin>
        <Spin  spinning={loadingDel}>
          {formItems}
        </Spin>
      </div>
    )
  }
}

export default Edit
