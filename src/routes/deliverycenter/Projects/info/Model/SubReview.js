import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Form, Input, List, Row, Col, message, Select, Table } from '@uyun/components'

import __ from '@uyun/utils/i18n'

import { postConApproval, getStageDeliverable } from '@/services/api'

import './SubReview.less'

const { TextArea } = Input
const { Option } = Select

const columns = [
  {
    title: '名称',
    dataIndex: 'deliverableTitle',
    key: 'deliverableTitle'
  }
]

@inject('projectsStore')
@observer
@Form.create()
class SubReview extends Component {
  constructor (props) {
    super(props)
    this.state = {
      initLoading: false,
      deliverList: [],
      reason: ''
    }
  }

  componentDidMount () {
    this.props.onRef(this)
    this.getDeliverTable()
  }

  save = () => {
    const { stageInfo, projectsStore, projectInfo } = this.props
    const { reason } = this.state

    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        values.applyTime = (new Date()).getTime()
        values.applyTitle = stageInfo.substageTitle
        values.applyJson = JSON.stringify(stageInfo)
        values.applyType = 13
        values.modeId = 13
        values.flag = 1
        values.createUser = projectsStore.deptList.find(k => k.userId === values.createUserId).realname
        values.projectId = projectInfo.id

        projectsStore.postAppAddPid(values).then(() => {
          if (projectsStore.pid.data) {
            this.props.onClose()
          }
        })
      }
    })
  }

  getDeliverTable = () => {
    const { projectInfo, stageInfo, projectsStore } = this.props

    projectsStore.getStageDeliverable1({
      subStageId: stageInfo.id
    }).then(() => {
      this.setState({ deliverList: projectsStore.deliverable1, initLoading: false })
    })
  }

  reasonChange = e => {
    this.setState({ reason: e.target.value })
  }

  render () {
    const { stageInfo, projectsStore } = this.props
    const { getFieldDecorator } = this.props.form
    const { deliverList = [], initLoading } = this.state
    const { deptList = [] } = projectsStore

    return (
      <div className="basic-SubReview">
        <Form>
          <Row>
            <Col span={12}>
              <Form.Item label="阶段名称">
                {stageInfo.substageTitle}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="阶段负责人">
                {stageInfo.substageLeaderName}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item label={__('addSubphase-weight')}>
                {stageInfo.weight}%
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label={__('newTask-plan-time') + '(h)'}>
                {stageInfo.planExpend}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item label={__('stageTask-plan')}>
                {stageInfo.startTime} - {stageInfo.endTime}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              阶段说明：
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              {stageInfo.substageDesc}
            </Col>
          </Row>
          <h3>{__('postproject-core-deliverables')}</h3>
          <Table
            rowKey="id"
            dataSource={deliverList}
            columns={columns}
            pagination={false}
            loading={initLoading}
          />
          <Row>
            <Form.Item label="申请说明">
              {getFieldDecorator('applyReason', { rules: [{ required: true, message: '必填项!' }] })(
                <TextArea />
              )}
            </Form.Item>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item label="审批人">
                {getFieldDecorator('createUserId', { initialValue: stageInfo.substageLeader, rules: [{ required: true, message: '必填项!' }] })(
                  <Select>
                    {deptList.map((item, i) => {
                      return <Option value={item.userId} key={i}>{item.realname}</Option>
                    })}
                  </Select>
                )}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}

export default SubReview
