import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Collapse, Row, Col, Table } from '@uyun/components'

import './index.less'

// 售前阶段表格
const columnsCommunicate = [
  {
    title: '任务名称',
    dataIndex: 'taskTitle',
    key: 'taskTitle'
  },
  {
    title: '任务类型',
    dataIndex: 'taskType',
    key: 'taskType',
    render: text => global.params.taskType.find(k => k.key === text).label
  },
  { title: '负责人', dataIndex: 'taskLeaderName', key: 'taskLeaderName' },
  { title: '预计工时(h)', key: 'planExpend', dataIndex: 'planExpend' },
  { title: '截止时间', key: 'endTime', dataIndex: 'endTime' },
  {
    title: '状态',
    key: 'status',
    dataIndex: 'status',
    render: text => (global.params.taskStatus.find(k => k.key === text).label)
  }
]

@inject('projectsStore')
@observer
class ProjectInfo extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      loading: false,
      modelName: 'NewTask',
      modelTitle: '',
      stageList: [],
      projectId: props.projectInfo.id,
      stageInfo: {},
      columnsQuotationLog: [
        {
          title: '序号',
          dataIndex: 'clueTitle',
          key: 'clueTitle'
        },
        { title: '任务名称', dataIndex: 'quotationTitle', key: 'quotationTitle' },
        { title: '预算金额(含税/元)', key: 'totalPrice', dataIndex: 'totalPrice' },
        {
          title: '状态',
          key: 'quotationStatus',
          dataIndex: 'quotationStatus',
          render: text => (global.params.quotationStatus.find(k => k.key === text).label)
        },
        {
          title: '更新时间',
          key: 'createTime',
          dataIndex: 'createTime'
        },
        {
          title: '操作',
          key: 'x6',
          dataIndex: 'x6',
          render: (a, row) => {
            switch (row.quotationStatus) {
              case '0':
                return (<a onClick={this.perfect(row)}>完善报表</a>)
              case '1':
                return (<span className="gay">完善报表</span>)
              case '2':
                return (<span className="gay">完善报表</span>)
              case '3':
                return '-'
            }
          }
        }
      ],
      columnsProBuget: [
        {
          title: '序号',
          dataIndex: 'clueTitle',
          key: 'clueTitle'
        },
        { title: '名称', dataIndex: 'budgetName', key: 'budgetName' },
        { title: '预算金额(含税/元)', key: 'totalPrices', dataIndex: 'totalPrices' },
        {
          title: '状态',
          key: 'status',
          dataIndex: 'status',
          render: text => (global.params.bugetStatus.find(k => k.key === text).label)
        },
        {
          title: '更新时间',
          key: 'creatTime',
          dataIndex: 'creatTime'
        },
        {
          title: '操作',
          key: 'x6',
          dataIndex: 'x6',
          render: (a, row) => {
            switch (row.status) {
              case '0':
                return (<span className="gay">预算审批</span>)
              case '1':
                return (<a onClick={this.edit(row)}>编辑</a>)
            }
          }
        }
      ]
    }
  }

  componentDidMount () {
    this.getList()
    this.getQuotation()
    this.getBudget()
  }

  getList = (e) => {
    var _this = e || this
    _this.setState({ loading: true }, () => {
      _this.props.projectsStore.getSubStageQueryStageTask({
        projectId: _this.state.projectId,
        stageId: 1
      }).then(res => {
        _this.setState({ loading: false })
      })
    })
  }

  perfect = (info) => {
    return () => {

    }
  }

  edit = (info) => {
    return () => {

    }
  }

  getQuotation = () => {
    const { projectsStore, projectInfo } = this.props
    projectsStore.getProjectQuotation({
      createUserId: projectInfo.createUserId,
      projectId: projectInfo.id
    })
  }

  getBudget = () => {
    const { projectsStore, projectInfo } = this.props
    projectsStore.getBudgetList({ projectId: projectInfo.id })
  }

  render () {
    const { stageTask, quotation, budgetList } = this.props.projectsStore
    const { taskStatus } = global.params

    return (
      <div className="basic-preSales">
        <div className="tab-two-content">
          <Collapse defaultActiveKey={['1', '2', '3']} onChange={this.callback3}>
            <Collapse.Card header="阶段任务" key="1">
              {stageTask.map(item => {
                return (
                  <div>
                    <div style={{ fontSize: '14px', fontWeight: 'bold', color: '#3883f8' }}>{item.substageTitle || '暂无'}</div>
                      <Row>
                        <Col span={6}>创建时间:&ensp;{item.createTime}</Col>
                        <Col span={1}>状态:&ensp;{item.taskType}</Col>
                        <Col style={{ color: '#3883f8', cursor: 'pointer' }} span={1}>{taskStatus.find(k => k.key === item.stageId).label}</Col>
                      </Row>
                    <div >
                      <Table rowKey="id" columns={columnsCommunicate} dataSource={item.stageTaskList} expandedRowRender={this.expandedRowRenderCom} pagination={false} />
                    </div>
                  </div>
                )
              })}
	          </Collapse.Card>
	          <Collapse.Card header="报价单记录" key="2">
	            <div >
	              <Table rowKey="id" columns={this.state.columnsQuotationLog} dataSource={quotation} pagination={false} />
	            </div>
	          </Collapse.Card>
	          <Collapse.Card header="项目预算" key="3">
	            <div >
	              <Table rowKey="id" columns={this.state.columnsProBuget} dataSource={budgetList.list} pagination={false} />
	            </div>
	          </Collapse.Card>
	        </Collapse>
        </div>
      </div>
    )
  }
}

export default ProjectInfo
