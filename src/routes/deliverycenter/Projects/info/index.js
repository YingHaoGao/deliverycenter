import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Button, Row, Col, Tabs, Modal } from '@uyun/components'
import { Link } from 'react-router-dom'

import PageHeader from '@/components/PageHeader'
import __ from '@uyun/utils/i18n'

import Intro from './Intro'
import Information from './Information/index'
import PreSales from './PreSales/index'
import Contract from './Contract/index'
import Transition from './Transition/index'
import Serve from './Serve/index'

import AddSubphase from './Model/AddSubphase'
import PostProject from './Model/PostProject'
import AssignManager from './Model/AssignManager'

import './index.less'

const TabPane = Tabs.TabPane

const tabList = [
  {
    key: 'Information',
    name: __('project-tab-information')
  },
  {
    key: 'PreSales',
    name: __('project-tab-preSales')
  },
  {
    key: 'Contract',
    name: __('project-tab-contract')
  },
  {
    key: 'Transition',
    name: __('project-tab-transition')
  },
  {
    key: 'Serve',
    name: '服务阶段'
  }
]
const comObj = {
  Information (info) {
    return <Information projectInfo={info} />
  },
  PreSales (info) {
    return <PreSales projectInfo={info} />
  },
  Contract (info) {
    return <Contract projectInfo={info} />
  },
  Transition (info) {
    return <Transition projectInfo={info} />
  },
  Serve (info) {
    return <Serve projectInfo={info} />
  },
  AddSubphase (onClose, onRef, info) {
    return <AddSubphase onClose={onClose} onRef={onRef} projectInfo={info} />
  },
  PostProject (onClose, onRef, info) {
    return <PostProject onClose={onClose} onRef={onRef} projectInfo={info} />
  },
  AssignManager (onClose, onRef, info) {
    return <AssignManager onClose={onClose} onRef={onRef} projectInfo={info} />
  }
}

var child = {}

@inject('projectsStore')
@observer
class ProjectInfo extends Component {
  constructor (props) {
    super(props)

    this.state = {
      projectId: '',
      visible: false,
      modelName: 'AddSubphase',
      modelTitle: '',
      projectInfo: {},
      tabKey: tabList[0].key
    }
    this.onClose = this.onClose.bind(this)
  }

  componentDidMount () {
    const { match, projectsStore } = this.props

    this.setState({ projectId: match.params.id }, () => {
      this.getTableData()
      projectsStore.getCompanyInfoByProId(match.params.id)
    })
  }

  getTableData () {
    const { match } = this.props
    var params = { projectId: match.params.id }

    this.props.projectsStore.getProjectInfo(params)
  }

  tabChange (k) {

  }

  returnCom (k) {
    return comObj[k]
  }

  togModel = (tag, title) => {
    return (e) => {
      this.setState({ modelName: tag, modelTitle: title })
      this.onOpen()
    }
  }

  onClose = () => {
    this.setState({ visible: false })
  }

  onOpen = () => {
    this.setState({ visible: true })
  }

  onRef (ref) {
    child = ref
  }

  save () {
    child.save()
  }

  render () {
    const { tabKey, modelName, visible, modelTitle, projectId } = this.state
    const projectInfo = this.props.projectsStore.projectInfo
    console.log(this.props.projectsStore)
    const editweekly = {
      pathname: '/deliverycenter/weeklys/editweekly',
      query: {
        projectId: projectId
      }
    }

    return (
      <div className="basic-projectInfo">
        <PageHeader />

        <Row>
          <Col span={20}>
            <header>
              <div className="buttonBox">
                <Button onClick={this.togModel('AddSubphase', __('button-add') + __('name-subphase'))}>{__('button-add') + __('name-subphase')}</Button>
                <Button onClick={this.togModel('PostProject', __('button-post-project'))}>{__('button-post-project')}</Button>
                <Button><Link to={editweekly}>新建周报</Link></Button>
                <Button onClick={this.togModel('AssignManager', __('button-designate-manager'))}>{__('button-designate-manager')}</Button>
              </div>
              <Intro info={projectInfo} />
            </header>
            <Tabs defaultActiveKey={tabKey} onChange={this.tabChange}>
              {tabList.map((item, i) => {
                return (
                  <TabPane tab={item.name} key={item.key}>
                    {this.returnCom(item.key)(projectInfo)}
                  </TabPane>
                )
              })}
            </Tabs>
          </Col>
        </Row>

        <Modal
          destroyOnClose
          title={modelTitle}
          visible={visible}
          onOk={this.save}
          onCancel={this.onClose}
        >{this.returnCom(modelName)(this.onClose, this.onRef, projectInfo)}</Modal>
      </div>
    )
  }
}

export default ProjectInfo
