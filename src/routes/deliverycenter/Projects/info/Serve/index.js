import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Collapse, Table } from '@uyun/components'

import ServeInfo from './ServeInfo'
import StageTask from './StageTask'

// import './index.less'

const columns2 = [
  {
    title: '名称',
    dataIndex: 'clueTitle',
    key: 'clueTitle',
    fixed: 'left'
  },
  { title: '计划收入时间', dataIndex: 'userName', key: 'userName' },
  { title: '实际收入时间', key: 'depteName', dataIndex: 'depteName' },
  { title: '文本收入时间', key: 'createTime', dataIndex: 'createTime' },
  {
    title: '任务状态',
    key: 'x',
    dataIndex: 'operate'
  },
  {
    title: '节点状态',
    key: 'status',
    dataIndex: 'status'
  }
]
const columns3 = [
  {
    title: '批次',
    dataIndex: 'clueTitle',
    key: 'clueTitle',
    fixed: 'left'
  },
  { title: '是否达到开票条件', dataIndex: 'userName', key: 'userName' },
  { title: '开票金额(元)', key: 'depteName', dataIndex: 'depteName' },
  { title: '开票时间', key: 'createTime', dataIndex: 'createTime' },
  {
    title: '开票状态',
    key: 'x3',
    dataIndex: 'operate'
  }
]

@inject('projectsStore')
@observer
class Serve extends Component {
  render () {
    const {  projectsStore } = this.props
    const { projectInfo } = projectsStore

    return (
      <div className="basic-serve">
        <Collapse defaultActiveKey={[]}>
          <Collapse.Card header="服务信息" key="1">
            <ServeInfo />
          </Collapse.Card>
          <Collapse.Card header="阶段任务" key="2">
            <StageTask projectInfo={projectInfo} />
          </Collapse.Card>
          <Collapse.Card header="收入节点" key="3">
            <div >
              <Table rowKey={record => record.id} columns={columns2} pagination={false} />
            </div>
          </Collapse.Card>
          <Collapse.Card header="开票记录" key="4">
            <div >
              <Table rowKey={record => record.id} columns={columns3} pagination={false} />
            </div>
          </Collapse.Card>
        </Collapse>
      </div>
    )
  }
}

export default Serve
