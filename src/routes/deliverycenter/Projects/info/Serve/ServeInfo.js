import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Row, Col, Modal, Spin } from '@uyun/components'

import { changeTaskStatus } from '@/services/api'

// import './index.less'

@inject('taskStore')
@observer
class ServerInfo extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      loading: false,
      modelName: 'NewTask',
      modelTitle: '',
      stageList: [],
      stageInfo: {}
    }
  }

  componentDidMount () {
  }

  statusChange = (item) => {
    return (e) => {
      item.status = e.target.value
      changeTaskStatus(item).then(res => {
        if (res.code === 0) {
          this.getList()
        }
      })
    }
  }

  onClose = () => {
    this.setState({ visible: false })
  }

  onOpen () {
    this.setState({ visible: true })
  }

  render () {
    const { projectInfo } = this.props
    const { visible, modelTitle, loading } = this.state

    return (
      <div>
        <Spin spinning={loading} />
        <div className="bodyer">
          <Row>
            <Col span={8} className="">
              <span name="name">质保起始/结束时间&nbsp;:&nbsp;</span>
              <span name="value">2019-12-11/2019-12-12</span>
            </Col>
          </Row>
          <Row>
            <Col span={6} className="">
              <span name="name">巡检周期&nbsp;:&nbsp;</span>
              <span name="value">月度</span>
            </Col>
            <Col span={6} className="">
              <span name="name">巡检次数&nbsp;:&nbsp;</span>
              <span name="value">12次</span>
            </Col>
          </Row>
          <Row>
            <Col span={6} className="">
              <span name="name">是否驻场&nbsp;:&nbsp;</span>
              <span name="value">是</span>
            </Col>
            <Col span={6} className="">
              <span name="name">驻场人年&nbsp;:&nbsp;</span>
              <span name="value">7</span>
            </Col>
            <Col span={7} className="">
              <span name="name">驻场起始/结束时间&nbsp;:&nbsp;</span>
              <span name="value">2019-12-11/2019-12-12</span>
            </Col>
          </Row>
        </div>
        <Modal
          title={modelTitle}
          visible={visible}
          onOk={this.save}
          onCancel={this.onClose}
          destroyOnClose
        >{<div>服务阶段弹出框</div>}</Modal>
      </div>
    )
  }
}

export default ServerInfo
