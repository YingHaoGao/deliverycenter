import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Table, Button, Row, Col, Modal, Popconfirm, Empty, Radio, Spin } from '@uyun/components'

import NewTask from '../Model/NewTask'
import SubReview from '../Model/SubReview'
import Edit from '../Model/Edit'

import { getSubStageQueryStageTask, delSubStage, changeTaskStatus } from '@/services/api'

// import './index.less'

const ButtonGroup = Button.Group

const columns = [{
  title: '任务名称',
  dataIndex: 'taskTitle',
  render: (taskTitle, record) => (<Link to={'/task/' + record.substageId}>{taskTitle}</Link>),
  key: 'taskTitle',
  width: '267px'
}, {
  title: '负责人',
  dataIndex: 'taskLeader',
  key: 'taskLeader',
  width: '83px',
  align: 'center'
}, {
  title: '实际/预计工时（h）',
  dataIndex: 'actualExpend',
  key: 'actualExpend',
  render: (text, record) => {
    return record.actualExpend + '/' + record.planExpend
  },
  width: '129px',
  align: 'center'
}, {
  title: '状态',
  dataIndex: 'status',
  key: 'state',
  width: '92px',
  align: 'center'
}, {
  title: '交付物',
  dataIndex: 'uploadDeliverable',
  key: 'uploadDeliverable',
  render: (text, record) => {
    return record.uploadDeliverable + '/' + record.relatedDeliverable
  },
  width: '82px',
  align: 'center'
}, {
  title: '创建时间',
  dataIndex: 'createTime',
  key: 'createTime',
  align: 'center'
}]

const comObj = {
  NewTask (onClose, onRef, stageInfo, projectInfo) {
    return <NewTask onClose={onClose} onRef={onRef} stageInfo={stageInfo} projectInfo={projectInfo} />
  },
  SubReview (onClose, onRef, stageInfo, projectInfo) {
    return <SubReview onClose={onClose} onRef={onRef} stageInfo={stageInfo} projectInfo={projectInfo} />
  },
  Edit (onClose, onRef, stageInfo, projectInfo) {
    return <Edit onClose={onClose} onRef={onRef} stageInfo={stageInfo} projectInfo={projectInfo} />
  }
}

@inject('taskStore')
@observer
class StageTask extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      loading: false,
      modelName: 'NewTask',
      modelTitle: '',
      stageList: [],
      stageInfo: {}
    }
  }

  componentDidMount () {
    this.getList()
  }

  getList = () => {
    const { projectInfo } = this.props
    console.log(projectInfo)
    console.log(this.props)
    this.setState({ loading: true })
    getSubStageQueryStageTask({
      projectId: projectInfo.id,
      stageId: 4
    }).then(res => {
      if (res.code === 0) {
        this.setState({ stageList: res.resultMap.data, stageInfo: res.resultMap.data[0] })
      }
    })
    this.setState({ loading: false })
  }

  onClose = () => {
    this.setState({ visible: false })
  }

  onOpen () {
    this.setState({ visible: true })
  }

  delClick = (id) => {
    return (e) => {
      delSubStage({ id: id })
    }
  }

  reviewClick = () => {
    this.setState({ modelName: 'SubReview', modelTitle: '发起评审' })
    this.onOpen()
  }

  editClick = (stageInfo) => {
    return () => {
      this.setState({ modelName: 'Edit', modelTitle: '编辑阶段', stageInfo: stageInfo })
      this.onOpen()
    }
  }

  addTaskClick = () => {
    this.setState({ modelName: 'NewTask', modelTitle: '添加任务' })
    this.onOpen()
  }

  onRef (ref) {
    child = ref
  }

  statusChange = (item) => {
    return (e) => {
      item.status = e.target.value
      changeTaskStatus(item).then(res => {
        if (res.code === 0) {
          this.getList()
        }
      })
    }
  }

  ifDis (key, item) {
    const review = item.taskType
    // const sub = item.status
    const sub = '0'
    const obj = {
      del: () => {
        if (sub === '0') {
          return true
        }
        if (sub === '1') {
          return true
        }
        if (sub === '2') {
          return true
        }
        if (sub === '3') {
          return true
        }
        if (sub === '4') {
          if (review === '1') {
            return true
          }
          return false
        }
        if (sub === '5') {
          return true
        }
      },
      edit: () => {
        if (sub === '0') {
          return false
        }
        if (sub === '1') {
          return true
        }
        if (sub === '2') {
          return true
        }
        if (sub === '3') {
          return true
        }
        if (sub === '4') {
          return false
        }
        if (sub === '5') {
          return true
        }
      },
      review: () => {
        if (review === '0') {
          return false
        }
        return true
      },
      'add-task': () => {
        if (sub === '0') {
          return false
        }
        if (sub === '1') {
          return true
        }
        if (sub === '2') {
          return true
        }
        if (sub === '3') {
          return true
        }
        if (sub === '4') {
          if (review === '1') {
            return false
          }
          return true
        }
        if (sub === '5') {
          return true
        }
      }
    }

    return obj[key]()
  }

  render () {
    const { projectInfo } = this.props
    const { modelName, visible, modelTitle, stageList, stageInfo, loading } = this.state

    return (
      <div>
        <Spin spinning={loading}>
          {/* {stageList.map((item, i) => { */}
          <div className="basic-stageTask">
            <header>
              <span className="tableName">标题</span>
              <div className="buttonBox">
                <ButtonGroup type="link" style={{ marginRight: 10 }}>
                  <Popconfirm title="是否确认删除" onConfirm={'hah'} okText="Yes" cancelText="No">
                    <a disabled={false}>删除</a>
                  </Popconfirm>
                  <a onClick={this.editClick(stageList[0])} disabled={false}>编辑</a>
                  <a onClick={this.reviewClick} disabled={false}>发起评审</a>
                  <a onClick={this.addTaskClick} disabled={false}>添加任务</a>
                </ButtonGroup>
                <Radio.Group value={12} onChange={''}>
                  <Radio.Button value="0" disabled={false}>开始</Radio.Button>
                  <Radio.Button value="1">完成</Radio.Button>
                </Radio.Group>
              </div>
            </header>
            <div className="bodyer">
              <Row>
                <Col span={6} className="">
                  <span name="name">阶段负责人&nbsp;:&nbsp;</span>
                  <span name="value">某某某</span>
                </Col>
                <Col span={6} className="">
                  <span name="name">评审状态&nbsp;:&nbsp;</span>
                  <span name="value">状态</span>
                </Col>
                <Col span={5} className="">
                  <span name="name">子阶段状态&nbsp;:&nbsp;</span>
                  <span name="value">状态</span>
                </Col>
                <Col span={7} className="">
                  <span name="name">创建时间&nbsp;:&nbsp;</span>
                  <span name="value">2019-12-10 10:08</span>
                </Col>
              </Row>
              <Row>
                <Col span={6} className="">
                  <span name="name">阶段权重&nbsp;:&nbsp;</span>
                  <span name="value">39%</span>
                </Col>
                <Col span={6} className="">
                  <span name="name">核心交付物&nbsp;:&nbsp;</span>
                  <span name="value">哈/嘿</span>
                </Col>
                <Col span={5} className="">
                  <span name="name">实际/预计工时&nbsp;:&nbsp;</span>
                  <span name="value">13/14</span>
                </Col>
                <Col span={7} className="">
                  <span name="name">计划开始/计划完成&nbsp;:&nbsp;</span>
                  <span name="value">2019-12-11/2019-12-12</span>
                </Col>
              </Row>
              <Row>
                <Col span={24} className="">
                  <span name="name">备注&nbsp;:&nbsp;</span>
                  <span name="value">这是备注</span>
                </Col>
              </Row>
            </div>
            <Table columns={columns} pagination={false} childrenColumnName="childList" dataSource={''} />
          </div>
          {/* })} */}
          {/* {stageList.length === 0 ? (<Empty type="table" />) : null} */}
        </Spin>

        <Modal
          title={modelTitle}
          visible={visible}
          onOk={this.save}
          onCancel={this.onClose}
          destroyOnClose
        >{comObj[modelName](this.onClose, this.onRef, stageInfo, projectInfo)}</Modal>
      </div>
    )
  }
}

export default StageTask
