import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Collapse, Row, Col, Table } from '@uyun/components'

import './index.less'

const rowSelection1 = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows)
  },
  getCheckboxProps: record => ({
    disabled: record.name === 'Disabled User', // Column configuration not to be checked
    name: record.name
  })
}
const columns1 = [
  {
    title: '名称',
    dataIndex: 'clueTitle',
    key: 'clueTitle',
    fixed: 'left'
  },
  {
    title: '比例(%)',
    key: 'companyName',
    dataIndex: 'companyName'
  },
  { title: '计划回款金额(元)',
    key: 'companyIndustry',
    dataIndex: 'companyIndustry'
  },
  { title: '实际回款金额(元)',
    dataIndex: 'status',
    key: 'status'
  },
  { title: '计划回款时间', dataIndex: 'userName', key: 'userName' },
  { title: '预计回款时间', key: 'depteName', dataIndex: 'depteName' },
  { title: '实际回款时间', key: 'createTime', dataIndex: 'createTime' },
  {
    title: '任务状态',
    key: 'xoperate',
    dataIndex: 'operate'
  },
  {
    title: '节点状态',
    key: 'xstatus',
    dataIndex: 'status'
  }
]
const columns2 = [
  {
    title: '名称',
    dataIndex: 'clueTitle',
    key: 'clueTitle',
    fixed: 'left'
  },
  { title: '计划收入时间', dataIndex: 'userName', key: 'userName' },
  { title: '实际收入时间', key: 'depteName', dataIndex: 'depteName' },
  { title: '文本收入时间', key: 'createTime', dataIndex: 'createTime' },
  {
    title: '任务状态',
    key: 'x',
    dataIndex: 'operate'
  },
  {
    title: '节点状态',
    key: 'status',
    dataIndex: 'status'
  }
]
const columns3 = [
  {
    title: '批次',
    dataIndex: 'clueTitle',
    key: 'clueTitle',
    fixed: 'left'
  },
  { title: '是否达到开票条件', dataIndex: 'userName', key: 'userName' },
  { title: '开票金额(元)', key: 'depteName', dataIndex: 'depteName' },
  { title: '开票时间', key: 'createTime', dataIndex: 'createTime' },
  {
    title: '开票状态',
    key: 'x3',
    dataIndex: 'operate'
  }
]
const columns4 = [
  {
    title: '序号',
    dataIndex: 'clueTitle',
    key: 'clueTitle',
    fixed: 'left'
  },
  { title: '名称', dataIndex: 'userName', key: 'userName' },
  { title: '预算金额(含税/元)', key: 'depteName', dataIndex: 'depteName' },
  {
    title: '实际成本',
    key: 'x4',
    dataIndex: 'operate'
  }
]

@inject('projectsStore')
@observer
class ProjectInfo extends Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }

  componentDidMount () {
    this.getConPay()
    this.getConAtt()
  }

  getConPay = () => {
    const { projectsStore } = this.props
    projectsStore.getConPayIdList({ id: projectsStore.contractInfo.id })
  }

  getConAtt = () => {
    const { projectsStore, projectInfo } = this.props
    projectsStore.getContractAttachmentInfo({ projectId: projectInfo.id }).then(() => {
      this.getConPay()
    })
  }

  area = (info) => {
    const { areaNameList } = global.params
    const arr = info.split(',')

    var str = ''
    var obj = {}

    arr.map(k => {
      obj = areaNameList[k]
      if (obj) {
        str += obj.label ? obj.label : obj
      }
    })
    return str
  }

  render () {
    const { contractInfo, conPayIdList } = this.props.projectsStore
    const { customerDepartment, developer, contractCategories, contractType } = global.params

    return (
      <div className="basic-contract">
      	<div className="tab-three-content">
	        <div className="basic-information">
	          <div>
	            <Collapse defaultActiveKey={[]}>
	              <Collapse.Card header="基本信息" key="1">
	                <div>
	                  <Row>
	                    <Col span={7}>合同编号:&ensp;{contractInfo.contractCode || '-'}</Col>
	                    <Col span={8}>合同名称:&ensp;{contractInfo.revName || '-'}</Col>
	                  </Row>
	                  <Row>
	                    <Col span={7}>合同甲方:&ensp;{contractInfo.firstParty}</Col>
	                    <Col span={4}>最终用户:&ensp;{contractInfo.endUser}</Col>
	                    <Col span={5}>客户类别:&ensp;{customerDepartment[contractInfo.customerType]}</Col>
	                    <Col span={8}>所属行业:&ensp;{contractInfo.industry || '-'}</Col>
	                  </Row>
	                  <Row>
	                    <Col span={7}>实施方:&ensp;{developer[contractInfo.customerType]}</Col>
	                    <Col span={4}>合同分类:&ensp;{contractCategories[contractInfo.contractCategories]}</Col>
	                    <Col span={5}>合同类别:&ensp;{contractType[contractInfo.contractType]}</Col>
	                    <Col span={8}>项目所在地:&ensp;{this.area(contractInfo.region || '')}</Col>
	                  </Row>
	                  <Row>
	                    <Col span={7}>合同金额:&ensp;{contractInfo.totalPrices}元</Col>
	                    <Col span={4}>合同毛利率:&ensp;{contractInfo.grossMargin}%</Col>
	                  </Row>
	                  <Row>
	                    <Col span={7}>履约/质量保证金:&ensp;{contractInfo.bond}元</Col>
	                    <Col span={4}>是否含集成商付款条件:&ensp;{contractInfo.isIntegratePayment === '1' ? '是' : '否'}</Col>
	                    <Col span={5}>质保期:&ensp;{contractInfo.warranty}年</Col>
	                    <Col span={8}>质保起算时间:&ensp;{contractInfo.beginTime}</Col>
	                  </Row>
	                  <Row>
	                    <Col span={7}>合同签约时间:&ensp;{contractInfo.createTime}</Col>
	                    <Col span={4}>初验时间:&ensp;{contractInfo.preliminaryInspection}</Col>
	                    <Col span={5}>终验时间:&ensp;{contractInfo.finalDays}</Col>
	                    <Col span={8}>质保截止时间:&ensp;{contractInfo.endTime}</Col>
	                  </Row>
	                </div>
	              </Collapse.Card>
	              <Collapse.Card header="回款节点" key="2">
	                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
	                  <Row>
	                    <Col style={{ width: '150px' }} span={12}>累计回款:&ensp;20%</Col>
	                    <Col style={{ width: '150px' }} span={12}>回款总额:&ensp;600,000</Col>
	                  </Row>
	                  <Row style={{ color: '#3883f8', cursor: 'pointer' }}>编辑回款计划</Row>
	                </div>
	                <div >
	                  <Table rowSelection={rowSelection1} rowKey="id" dataSource={conPayIdList} columns={columns1} pagination={false} />
	                </div>
	              </Collapse.Card>
	              <Collapse.Card header="收入节点" key="3">
	                <div >
	                  <Table rowKey={record => record.id} columns={columns2} pagination={false} />
	                </div>
	              </Collapse.Card>
	              <Collapse.Card header="开票记录" key="4">
	                <div >
	                  <Table rowKey={record => record.id} columns={columns3} pagination={false} />
	                </div>
	              </Collapse.Card>
	            </Collapse>
	          </div>
	        </div>
	        <div>
	          <Collapse defaultActiveKey={[]}>
	            <Collapse.Card header="成本预算" key="1">
	              <Row>
	                <Col span={3}>合同金额:&ensp;200万元</Col>
	                <Col style={{ color: '#3883f8', cursor: 'pointer' }} span={3}>查看明细</Col>
	                <Col span={3}>项目预算:&ensp;10万元</Col>
	                <Col style={{ color: '#3883f8', cursor: 'pointer' }} span={3}>查看明细</Col>
	              </Row>
	              <div >
	                <Table rowKey={record => record.id} columns={columns4} pagination={false} />
	              </div>
	            </Collapse.Card>
	          </Collapse>
	        </div>
	      </div>
      </div>
    )
  }
}

export default ProjectInfo
