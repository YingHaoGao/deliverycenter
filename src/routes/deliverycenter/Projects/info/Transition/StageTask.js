import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Table, Button, Row, Col, Modal, Popconfirm, Empty, Radio, Spin, notification } from '@uyun/components'

import NewTask from '../Model/NewTask'
import SubReview from '../Model/SubReview'
import Edit from '../Model/Edit'

import __ from '@uyun/utils/i18n'

import './StageTask.less'

const ButtonGroup = Button.Group

const columns = [{
  title: __('stageTask-task-name'),
  dataIndex: 'taskTitle',
  render: (taskTitle, record) => (<Link to={'/task/' + record.substageId}>{taskTitle}</Link>),
  key: 'taskTitle',
  width: '267px'
}, {
  title: __('stageTask-principal'),
  dataIndex: 'taskLeaderName',
  key: 'taskLeaderName',
  width: '83px',
  align: 'center'
}, {
  title: __('stageTask-man-hour'),
  dataIndex: 'actualExpend',
  key: 'actualExpend',
  render: (text, record) => {
    return record.actualExpend + '/' + record.planExpend
  },
  width: '129px',
  align: 'center'
}, {
  title: __('stageTask-state'),
  dataIndex: 'status',
  key: 'state',
  width: '92px',
  align: 'center',
  render: text => (global.params.taskStatus.find(k => k.key === text).label)
}, {
  title: __('stageTask-deliverable'),
  dataIndex: 'uploadDeliverable',
  key: 'uploadDeliverable',
  render: (text, record) => {
    return record.relatedDeliverable + '/' + record.uploadDeliverable
  },
  width: '82px',
  align: 'center'
}, {
  title: __('stageTask-creation-time'),
  dataIndex: 'createTime',
  key: 'createTime',
  align: 'center'
}]

const comObj = {
  NewTask (onClose, onRef, stageInfo, projectInfo, onRefresh) {
    return <NewTask onClose={onClose} onRef={onRef} onRefresh={onRefresh} stageInfo={stageInfo} projectInfo={projectInfo} />
  },
  SubReview (onClose, onRef, stageInfo, projectInfo, onRefresh) {
    return <SubReview onClose={onClose} onRef={onRef} onRefresh={onRefresh} stageInfo={stageInfo} projectInfo={projectInfo} />
  },
  Edit (onClose, onRef, stageInfo, projectInfo, onRefresh) {
    return <Edit onClose={onClose} onRef={onRef} onRefresh={onRefresh} stageInfo={stageInfo} projectInfo={projectInfo} />
  }
}

var child = {}

@inject('projectsStore')
@observer
class StageTask extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      loading: false,
      modelName: 'NewTask',
      modelTitle: '',
      stageList: [],
      projectId: props.projectInfo.id,
      stageInfo: {}
    }
  }

  componentDidMount () {
    this.getList()
  }

  getList = (e) => {
    var _this = e || this
    _this.setState({ loading: true }, () => {
      _this.props.projectsStore.getSubStageQueryStageTask({
        projectId: _this.state.projectId,
        stageId: 3
      }).then(res => {
        _this.setState({ loading: false })
      })
    })
  }

  onClose = () => {
    this.setState({ visible: false })
  }

  onOpen () {
    this.setState({ visible: true })
  }

  save () {
    child.save()
  }

  delClick = (id) => {
    return (e) => {
      this.props.projectsStore.delSubStage({ id: id }).then(() => {
        if (this.props.projectsStore.del) {
            notification.success({
              message: '删除子阶段成功'
            })
            this.getList()
        }
      })
    }
  }

  reviewClick = (stageInfo) => {
    return () => {
      this.setState({ modelName: 'SubReview', modelTitle: __('button-sub-phase-review'), stageInfo: stageInfo }, () => {
        this.onOpen()
      })
    }
  }

  editClick = (stageInfo) => {
    return () => {
      this.setState({ modelName: 'Edit', modelTitle: __('button-edit-stage'), stageInfo: stageInfo }, () => {
        this.onOpen()
      })
    }
  }

  addTaskClick = (stageInfo) => {
    return () => {
      this.setState({ modelName: 'NewTask', modelTitle: __('button-add-task'), stageInfo: stageInfo }, () => {
        this.onOpen()
      })
    }
  }

  onRef (ref) {
    child = ref
  }

  statusChange = (item) => {
    return (e) => {
      const params = {
        id: item.id,
        status: e.target.value
      }
      console.log(params, item)
      this.props.projectsStore.changeSubStageStatus(params).then(() => {
        this.getList()
      })
    }
  }

  ifDis (key, item) {
    // const review = item.taskType
    const review = '0'
    const sub = '0'
    const obj = {
      del: () => {
        if (sub === '0') {
          return true
        }
        if (sub === '1') {
          return true
        }
        if (sub === '2') {
          return true
        }
        if (sub === '3') {
          return true
        }
        if (sub === '4') {
          if (review === '1') {
            return true
          }
          return false
        }
        if (sub === '5') {
          return true
        }
      },
      edit: () => {
        if (sub === '0') {
          return false
        }
        if (sub === '1') {
          return true
        }
        if (sub === '2') {
          return true
        }
        if (sub === '3') {
          return true
        }
        if (sub === '4') {
          return false
        }
        if (sub === '5') {
          return true
        }
      },
      review: () => {
        if (review === '0') {
          return false
        }
        return true
      },
      'add-task': () => {
        if (sub === '0') {
          return false
        }
        if (sub === '1') {
          return true
        }
        if (sub === '2') {
          return true
        }
        if (sub === '3') {
          return true
        }
        if (sub === '4') {
          if (review === '1') {
            return false
          }
          return true
        }
        if (sub === '5') {
          return true
        }
      }
    }

    return obj[key]()
  }

  render () {
    const { projectInfo } = this.props
    const { modelName, visible, modelTitle, stageInfo, loading } = this.state
    const stageList = this.props.projectsStore.stageTask
    const ifStatus = (key, status) => {
      if (!status) {
        return '暂无'
      }
      var text = global.params[key].find(k => k.key === status).label
      return text
    }

    return (
      <div>
        <Spin spinning={loading}>
          {stageList.map((item, i) => {
            return (
              <div className="basic-stageTask" key={i}>
                <header>
                  <span className="tableName">{item.substageTitle || '暂无'}</span>
                  <div className="buttonBox">
                    <ButtonGroup type="link" style={{ marginRight: 10 }}>
                      <Popconfirm title={__('stagetask-del')} onConfirm={this.delClick(item.id)} okText="Yes" cancelText="No">
                        <a disabled={this.ifDis('del', item)}>{__('button-del')}</a>
                      </Popconfirm>
                      <a onClick={this.editClick(item)} disabled={this.ifDis('edit', item)}>{__('button-edit')}</a>
                      <a onClick={this.reviewClick(item)} disabled={this.ifDis('review', item)}>{__('button-a-review')}</a>
                      <a onClick={this.addTaskClick(item)} disabled={this.ifDis('add-task', item)}>{__('button-add-task')}</a>
                    </ButtonGroup>
                    <Radio.Group value={item.status} onChange={this.statusChange(item)}>
                      <Radio.Button value="0" disabled={item.status === '1'}>{__('button-start')}</Radio.Button>
                      <Radio.Button value="1">{__('button-accomplish')}</Radio.Button>
                    </Radio.Group>
                  </div>
                </header>
                <div className="bodyer">
                  <Row>
                    <Col span={6} className="">
                      <span name="name">{__('stageTask-stage-manager')}&nbsp;:&nbsp;</span>
                      <span name="value">{item.substageLeaderName}</span>
                    </Col>
                    <Col span={6} className="">
                      <span name="name">{__('stageTask-review-status')}&nbsp;:&nbsp;</span>
                      <span name="value">{ifStatus('stageStatus', item.status)}</span>
                    </Col>
                    <Col span={5} className="">
                      <span name="name">{__('stageTask-subphase-state')}&nbsp;:&nbsp;</span>
                      <span name="value">{ifStatus('taskStatus', item.substageType)}</span>
                    </Col>
                    <Col span={7} className="">
                      <span name="name">{__('stageTask-creation-time')}&nbsp;:&nbsp;</span>
                      <span name="value">{item.createTime}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={6} className="">
                      <span name="name">{__('stageTask-phase-weight')}&nbsp;:&nbsp;</span>
                      <span name="value">{item.weight}%</span>
                    </Col>
                    <Col span={6} className="">
                      <span name="name">{__('stageTask-core-deliverables')}&nbsp;:&nbsp;</span>
                      <span name="value">{item.uploadDeliverable || 0}/{item.totalDeliverable || 0}</span>
                    </Col>
                    <Col span={5} className="">
                      <span name="name">{__('stageTask-man-hour')}&nbsp;:&nbsp;</span>
                      <span name="value">{item.actualExpend || 0}/{item.planExpend || 0}</span>
                    </Col>
                    <Col span={7} className="">
                      <span name="name">{__('stageTask-plan')}&nbsp;:&nbsp;</span>
                      <span name="value">{item.startTime}/{item.endTime}</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={24} className="">
                      <span name="name">{__('stageTask-remark')}&nbsp;:&nbsp;</span>
                      <span name="value">{item.substageDesc}</span>
                    </Col>
                  </Row>
                </div>
                <Table columns={columns} pagination={false} childrenColumnName="childList" dataSource={item.stageTaskList} />
              </div>
            )
          })}
          {stageList.length === 0 ? (<Empty type="table" />) : null}
        </Spin>

        <Modal
          title={modelTitle}
          visible={visible}
          onOk={this.save}
          onCancel={this.onClose}
          destroyOnClose
        >{comObj[modelName](this.onClose, this.onRef, stageInfo, projectInfo, this.getList)}</Modal>
      </div>
    )
  }
}

export default StageTask
