import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Table, Button, Row, Col, Spin } from '@uyun/components'

import __ from '@uyun/utils/i18n'

import './Investment.less'

const ButtonGroup = Button.Group

const columns = [{
  title: __('investment-employee-ID'),
  dataIndex: 'realname',
  key: 'realname'
}, {
  title: __('investment-name'),
  dataIndex: 'age',
  key: 'name',
  width: '12%'
}, {
  title: __('investment-station'),
  dataIndex: 'age',
  key: 'station',
  width: '12%'
}, {
  title: __('investment-level'),
  dataIndex: 'age',
  key: 'level',
  width: '12%'
}, {
  title: __('investment-working-hours'),
  dataIndex: 'actual_expend',
  key: 'actual_expend',
  width: '12%'
}]

@inject('projectsStore')
@observer
class Investment extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      manNameList: [],
      actualCost: []
    }
  }

  componentDidMount () {
    this.getPrincipal(() => {
      this.getData()
    })
  }

  getData = () => {
    this.setState({ loading: true }, () => {
      this.props.projectsStore.getMonitoring({
        projectId: this.props.projectInfo.id,
        stageId: this.props.projectInfo.stageType
      }).then(() => {
        var actualCost = []
        var manNameList = this.state.manNameList
        this.props.projectsStore.monitor.actualCost.map(item => {
          actualCost.push(manNameList.find(k => k.userId === item.create_user_id))
        })
        this.setState({ loading: false, actualCost: actualCost })
      })
    })
  }

  getPrincipal = (fn) => {
    const { projectInfo, projectsStore } = this.props
    projectsStore.getManagerNameList({ deptName : '交付中心', tenantId: window.USER_INFO.tenantId }).then(() => {
      this.setState({manNameList: projectsStore.manNameList}, fn())
    })
  }

  render () {
    const { projectInfo } = this.props
    const monitor = this.props.projectsStore.monitor || { planCost: [], actualCost: [] }
    console.log(monitor)
    const plan = monitor.planCost.filter(k => k) || []
    const actual = monitor.actualCost.filter(k => k) || []
    const employees = actual.length
    var hours = 0
    actual.map(item => hours += (item.actual_expend || 0))
    var shift = (((actual.actual_expend / 8) + (actual.travel || 0) - plan.manpower + plan.travel) / (plan.manpower + plan.travel) * 100)
    shift = isNaN(shift) ? '--' : shift + '%'

    return (
      <div className="basic-table">
        <Spin spinning={this.state.loading}>
          <div className="buttonBox">
            <Button type="primary">
              <Link to={'/home/budget/' + projectInfo.id}>{__('investment-supplementary-budget')}</Link>
            </Button>
          </div>
          <div className="bodyer">
            <Row>
              <Col span={20}>
                <Row>
                  <Col span={8}>
                    <span name="name">{__('investment-expected-manpower-input')}&nbsp;:&nbsp;</span>
                    <span name="value">{plan.manpower || 0}</span>
                  </Col>
                  <Col span={8}>
                    <span name="name">{__('investment-actual-labor-input')}&nbsp;:&nbsp;</span>
                    <span name="value">{(hours / 8).toFixed(1)}</span>
                  </Col>
                </Row>
                <Row>
                  <Col span={8}>
                    <span name="name">{__('investment-expected-travel-budget')}&nbsp;:&nbsp;</span>
                    <span name="value">{plan.travel || 0}</span>
                  </Col>
                  <Col span={8}>
                    <span name="name">{__('investment-actual-margin-budget')}&nbsp;:&nbsp;</span>
                    <span name="value">--</span>
                  </Col>
                  <Col span={8}>
                    <span name="name">{__('investment-total-shift')}&nbsp;:&nbsp;</span>
                    <span name="value">{shift}</span>
                  </Col>
                </Row>
                <Row>
                  <Col span={8}>
                    <span name="name">{__('investment-total-number-employees')}&nbsp;:&nbsp;</span>
                    <span name="value">{employees}</span>
                  </Col>
                  <Col span={8}>
                    <span name="name">{__('investment-total-project-hours')}&nbsp;:&nbsp;</span>
                    <span name="value">{hours}</span>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          <Table
            rowKey="id"
            columns={columns}
            pagination={false}
            dataSource={this.state.actualCost} />
        </Spin>
      </div>
    )
  }
}

export default Investment
