import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Table, Button, Input, Transfer, Modal, Spin, notification } from '@uyun/components'

import __ from '@uyun/utils/i18n'

import './Member.less'

const ButtonGroup = Button.Group
const Search = Input.Search

@inject('projectsStore')
@observer
class Member extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      loading: false,
      memberList: [],
      params: {
        appId: '',
        name: '',
        tenantId: '',
        type: ''
      },
      listUserParams: {
        name: '',
        tenantId: window.USER_INFO.tenantId,
        departId: ''
      },
      columns: [
        {
          title: __('member-employee-ID'),
          dataIndex: 'userId',
          key: 'userId',
          align: 'center',
          width: '80px'
        }, {
          title: __('member-name'),
          dataIndex: 'realname',
          key: 'realname',
          align: 'center',
          width: '90px'
        }, {
          title: __('member-station'),
          dataIndex: 'station',
          key: 'station',
          align: 'center',
          width: '460px',
          render: text => text || '--'
        }, {
          title: __('member-level'),
          dataIndex: 'level',
          key: 'level',
          align: 'center',
          width: '125px',
          render: text => text || '--'
        }, {
          title: __('member-operation'),
          width: '30%',
          align: 'center',
          key: 'operation',
          render: (text, record) => (
            <ButtonGroup type="link">
              <a>{__('button-remove')}</a>
            </ButtonGroup>
          )
        }
      ]
    }
  }

  componentDidMount () {
    const { projectInfo } = this.props
    const user = window.USER_INFO
    this.setState({
      params: {
        appId: projectInfo.id,
        name: user.username,
        tenantId: user.tenantId,
        type: user.userType
      },
      selected: []
    }, () => {
      this.getTable()
      this.getUsers()
    })
  }

  save = () => {
    const { projectsStore } = this.props
    const { memberList } = this.state

    const { selected } = this.state
    const add = selected.filter(k => memberList.indexOf(k) < 0)
    var del = memberList.filter(k => selected.indexOf(k) < 0)
    del = del.map(item => ({ id: item.id }))

    if (add && add.length > 0) {
      projectsStore.addBatchProjectMember(add)
    }
    if (del && del.length > 0) {
      projectsStore.delBatchProjectMember(del)
    }
    notification.success({
      message: '添加项目成员成功'
    })
    this.onClose()
  }

  getTable = () => {
    const { projectsStore, projectInfo } = this.props
    projectsStore.getProjectMember({ projectId: projectInfo.id }).then(() => {
      this.setState({ memberList: projectsStore.memberList })
    })
  }

  getUsers = () => {
    this.setState({ loading: true })
    this.props.projectsStore.getTenantList().then(() => {
      const res = this.props.projectsStore.tenants

      if (res) {
        var tenantList = res
        tenantList.filter(t => {
          if (t.name === '交付中心') {
            this.setState(state => ({
              listUserParams: {
                ...state.listUserParams,
                departId: t.departId
              }
            }), () => {
              this.props.projectsStore.postListUsers(this.state.listUserParams).then(() => {
                const rMember = this.props.projectsStore.memberList.map(item => item.memberId)
                this.setState({ loading: false, rMember: rMember })
              })
            })
          }
        })
      }
    })
  }

  onClose = () => {
    this.setState({ visible: false })
  }

  onOpen = () => {
    this.setState({ visible: true }, () => {
      this.getUsers()
    })
  }

  onSearch = (val) => {
    this.setState(state => ({
      listUserParams: {
        ...state.listUserParams,
        name: val
      }
    }), () => {
      this.getUsers()
    })
  }

  onClear = () => {
    this.setState(state => ({
      listUserParams: {
        ...state.listUserParams,
        name: ''
      }
    }), () => {
      this.getUsers()
    })
  }

  handleChange = (targetKeys, direction, moveKeys) => {
    const { projectsStore } = this.props
    const list = [...this.state.memberList, ...projectsStore.userList]
    const targetList = list.map(item => {
      if (targetKeys.indexOf(item.id) >= 0) {
        return item
      }
    })

    this.setState({ rMember: targetKeys, selected: targetList.filter(k => !!k) })
  }

  renderItem = (item) => {
    const customLabel = (
      <div className="transferItem">
        <span>{item.realname}</span>
        --
      </div>
    )

    return {
      label: customLabel,
      value: item.realname
    }
  }

  render () {
    const { visible, loading, columns, rMember, memberList } = this.state
    const { projectsStore } = this.props
    var userList = projectsStore.userList
    Array.from(userList, item => item.key = item.id)
    userList = userList.filter(k => k)
    console.log('memberList', memberList)
    return (
      <div className="basic-member">
        <header>
          <div className="buttonBox">
            <Button type="primary" onClick={this.onOpen}>{__('button-add-members')}</Button>
          </div>
        </header>
        <Table columns={columns} rowKey="id" pagination={false} dataSource={memberList} footer={() => '共 ' + memberList.length + ' 名成员'} />

        <Modal
          title={__('button-add-project-members')}
          className="memberModal"
          visible={visible}
          onOk={this.save}
          onCancel={this.onClose}
          destroyOnClose
        >
          <Spin spinning={loading}>
            <Search
              placeholder="请输入"
              onSearch={this.onSearch}
              allowClear
              onClear={this.onClear}
            />
            <Transfer
              dataSource={userList}
              targetKeys={rMember}
              onChange={this.handleChange}
              render={this.renderItem}
            />
          </Spin>
        </Modal>
      </div>
    )
  }
}

export default Member
