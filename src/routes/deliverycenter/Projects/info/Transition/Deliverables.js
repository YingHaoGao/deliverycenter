import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Table, Button, Input, Select, Modal, Form, Row, Col, Spin } from '@uyun/components'

import __ from '@uyun/utils/i18n'

import './Deliverables.less'

const ButtonGroup = Button.Group
const { TextArea } = Input
const Search = Input.Search
const Option = Select.Option

const ifDis = (key) => {
  return true
}

const columns = [{
  title: __('deliverables-name'),
  dataIndex: 'deliverableTitle',
  key: 'deliverableTitle'
}, {
  title: __('deliverables-subordinate-stage'),
  dataIndex: 'age',
  key: 'stage',
  width: '12%',
  render: text => global.params.stageType.find(k => k.key === text).label
}, {
  title: __('deliverables-delivery-statuse'),
  dataIndex: 'status',
  key: 'status',
  width: '12%',
  render: text => global.params.typeList.find(k => k.key === text).label
}, {
  title: __('deliverables-submitter'),
  dataIndex: 'createUser',
  key: 'createUser',
  width: '12%'
}, {
  title: __('deliverables-submission-time'),
  dataIndex: 'createTime',
  key: 'createTime',
  width: '12%'
}, {
  title: __('deliverables-operation'),
  dataIndex: 'address',
  width: '30%',
  key: 'address',
  render: (text, record) => (
    <ButtonGroup type="link">
      <a disabled={ifDis('review')}>{__('button-review')}</a>
      <a href={record.filePath}>{__('button-download')}</a>
    </ButtonGroup>
  )
}]
const stageList = global.params.stageType
const modelColumns = [{
  title: __('table-index'),
  key: 'index',
  width: '40px',
  render: (text, record, index) => index
}, {
  title: __('deliverables-name'),
  dataIndex: 'name',
  width: '150px',
  key: 'name'
}, {
  title: __('deliverables-subordinate-stage'),
  dataIndex: 'stage',
  key: 'stage',
  render: (text, record) => {
    const stage = stageList.filter(item => item.key === record.stage)
    return stage[0] ? stage[0].label : ''
  }
}, {
  title: __('deliverables-delivery-statuse'),
  dataIndex: 'statuse',
  key: 'statuse',
  width: '100px'
}, {
  title: __('deliverables-submitter'),
  dataIndex: 'submitter',
  key: 'submitter',
  width: '60px'
}, {
  title: __('deliverables-submission-time'),
  dataIndex: 'tim',
  key: 'tim',
  width: '140px'
}]

@inject('projectsStore')
@observer
@Form.create()
class Deliverables extends Component {
  constructor (props) {
    super(props)

    this.state = {
      visible: false,
      loading: false,
      managerList: [],
      tableData: [],
      params: {
        stageId: 5,
        filename: '',
        substageId: 1,
        projectId: props.projectInfo.id
      }
    }
  }

  componentDidMount () {
    this.getDeliverable()
    this.getPrincipal()
  }

  getPrincipal = () => {
    const { projectInfo, projectsStore } = this.props
    projectsStore.getManagerNameList({ deptName : '交付中心', tenantId: window.USER_INFO.tenantId }).then(() => {
      this.setState({ managerList: projectsStore.manNameList })
    })
  }

  handleChange = (value) => {
    var params = Object.assign({}, this.state.params, { substageId: value })

    this.setState({ params: params }, () => {
      this.getDeliverable()
    })
  }

  getDeliverable = () => {
    const { projectInfo, projectsStore } = this.props
    projectsStore.getStageDeliverable({ projectId: projectInfo.id, stageId: 3, filename: this.state.params.filename }).then(() => {
      this.setState({ tableData: projectsStore.deliverable })
    })
  }

  onSearch = (val) => {
    const { params } = this.state
    params.filename = val
    this.setState({ params: params }, () => {
      this.getDeliverable()
    })
  }

  onClear = () => {
    const { params } = this.state
    params.filename = ''
    this.setState({ params: params }, () => {
      this.getDeliverable()
    })
  }

  save = () => {
    const { projectInfo } = this.props

    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({ loading: true })
        var obj = {}
        obj.applyJson = JSON.stringify(projectInfo)
        obj.modeId = 14
        obj.approvalUserId = values.approvalUserId
        obj.applyReason = values.applyReason
        obj.applyTime = moment((new Date()).getTime()).format('YYYY-MM-DD HH:MM:ss')
        obj.applyTitle = projectInfo.projectName
        obj.applyType = 14
        obj.projectId = projectInfo.id
        projectsStore.postAppAddPid(obj).then(() => {
          this.props.onClose()
          this.setState({ loading: false })
        })
      }
    })
  }

  onClose = () => {
    this.setState({ visible: false })
  }

  onOpen = () => {
    this.setState({ visible: true })
  }

  render () {
    const { visible, params, loading, managerList, tableData } = this.state
    const { projectsStore } = this.props
    const { getFieldDecorator } = this.props.form

    return (
      <div className="basic-deliver">
        <header>
          <div className="inputBox">
            <Search
              onSearch={this.onSearch}
              allowClear
              onClear={this.onClear}
            />
          </div>
          <div className="inputBox">
            <Select defaultValue={params.substageId} style={{ width: 200 }} onChange={this.handleChange}>
              {stageList.map(item => {
                return <Option value={item.key} key={item.key}>{item.label}</Option>
              })}
            </Select>
          </div>
          <div className="buttonBox">
            <Button type="primary" onClick={this.onOpen}>{__('deliverables-delivery-review')}</Button>
          </div>
        </header>
        <Spin spinning={loading}>
          <Table
            columns={columns}
            pagination={false}
            dataSource={tableData} />
        </Spin>

        <Modal
          title={__('deliverables-delivery-review')}
          className="deliverableMode"
          visible={visible}
          onOk={this.save}
          onCancel={this.onClose}
          destroyOnClose={true}
        >
          <Table columns={modelColumns} pagination={false} dataSource={tableData} />
          <Form>
            <Row className="text">
              <Col span={24}>
                <Form.Item label={__('newTask-apply-instructions')}>
                  {getFieldDecorator('applyReason')(
                    <TextArea rows={4} />
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Form.Item label="审批人">
                  {getFieldDecorator('approvalUserId', { rules: [{ required: true }] })(
                    <Select>
                      {managerList.map((item, i) => {
                        return <Option value={item.userId} key={i}>{item.realname}</Option>
                      })}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Modal>
      </div>
    )
  }
}

export default Deliverables
