import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

import ProjectInfo from '@/components/ProjectInfo'
import CommuniRecordCnt from '@/components/CommuniRecord'

import './index.less'

@inject('projectsStore')
@observer
class Information extends Component {
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      params: {
        projectId: props.projectInfo.id,
        pageNum: 0,
        pageSize: 10
      }
    }
  }

	componentDidMount () {
    this.getCommunication()
	}

  // 获取沟通列表
  getCommunication () {
    const { projectsStore } = this.props

    projectsStore.getCommuniInfoByProId(this.state.params)
  }

  render () {
    const { projectInfo, companyInfo, communiInfo } = this.props.projectsStore

    return (
      <div>
        <ProjectInfo projectInfo={projectInfo} companyInfo={companyInfo} />
        <CommuniRecordCnt communiInfo={communiInfo} getCommunication={this.getCommunication} />
      </div>
    )
  }
}

export default Information
