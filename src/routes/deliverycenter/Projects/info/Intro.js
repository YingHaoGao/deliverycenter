import __ from '@uyun/utils/i18n'
import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Card } from '@uyun/components'

import './Intro.less'

const projectStageList = [
  {
    key: '',
    label: __('state-all')
  },
  {
    key: '1',
    label: __('project-tab-preSales')
  },
  {
    key: '2',
    label: __('project-tab-contract')
  },
  {
    key: '3',
    label: __('project-tab-transition')
  },
  {
    key: '4',
    label: __('project-tab-service')
  },
  {
    key: '5',
    label: __('project-archived')
  }
]

export default class ProjectCard extends Component {
  render () {
    const { info } = this.props

    return (
      <Card>
        <div className="title">{info.projectName}</div>
        <div className="code">{info.projectCode}</div>
        <div className="info">
          <div className="stage">{__('project-tab-projectStage')}: <span>{((projectStageList.filter(k => k.key === info.projectStage))[0] ? (projectStageList.filter(k => k.key === info.projectStage))[0].label : '--')}</span></div>
          <div className="manageUser">{__('project-tab-manageUser')}: <span>{info.manageUser}</span></div>
          <div className="createTime">{__('project-tab-createTime')}: <span>{info.createTime}</span></div>
        </div>
      </Card>
    )
  }
}
