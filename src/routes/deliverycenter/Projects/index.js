import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Card, Table, Select, Input, Spin, Form, Row, Col, Icon, Button, DatePicker } from '@uyun/components'
import classNames from 'classnames'

import PageHeader from '@/components/PageHeader'
import __ from '@uyun/utils/i18n'

import './index.less'

const Search = Input.Search
const Option = Select.Option
const FormItem = Form.Item
const radioList = global.params.projectStatus.filter(k => k.key !== '0')
const columns = [
  {
    title: __('table-title-project-number'),
    dataIndex: 'projectCode',
    render: (name, row) => <Link to={'/deliverycenter/projects/project/' + row.id}>{name}</Link>
  },
  {
    title: __('table-title-project-name'),
    dataIndex: 'projectName',
    render: (name, row) => <Link to={'/deliverycenter/projects/project/' + row.id}>{name}</Link>
  },
  {
    title: __('table-title-customer-name'),
    dataIndex: 'proxyName'
  },
  {
    title: __('table-title-industry'),
    dataIndex: 'industry'
  },
  {
    title: __('table-title-area'),
    dataIndex: 'department'
  },
  {
    title: __('table-title-director-sales'),
    dataIndex: 'applyUser'
  },
  {
    title: __('table-title-preSales-person'),
    dataIndex: 'presalesUser'
  },
  {
    title: __('table-title-project-manager'),
    dataIndex: 'manageUser'
  },
  {
    title: __('table-title-project-phase'),
    dataIndex: 'projectStage',
    render: s => ((projectStageList.filter(k => k.key === s))[0] ? (projectStageList.filter(k => k.key === s))[0].label : '--')
  },
  {
    title: __('table-title-project-state'),
    dataIndex: 'status',
    render: s => ((radioList.filter(k => k.key === s))[0] ? (radioList.filter(k => k.key === s))[0].label : '--')
  },
  {
    title: __('table-title-project-time'),
    dataIndex: 'createTime'
  }
]
const projectStageList = [
  {
    key: '',
    label: __('state-all')
  },
  {
    key: '1',
    label: __('project-tab-preSales')
  },
  {
    key: '2',
    label: __('project-tab-contract')
  },
  {
    key: '3',
    label: __('project-tab-transition')
  },
  {
    key: '4',
    label: __('project-tab-service')
  },
  {
    key: '5',
    label: __('project-archived')
  }
]

@inject('projectsStore')
@observer
@Form.create()
class ProjectsTable extends Component {
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      tenantList: [],
      managerList: [],
      search: '',
      loading: false,
      expand: true,
      params: {
        applyUserId: window.USER_INFO.userId,
        createTime: '',
        department: '',
        pageNum: 0,
        pageSize: 10,
        tenantId: window.USER_INFO.tenantId,
        projectName: '',
        status: '',
        projectStage: ''
      }
    }
  }

  componentDidMount () {
    this.getTableData()
    this.props.projectsStore.getTenantList()
    this.getPrincipal()
  }

  getPrincipal = () => {
    var params = {
      // name: window.USER_INFO.realname,
      // tenantId: window.USER_INFO.tenantId,
      name: '交付人员',
      tenantId: 'e10adc3949ba59abbe56e057f20f88dd',
      userId: window.USER_INFO.userId
    }
    this.props.projectsStore.getDeptList(params).then(() => {
        this.setState({ managerList: this.props.projectsStore.deptList })
    })
  }

  radioChange = (val) => {
    this.setState(state => ({
      params: {
        ...state.params,
        status: val
      }
    }), () => {
      this.getTableData()
    })
  }

  paginChange = (pagination) => {
    const { params } = this.state

    params.pageSize = pagination.pageSize
    params.pageNum = pagination.current

    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  onSearch = (val) => {
    const { params } = this.state
    params.applyUserId = val
    params.projectName = val
    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  onClear = () => {
    const { params } = this.state
    params.applyUserId = ''
    params.projectName = ''
    this.setState({ params: params }, () => {
      this.getTableData()
    })
  }

  getTableData () {
    this.setState({ loading: true }, () => {
      this.props.projectsStore.getProjects(this.state.params).then(res => {
        this.setState({ loading: false })
      })
    })
  }

  handleSearch = (e) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState(state => ({
          params: {
            ...state.params,
            ...values,
            createTime: values.createTime.format('YYYY-MM-DD')
          }
        }), () => {
          this.getTableData()
        })
      }
    })
  }

  handleReset = () => {
    this.props.form.resetFields()
  }

  toggle = () => {
    const { expand } = this.state
    this.setState({ expand: !expand })
  }

  render () {
    const { params, loading, expand, managerList } = this.state
    const { getFieldDecorator } = this.props.form
    const tenantList = this.props.projectsStore.tenants || []
    const tableData = this.props.projectsStore.projects || { list: [] }
    const cls = classNames('advanced-search-form', {
      'advanced-search-form-collapse': !expand
    })

    return (
      <div className="basic-projects">
        <PageHeader />

        <header>
          <Search
            placeholder={__('form-item-search')}
            onSearch={this.onSearch}
            allowClear
            onClear={this.onClear}
          />
          <Select className="radio" onChange={this.radioChange} defaultValue={params.status}>
            {radioList.map((item, i) => {
              return <Option key={i} value={item.key}>{item.label}</Option>
            })}
          </Select>
          <span className="advanced-search-form-toggle" onClick={this.toggle}>
            高级筛选 <Icon type={expand ? 'up' : 'down'} style={{ marginLeft: 8 }} />
          </span>
          <div className={cls}>
            <Form
              hideRequiredMark
              className="advanced-search-form-fields"
              onSubmit={this.handleSearch}
            >
              <Row gutter={24}>
                <Col span={6}>
                  <FormItem label="销售区域" labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}>
                    {getFieldDecorator('department')(
                      <Select>
                        {tenantList.map((item, i) => {
                          return <Option key={i} value={item.departId}>{item.name}</Option>
                        })}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col span={6}>
                  {
                    // <FormItem label="售前负责人" labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}>
                    //   {getFieldDecorator('presalesUser')(<Input />)}
                    // </FormItem>
                  }
                  <Form.Item label="售前负责人" labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}>
                    {getFieldDecorator('presalesUserId')(
                      <Select>
                        {managerList.map(item => {
                          return <Option value={item.userId} key={item.userId}>{item.realname}</Option>
                        })}
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col span={6}>
                  {
                    // <FormItem label="项目经理" labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}>
                    //   {getFieldDecorator('manageUser')(<Input />)}
                    // </FormItem>
                  }
                  <Form.Item label="项目经理" labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}>
                    {getFieldDecorator('manageUserId')(
                      <Select>
                        {managerList.map(item => {
                          return <Option value={item.userId} key={item.userId}>{item.realname}</Option>
                        })}
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col span={6}>
                  <FormItem label="项目阶段" labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}>
                    {getFieldDecorator('projectStage')(
                      <Select>
                        {projectStageList.map((item, i) => {
                          return <Option key={i} value={item.key}>{item.label}</Option>
                        })}
                      </Select>
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col span={6}>
                  <FormItem label="启动时间" labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}>
                    {getFieldDecorator('createTime')(
                      <DatePicker />
                    )}
                  </FormItem>
                </Col>
                <Col span={18} style={{ textAlign: 'right' }}>
                  <Button type="primary" htmlType="submit">查询</Button>
                  <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
                    重置
                  </Button>
                </Col>
              </Row>
            </Form>
          </div>
        </header>
        <Card
          bordered={false}
          style={{ marginTop: 16 }}
        >
          <Spin spinning={loading}>
            <Table
              rowKey="id"
              dataSource={tableData.list}
              columns={columns}
              pagination={{ total: tableData.total }}
              onChange={this.paginChange}
            />
          </Spin>
        </Card>
      </div>
    )
  }
}

export default ProjectsTable
