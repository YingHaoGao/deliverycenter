import { observable, action, runInAction } from 'mobx'

import {
  getBacklogList,
  getTenantList,
  getTaskInfo,
  getAttachmentList,
  getTaskDeliverList,
  postWorkLogList,
  getTaskList,
  delTask,
  delFile,
  postTaskChangeStatus,
  delDeliverable,
  postTaskAdd,
  getUnDelList,
  getConPayList,
  getConIncomeList,
  getManagerNameList,
  getSubStageQueryStageTask,
  editTaskInfo
} from '@/services/api'

export default class Task {
  @observable
  taskObj = {}

  @observable
  tenants = []

  @observable
  taskInfo = {}

  @observable
  attachmentInfo = {}

  @observable
  deliverInfo = {}

  @observable
  hourTable = {}

  @observable
  subTask = []

  @observable
  delFileFn = {}

  @observable
  statusCode = false

  @observable
  delDelCode = false

  @observable
  unDelList = []

  @observable
  conPayList = []

  @observable
  conIncomeList = []

  @observable
  manNameList = []

  @observable
  stageTask = []

  @action
  async getBacklogList (params) {
    const data = await getBacklogList(params)

    runInAction(() => {
      this.taskObj = data.resultMap.data ? data.resultMap.data : { list: [] }
    })
  }

  @action
  async getTenantList (params) {
    const data = await getTenantList(params)

    runInAction(() => {
      this.tenants = data.resultMap.data
    })
  }

  @action
  async getTaskInfo (params) {
    const data = await getTaskInfo(params)

    runInAction(() => {
      this.taskInfo = data.resultMap.data ? data.resultMap.data : {}
    })
  }

  @action
  async getAttachmentList (params) {
    const data = await getAttachmentList(params)

    runInAction(() => {
      this.attachmentInfo = data.resultMap.data ? data.resultMap.data : {}
    })
  }

  @action
  async getTaskDeliverList (params) {
    const data = await getTaskDeliverList(params)

    runInAction(() => {
      this.deliverInfo = data.resultMap.data ? data.resultMap.data : {}
    })
  }

  @action
  async postWorkLogList (params) {
    const data = await postWorkLogList(params)

    runInAction(() => {
      this.hourTable = data.resultMap.data ? data.resultMap.data : {}
    })
  }

  @action
  async getTaskList (params) {
    const data = await getTaskList(params)

    runInAction(() => {
      this.subTask = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async delTask (params) {
    const data = await delTask(params)

    runInAction(() => {
      
    })
  }

  @action
  async delFile (params) {
    const data = await delFile(params)

    runInAction(() => {
      this.delFileFn = data.resultMap.data ? data.resultMap.data : {}
    })
  }

  @action
  async postTaskChangeStatus (params) {
    const data = await postTaskChangeStatus(params)

    runInAction(() => {
      this.statusCode = data.code === 0
    })
  }

  @action
  async delDeliverable (params) {
    const data = await delDeliverable(params)

    runInAction(() => {
      this.delDelCode = data.code === 0
    })
  }

  @action
  async postTaskAdd (params) {
    const data = await postTaskAdd(params)

    runInAction(() => {
    })
  }

  @action
  async getUnDelList (params) {
    const data = await getUnDelList(params)

    runInAction(() => {
      this.unDelList = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getConPayList (params) {
    const data = await getConPayList(params)

    runInAction(() => {
      this.conPayList = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getConIncomeList (params) {
    const data = await getConIncomeList(params)

    runInAction(() => {
      this.conIncomeList = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getManagerNameList (params) {
    const data = await getManagerNameList(params)

    runInAction(() => {
      this.manNameList = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getSubStageQueryStageTask (params) {
    const data = await getSubStageQueryStageTask(params)

    runInAction(() => {
      this.stageTask = data.resultMap.data
    })
  }

  @action
  async editTaskInfo (params) {
    const data = await editTaskInfo(params)

    runInAction(() => {
      
    })
  }
}
