import { observable, action, runInAction } from 'mobx'

import {
  getContractYear,
  getContractAcceptance,
  getWorkHours,
  getTenantList
} from '@/services/api'

export default class apply {
  @observable
  contractYear = {}

  @observable
  acceptance = []

  @observable
  workHours = []

  @observable
  deptList = []

  @action
  async getContractYear (params) {
    const data = await getContractYear(params)

    runInAction(() => {
      this.contractYear = data.resultMap.data ? data.resultMap.data[0] : {}
    })
  }

  @action
  async getContractAcceptance (params) {
    const data = await getContractAcceptance(params)

    runInAction(() => {
      this.acceptance = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getWorkHours (params) {
    const data = await getWorkHours(params)

    runInAction(() => {
      this.workHours = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getTenantList (params) {
    const data = await getTenantList(params)

    runInAction(() => {
      this.deptList = data.resultMap.data ? data.resultMap.data : []
    })
  }
}
