import { observable, action, runInAction } from 'mobx'

import {
  postWorkList,
  postListUsers
} from '@/services/api'

export default class Logs {
  @observable
  workObj = {}

  @observable
  userList = []

  @action
  async postWorkList (params) {
    const data = await postWorkList(params)

    runInAction(() => {
      this.workObj = data.resultMap.data ? data.resultMap.data : { list: [] }
    })
  }

  @action
  async postListUsers (params) {
    const data = await postListUsers(params)

    runInAction(() => {
      console.log(data)
      this.userList = data.resultMap.data ? data.resultMap.data : []
    })
  }
}
