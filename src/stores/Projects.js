import { observable, action, runInAction } from 'mobx'

import {
  getProjects,
  getTenantList,
  getProjectInfo,
  getStageDeliverable,
  getDeptList,
  postAppAddPid,
  getSubStageQueryStageTask,
  delSubStage,
  changeTaskStatus,
  getMonitoring,
  postListUsers,
  getProjectMember,
  delBatchProjectMember,
  addBatchProjectMember,
  getWeightExceed100,
  postSubUpdata,
  postTaskAdd,
  getCompanyContact,
  addDeliverable,
  postSubStageAdd,
  delDeliverable,
  getStageDeliverable1,
  getUnDelList,
  getConPayList,
  getConIncomeList,
  postFileUpload,
  getManagerNameList,
  getDeptInfo,
  postProjectUpdata,
  getDeliverableList,
  getCompanyInfoByProId,
  getCommuniInfoByProId,
  changeSubStageStatus,
  getContractAttachment,
  getBudgetDetail,
  getProjectQuotation,
  getBudgetList,
  getConPayIdList,
  getconAttInfo
} from '@/services/api'

export default class Projects {
  @observable
  projects = {}

  @observable
  projectInfo = {}

  @observable
  tenants = []

  @observable
  deliverable = []

  @observable
  deptList = []

  @observable
  stageTask = []

  @observable
  monitor = { planCost: [], actualCost: [] }

  @observable
  userList = []

  @observable
  memberList = []

  @observable
  weight = {}

  @observable
  companys = {}

  @observable
  del = {}

  @observable
  deliverable1 = []

  @observable
  pid = {}

  @observable
  unDelList = []

  @observable
  conPayList = []

  @observable
  conIncomeList = []

  @observable
  fileUpload = {}

  @observable
  manNameList = []

  @observable
  deptInfo = {}

  @observable
  delList = []

  // 公司信息
  @observable
  companyInfo = {}

  // 行业信息
  @observable
  industry = []

  // 销售产品
  @observable
  salesProducts = []

  // 竞争对手
  @observable
  competitor = []

  // 沟通记录
  @observable
  communiInfo = { list: [] }

  @observable
  changeSuc = false

  @observable
  contractInfo = []

  @observable
  budgetInfo = {}

  @observable
  quotation = []

  @observable
  budgetList = { list: [] }

  @observable
  conPayIdList = []

  @observable
  conAttInfo = {}

  @action
  async getProjects (params) {
    const data = await getProjects(params)

    runInAction(() => {
      this.projects = data.resultMap.data
    })
  }

  @action
  async getProjectInfo (params) {
    const data = await getProjectInfo(params)

    runInAction(() => {
      this.projectInfo = data.resultMap.data
      this.competitor = data.resultMap.data.competitor.split(',')
      this.salesProducts = data.resultMap.data.productIds.split(',')
    })
  }

  @action
  async getTenantList (params) {
    const data = await getTenantList(params)

    runInAction(() => {
      this.tenants = data.resultMap.data
    })
  }

  @action
  async getStageDeliverable (params) {
    const data = await getStageDeliverable(params)

    runInAction(() => {
      this.deliverable = data.resultMap ? data.resultMap.data : []
    })
  }

  @action
  async getDeptList (params) {
    const data = await getDeptList(params)

    runInAction(() => {
      this.deptList = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async postAppAddPid (params) {
    const data = await postAppAddPid(params)

    runInAction(() => {
      this.pid = data.resultMap
    })
  }

  @action
  async getSubStageQueryStageTask (params) {
    const data = await getSubStageQueryStageTask(params)

    runInAction(() => {
      this.stageTask = data.resultMap.data
    })
  }

  @action
  async delSubStage (params) {
    const data = await delSubStage(params)

    runInAction(() => {
      this.del = data.resultMap ? data.resultMap.data : {}
    })
  }

  @action
  async changeTaskStatus (params) {
    const data = await changeTaskStatus(params)

    runInAction(() => {})
  }

  @action
  async getMonitoring (params) {
    const data = await getMonitoring(params)

    runInAction(() => {
      this.monitor = data.resultMap ? data.resultMap : { planCost: [], actualCost: [] }
    })
  }

  @action
  async postListUsers (params) {
    const data = await postListUsers(params)
    runInAction(() => {
      this.userList = data.resultMap ? data.resultMap.data : []
    })
  }

  @action
  async getProjectMember (params) {
    const data = await getProjectMember(params)

    runInAction(() => {
      this.memberList = data.resultMap ? data.resultMap.data : []
    })
  }

  @action
  async delBatchProjectMember (params) {
    const data = await delBatchProjectMember(params)

    runInAction()
  }

  @action
  async addBatchProjectMember (params) {
    const data = await addBatchProjectMember(params)

    runInAction()
  }

  @action
  async getWeightExceed100 (params) {
    const data = await getWeightExceed100(params)

    runInAction(() => {
      this.weight = data.resultMap
    })
  }

  @action
  async postSubUpdata (params) {
    const data = await postSubUpdata(params)

    runInAction(() => {
    })
  }

  @action
  async postTaskAdd (params) {
    const data = await postTaskAdd(params)

    runInAction(() => {
    })
  }

  @action
  async getCompanyContact (params) {
    const data = await getCompanyContact(params)

    runInAction(() => {
      this.companys = data.resultMap.data ? data.resultMap.data.contactList : { contactList: [] }
    })
  }

  @action
  async addDeliverable (params) {
    const data = await addDeliverable(params)

    runInAction(() => {})
  }

  @action
  async postSubStageAdd (params) {
    const data = await postSubStageAdd(params)

    runInAction(() => {})
  }

  @action
  async delDeliverable (params) {
    const data = await delDeliverable(params)

    runInAction(() => {})
  }

  @action
  async getStageDeliverable1 (params) {
    const data = await getStageDeliverable1(params)

    runInAction(() => {
      this.deliverable1 = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getUnDelList (params) {
    const data = await getUnDelList(params)

    runInAction(() => {
      this.unDelList = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getConPayList (params) {
    const data = await getConPayList(params)

    runInAction(() => {
      this.conPayList = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getConIncomeList (params) {
    const data = await getConIncomeList(params)

    runInAction(() => {
      this.conIncomeList = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async postFileUpload (params) {
    const data = await postFileUpload(params)

    runInAction(() => {
      this.fileUpload = data.resultMap.data ? data.resultMap.data : {}
    })
  }

  @action
  async getManagerNameList (params) {
    const data = await getManagerNameList(params)

    runInAction(() => {
      this.manNameList = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getDeptInfo (params) {
    const data = await getDeptInfo(params)

    runInAction(() => {
      this.deptInfo = data.resultMap.data ? data.resultMap.data : {}
    })
  }

  @action
  async postProjectUpdata (params) {
    const data = await postProjectUpdata(params)

    runInAction(() => {
    
    })
  }

  @action
  async getDeliverableList (params) {
    const data = await getDeliverableList(params)

    runInAction(() => {
      this.delList = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getCompanyInfoByProId (id) {
    const data = await getCompanyInfoByProId({
      id: id
    })
    runInAction(() => {
      this.companyInfo = data.resultMap.data
      this.industry = data.resultMap.data ? data.resultMap.data.companyIndustry.split(',') : []
    })
  }

  @action
  async getCommuniInfoByProId (params) {
    const data = await getCommuniInfoByProId(params)
    runInAction(() => {
      this.communiInfo = data.resultMap.data ? data.resultMap.data : { list: [] }
    })
  }

  @action
  async changeSubStageStatus (params) {
    const data = await changeSubStageStatus(params)
    runInAction(() => {
      this.changeSuc = data.resultMap.code === 0
    })
  }

  @action
  async getContractAttachmentInfo (params) {
    const data = await getContractAttachment({
      contractId: params
    })
    runInAction(() => {
      this.contractInfo = data.resultMap.data ? data.resultMap.data.list[0] : []
    })
  }

  @action
  async getBudgetDetail (params) {
    const data = await getBudgetDetail(params)
    runInAction(() => {
      this.budgetInfo = data.resultMap.data ? data.resultMap.data : {}
    })
  }

  @action
  async getProjectQuotation (params) {
    const data = await getProjectQuotation(params)
    runInAction(() => {
      this.quotation = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getBudgetList (params) {
    const data = await getBudgetList(params)
    runInAction(() => {
      this.budgetList = data.resultMap.data ? data.resultMap.data : { list: [] }
    })
  }

  @action
  async getConPayIdList (params) {
    const data = await getConPayIdList(params)
    runInAction(() => {
      this.conPayIdList = data.resultMap.data ? data.resultMap.data : []
    })
  }

  @action
  async getconAttInfo (params) {
    const data = await getconAttInfo(params)
    runInAction(() => {
      this.conAttInfo = data.resultMap.data ? data.resultMap.data : {}
    })
  }
}
