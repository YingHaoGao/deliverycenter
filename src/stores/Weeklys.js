import { observable, action, runInAction } from 'mobx'

import {
  getWorkList,
  getProjectInfo,
  getWorkWeekly,
  postWorkWeekly,
  getDeliverableList
} from '@/services/api'

export default class weeklys {
  @observable
  workObj = {}

  @observable
  projectInfo = {}

  @observable
  weeklyInfo = {}

  @observable
  delList = []

  @action
  async getWorkList (params) {
    const data = await getWorkList(params)

    runInAction(() => {
      this.workObj = data.resultMap.data ? data.resultMap.data : { list: [] }
    })
  }

  @action
  async getProjectInfo (params) {
    const data = await getProjectInfo(params)

    runInAction(() => {
      this.projectInfo = data.resultMap.data ? data.resultMap.data : {}
    })
  }

  @action
  async getWorkWeekly (params) {
    const data = await getWorkWeekly(params)

    runInAction(() => {
      this.weeklyInfo = data.resultMap.data ? data.resultMap.data[0] : {}
    })
  }

  @action
  async postWorkWeekly (params) {
    const data = await postWorkWeekly(params)

    runInAction(() => {
      
    })
  }

  @action
  async getDeliverableList (params) {
    const data = await getDeliverableList(params)

    runInAction(() => {
      this.delList = data.resultMap.data ? data.resultMap.data : []
    })
  }
}
