import { observable, action, runInAction } from 'mobx'

import {
  getApplyList
} from '@/services/api'

export default class apply {
  @observable
  applyObj = {}

  @action
  async getApplyList (params) {
    const data = await getApplyList(params)

    runInAction(() => {
      this.applyObj = data.resultMap.data ? data.resultMap.data : { list: [] }
    })
  }
}
