import { observable, action, runInAction } from 'mobx'

import {
  getBacklogList
} from '@/services/api'

export default class Backlog {
  @observable
  data = {}

  @action
  async getBacklogList (params) {
    const data = await getBacklogList(params)

    runInAction(() => {
      this.data = data.resultMap.data
    })
  }
}
