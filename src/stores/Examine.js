import { observable, action, runInAction } from 'mobx'

import {
  postApprovalList,
  getApprovalInfoById,
  getApplyInfoById,
  getNextApprovalList
} from '@/services/api'

export default class apply {
  @observable
  examineObj = {}

  @observable
  approvalInfo = {}

  @observable
  applicationInfo = {}

  @observable
  currentNode = []

  @action
  async postApprovalList (params) {
    const data = await postApprovalList(params)

    runInAction(() => {
      this.examineObj = data.resultMap.data ? data.resultMap.data : { list: [] }
    })
  }

  @action
  async getApprovalInfoById (params) {
    const data = await getApprovalInfoById(params)

    runInAction(() => {
      this.approvalInfo = data.resultMap.data ? data.resultMap.data[0] : {}
    })
  }

  @action
  async getApplyInfo (id) {
    const data = await getApplyInfoById({ id })
    const applyInfo = data.resultMap.data
    runInAction(() => {
      this.applicationInfo = JSON.parse(applyInfo.applyJson)
    })
  }

  @action
  async getNextApprovalList (workId) {
    const data = await getNextApprovalList({
      userId: window.USER_INFO.userId,
      workId: workId
    })
    runInAction(() => {
      this.currentNode = data.resultMap.data ? data.resultMap.data.sequence : []
    })
  }
}
